﻿using DotFramework.Infra.BusinessFacade;
using DotFramework.Infra.BusinessRules;
using System;
using TestAssembly.BusinessRules;
using TestAssembly.DataAccessFactory;
using TestAssembly.Model;
using TestAssembly.ServiceFactory;

namespace TestAssembly.BusinessFacade
{
    public class CityService : SecureServiceBase<Int32, City, CityCollection, CityRules, ICityDataAccess>, ICityService
    {
        public override IBusinessRulesFactory RulesFactory => TestBusinessRulesFactory.Instance;
    }
}
