﻿using System;
using System.Collections.Generic;
using System.Text;
using TestAssembly.Model;
using TestAssembly.ServiceFactory;
using DotFramework.Infra.BusinessFacade;
using TestAssembly.DataAccessFactory;
using TestAssembly.BusinessRules;
using DotFramework.Infra.BusinessRules;

namespace TestAssembly.BusinessFacade
{
    public class PersonService : SecureServiceBase<Int32, Person, PersonCollection, PersonRules, IPersonDataAccess>, IPersonService
    {
        public override IBusinessRulesFactory RulesFactory => TestBusinessRulesFactory.Instance;

        public int Devide(int a, int b)
        {
            return BusinessRules.Devide(a, b);
        }
    }
}
