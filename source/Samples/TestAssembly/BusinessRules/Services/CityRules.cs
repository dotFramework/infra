﻿using DotFramework.Infra.BusinessRules;
using System;
using System.Collections.Generic;
using System.Text;
using TestAssembly.DataAccessFactory;
using TestAssembly.Model;

namespace TestAssembly.BusinessRules
{
    public class CityRules : RulesBase<Int32, City, CityCollection, ICityDataAccess>
    {
        protected override ICityDataAccess DataAccess => throw new NotImplementedException();

        protected override List<Action<City>> MasterPopulateActions => throw new NotImplementedException();

        protected override List<Action<City>> DetailPopulateActions => throw new NotImplementedException();

        protected override CityCollection Cache => throw new NotImplementedException();

        protected override bool AllowCache => throw new NotImplementedException();
    }
}
