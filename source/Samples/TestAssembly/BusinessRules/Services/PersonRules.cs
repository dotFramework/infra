﻿using DotFramework.Infra.BusinessRules;
using System;
using System.Collections.Generic;
using System.Text;
using TestAssembly.Caching;
using TestAssembly.DataAccessFactory;
using TestAssembly.Model;

namespace TestAssembly.BusinessRules
{
    public class PersonRules : RulesBase<Int32, Person, PersonCollection, IPersonDataAccess>
    {
        protected override IPersonDataAccess DataAccess => TestDataAccessFactory.Instance.GetDataAccess<IPersonDataAccess>();

		private List<Action<Person>> _MasterPopulateActions;
		protected override List<Action<Person>> MasterPopulateActions
		{
			get
			{
				if (this._MasterPopulateActions == null)
				{
					lock (padlock)
					{
						this._MasterPopulateActions = new List<Action<Person>>();

					}
				}

				return this._MasterPopulateActions;
			}
		}

		private List<Action<Person>> _DetailPopulateActions;
		protected override List<Action<Person>> DetailPopulateActions
		{
			get
			{
				if (this._DetailPopulateActions == null)
				{
					lock (padlock)
					{
						this._DetailPopulateActions = new List<Action<Person>>();
					}
				}

				return this._DetailPopulateActions;
			}
		}

		protected override PersonCollection Cache => TestCache.Instance.GetCache<PersonCollection>();

        protected override bool AllowCache => TestCache.Instance.IsAllowedCache<PersonCollection>();

        public virtual int Devide(int a, int b)
        {
            return DataAccess.Devide(a, b);
        }
    }
}
