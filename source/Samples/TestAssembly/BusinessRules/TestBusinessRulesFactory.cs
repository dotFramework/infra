﻿using DotFramework.Infra.BusinessRules.Autofac;

namespace TestAssembly.BusinessRules
{
    public class TestBusinessRulesFactory: BusinessRulesFactory<TestBusinessRulesFactory>
    {
        private TestBusinessRulesFactory()
        {

        }
    }
}
