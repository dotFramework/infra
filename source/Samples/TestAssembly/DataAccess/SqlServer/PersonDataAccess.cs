﻿using DotFramework.Infra.DataAccess;
using DotFramework.Infra.DataAccess.SqlServer;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Text;
using TestAssembly.DataAccessFactory;
using TestAssembly.Model;

namespace TestAssembly.DataAccess.SqlServer
{
    public class PersonDataAccess : SqlServerDataAccessBase<Int32, Person, PersonCollection>, IPersonDataAccess
    {
        public virtual int Devide(int a, int b)
        {
            return a / b;
        }

        public override Person Select(int key)
        {
            return new Person
            {
                PersonID = key,
                Name = $"Person {key}",
                HasValue = true
            };
        }

        protected override Person FillFromDataReader(DbDataReader dr)
        {
            throw new NotImplementedException();
        }
    }
}
