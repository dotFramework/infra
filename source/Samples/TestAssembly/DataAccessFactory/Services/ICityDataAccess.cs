﻿using DotFramework.Infra.DataAccessFactory;
using System;
using System.Collections.Generic;
using System.Text;
using TestAssembly.Model;

namespace TestAssembly.DataAccessFactory
{
    public interface ICityDataAccess : IDataAccessBase<Int32, City, CityCollection>
    {
    }
}
