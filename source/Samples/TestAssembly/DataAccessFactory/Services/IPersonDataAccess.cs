﻿using DotFramework.Infra.DataAccessFactory;
using System;
using TestAssembly.Model;

namespace TestAssembly.DataAccessFactory
{
    public interface IPersonDataAccess : IDataAccessBase<Int32, Person, PersonCollection>
    {
        int Devide(int a, int b);
    }
}
