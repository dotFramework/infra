﻿using DotFramework.Infra.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace TestAssembly.Model
{
    public class City : SecureDomainModelBase
    {
        [IsKey]
        public int CityID { get; set; }
    }

    public class CityCollection : ListBase<Int32, City>
    {

    }
}
