﻿using DotFramework.Infra.Model;
using System;

namespace TestAssembly.Model
{
    public class Person : SecureDomainModelBase
    {
        [IsKey]
        public int PersonID { get; set; }

        public string Name { get; set; }
    }

    public class PersonCollection : ListBase<Int32, Person>
    {

    }
}
