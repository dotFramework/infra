﻿using DotFramework.Infra.Request;

namespace TestAssembly.RequestServiceFactory
{
    public interface ITest2RequestService: IRequestService<Test2DataRequest, Test2DataResponse>
    {
    }
}