﻿using Autofac;
using DotFramework.Core;
using DotFramework.Infra.Request.Autofac;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace TestAssembly.RequestServiceFactory
{
    public class TestRequestServiceFactory : SecureRequestServiceFactoryBase<TestRequestServiceFactory>
    {
        protected override void RegisterAllServices()
        {
            base.RegisterAllServices();

            var assembly = Assembly.GetAssembly(typeof(ITest2RequestService));

            RegisterType(assembly.GetType("TestAssembly.RequestServiceFactory.Test2RequestService"),
                assembly.GetType("TestAssembly.RequestServiceFactory.ITest2RequestService"));
        }
    }
}
