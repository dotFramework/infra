﻿using DotFramework.Infra.ServiceFactory;
using System;
using TestAssembly.Model;

namespace TestAssembly.ServiceFactory
{
    public interface ICityService : ISecureServiceBase<Int32, City, CityCollection>
    {
    }
}
