﻿using DotFramework.Infra.ServiceFactory;
using System;
using TestAssembly.Model;

namespace TestAssembly.ServiceFactory
{
    public interface IPersonService: ISecureServiceBase<Int32, Person, PersonCollection>
    {
        int Devide(int a, int b);
    }
}
