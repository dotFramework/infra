﻿using DotFramework.Infra.ServiceFactory.Autofac;
using System;
using System.Collections.Generic;
using System.Text;

namespace TestAssembly.ServiceFactory
{
    public class TestServiceFactory: SecureServiceFactoryBase<TestServiceFactory>
    {
        private TestServiceFactory()
        {

        }
    }
}
