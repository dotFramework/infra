﻿using DotFramework.Infra.ExceptionHandling;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;

namespace WebApplication1.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class KeepAliveController : ControllerBase
    {
        private readonly ILogger<KeepAliveController> _logger;

        public KeepAliveController(ILogger<KeepAliveController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public string Get()
        {
            //var ex = new Exception("Test Exception");
            //ex.Data.Add("Arguments", "This is a test");

            //throw ex;

            //try
            //{
            //    var a = 0;
            //    var b = 10 / a;
            //}
            //catch (Exception ex)
            //{
            //    TraceLogManager.Instance.HandleException(ex, ExceptionHandlingPolicyConstants.LogOnlyPolicy);
            //}

            return "Test is alive!";
        }

        [HttpPost]
        public string Post()
        {
            return "Test is alive!";
        }
    }
}
