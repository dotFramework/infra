﻿using DotFramework.Infra.Request;

namespace WebApplication1.Services
{
    public interface ITestRequestService: IRequestService<TestDataRequest, TestDataResponse>
    {
    }
}
