﻿using DotFramework.Core.DependencyInjection;
using System.Reflection;
using System;
using DotFramework.Infra.Request;

namespace WebApplication1.Services
{
    public class SampleRequestServiceFactory : FactoryBase<SampleRequestServiceFactory, RequestServiceBase, RequestServiceInterceptor>
    {
        protected override InterceptionMethod InterceptionMethod => InterceptionMethod.Class;

        protected override bool HandleException(ref Exception ex)
        {
            return true;
        }

        public override TType Resolve<TType>()
        {
            var type = base.Resolve<TType>();
            return type;
        }

        public void RegisterServices(string dllPath)
        {
            Assembly assembly = Assembly.LoadFrom(dllPath);
            Register(assembly);
        }

        public new void Build()
        {
            base.Build();
        }
    }
}
