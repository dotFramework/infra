﻿using DotFramework.Core.Configuration;
using DotFramework.Infra.Security;
using DotFramework.Infra.Web;
using DotFramework.Infra.Web.API.Auth.Base.Providers;
using DotFramework.Infra.Web.API.Auth;
using DotFramework.Infra.Web.API.Auth.Middleware;
using DotFramework.Infra.Web.API.Auth.Providers;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using DotFramework.Infra.Web.API;
using System.Net.Mime;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using DotFramework.Infra.ExceptionHandling;
using System.Collections.Generic;
using DotFramework.Infra;
using DotFramework.Infra.Web.API.Controller;
using WebApplication1.Middleware;
using System.Net.Http;
using System.Configuration;
using DotFramework.Infra.Configuration;
using TestAssembly.ServiceFactory;
using DotFramework.Infra.Caching;
using System.Reflection;
using System.IO;
using System.Linq;
using DotFramework.Infra.Request;
using TestAssembly.RequestServiceFactory;
using WebApplication1.Services;
using Autofac;
using TestAssembly.Caching;
using System.Threading;

namespace WebApplication1
{
    public class Startup : SecureWebAPIStartup
    {
        public override string AplicationCode => AppSettingsManager.Instance.Get("ApplicationCode");

        public Startup(IConfiguration configuration) : base(configuration)
        {
            AccessTokenExpireTimeSpan = new TimeSpan(0, 30, 0);
        }

        public override void ConfigureServices(IServiceCollection services)
        {
            base.ConfigureServices(services);

            services
                .AddControllers(options =>
                {
                    options.AllowEmptyInputInBodyModelBinding = true;
                    options.Filters.Add<ExceptionActionFilter>();
                })
                .AddNewtonsoftJson(jsonOptions =>
                {
                    jsonOptions.SerializerSettings.ContractResolver = null;
                })
                .ConfigureApiBehaviorOptions(options =>
                {
                    options.InvalidModelStateResponseFactory = context =>
                    {
                        var result = new BadRequestObjectResult(context.ModelState);

                        // TODO: add `using System.Net.Mime;` to resolve MediaTypeNames
                        result.ContentTypes.Add(MediaTypeNames.Application.Json);
                        result.ContentTypes.Add(MediaTypeNames.Application.Xml);

                        return result;
                    };
                });

            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy",
                    builder => builder
                    .AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader());
            });

            services.AddSwaggerGen(c => c.SwaggerDoc("v1", new Microsoft.OpenApi.Models.OpenApiInfo
            {
                Version = "v1",
                Title = "My API"
            }));

            //services.AddSingleton<IExceptionMetadataService>(new ExceptionMetadataService("test.com"));

            InitializeServices(services);
            InitializeAuthServices(services);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public override void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            base.Configure(app, env);

            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "MyAPI");
            });

            app.UseHttpsRedirection();

            app.UseCors("CorsPolicy");

            app.UseRouting();

            app.UseAuthorization();

            InitializeApplication(app, env);

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            InitializeAuthApplication(app, env);
        }

        public override void InitializeServices(IServiceCollection services)
        {
            base.InitializeServices(services);

            services.AddSingleton<ICache, ExtendedMemoryCache>();
            AddDistributedMemoryCache(services);

            //services.AddSingleton<ICache, ExtendedMemoryCache>();
            //AddDistributedSqlServerCache(services);
        }

        private void AddDistributedSqlServerCache(IServiceCollection services)
        {
            string connectionString = ConfigurationManager.ConnectionStrings["PerformanceSqlServer"].ConnectionString;
            string performanceSchemaName = AppSettingsManager.Instance.Get("PerformanceSchemaName", "Perf");
            string defaultCacheTableName = AppSettingsManager.Instance.Get("DefaultCacheTableName", "DefaultCache");

            services
                .AddDistributedSqlServerCache(options =>
                {
                    options.ConnectionString = connectionString;
                    options.SchemaName = performanceSchemaName;
                    options.TableName = defaultCacheTableName;
                });
        }

        private void AddDistributedMemoryCache(IServiceCollection services)
        {
            services.AddDistributedMemoryCache((options) =>
            {

            });
        }

        public override void InitializeApplication(IApplicationBuilder app, IWebHostEnvironment env)
        {
            base.InitializeApplication(app, env);
            InitializeAutofac();
            BuildAutofac();


            UseAuthenticationMiddleware(app);
            CacheProvider.Instance.Initialize(ServiceProvider.GetService<ICache>());

            //TestFactory();

            //TestCaching();
            //Testlogger();

            TestCaching2();

            TestMakingCacheDirtySimple();
        }

        private void TestCaching2()
        {
            CacheProvider.Instance.RegisterRepository("Test", (parameters) =>
            {
                return new Random().Next(0, 100).ToString();
            }, TimeSpan.FromSeconds(3));

            var result1 = CacheProvider.Instance.Retrieve<String>("Test");
            var result2 = CacheProvider.Instance.Retrieve<String>("Test");
        }

        private void TestCaching()
        {
            string result;
            string _testStr = "This is a test from Repo";

            CacheProvider.Instance.RegisterRepository("Test", () =>
            {
                return _testStr;
            });

            result = CacheProvider.Instance.Retrieve<String>("Test");

            _testStr = _testStr + "*";
            result = CacheProvider.Instance.Retrieve<String>("Test");

            CacheProvider.Instance.MakeCacheDirty("Test");
            result = CacheProvider.Instance.Retrieve<String>("Test");

            _testStr = _testStr + "-";
            CacheProvider.Instance.MakeCacheDirty("Test");
            result = CacheProvider.Instance.Retrieve<String>("Test");

            try
            {
                CacheProvider.Instance.Register("Test", "This is a Test");
            }
            catch (Exception ex)
            {

            }

            try
            {
                CacheProvider.Instance.Remove("Test");
            }
            catch (Exception ex)
            {

            }

            try
            {
                CacheProvider.Instance.RegisterRepository("Test", () =>
                {
                    return _testStr;
                });
            }
            catch (Exception ex)
            {

            }

            result = CacheProvider.Instance.Retrieve<String>("Test");

            CacheProvider.Instance.UnregisterRepository("Test");

            CacheProvider.Instance.Register("Test", "This is a Test");
            result = CacheProvider.Instance.Retrieve<String>("Test");

            CacheProvider.Instance.Remove("Test");
            result = CacheProvider.Instance.Retrieve<String>("Test");

            /////////////////////////////////////////////////////////

            CacheProvider.Instance.RegisterRepository("Test_Param", (parameters) =>
            {
                int i = (int)parameters[0];
                return $"{i}: {_testStr}";
            });

            _testStr = _testStr + "*";

            result = CacheProvider.Instance.Retrieve<String>("Test_Param", "1", 1);
            result = CacheProvider.Instance.Retrieve<String>("Test_Param", "2", 2);

            result = CacheProvider.Instance.Retrieve<String>("Test_Param", "1", 1);
            result = CacheProvider.Instance.Retrieve<String>("Test_Param", "2", 2);

            CacheProvider.Instance.MakeCacheDirty("Test_Param", "2");

            _testStr = _testStr + "-";

            result = CacheProvider.Instance.Retrieve<String>("Test_Param", "1", 1);
            result = CacheProvider.Instance.Retrieve<String>("Test_Param", "2", 2);

            CacheProvider.Instance.MakeCacheDirty("Test_Param");

            result = CacheProvider.Instance.Retrieve<String>("Test_Param", "1", 1);
            result = CacheProvider.Instance.Retrieve<String>("Test_Param", "2", 2);

            /////////////////////////////////////////////////////////////

            CacheProvider.Instance.RegisterRepository("##Test_Param", (parameters) =>
            {
                int i = (int)parameters[0];
                return $"{i}: {_testStr}";
            });

            _testStr = _testStr + "*";

            result = CacheProvider.Instance.Retrieve<String>("##Test_Param", "1", 1);
            result = CacheProvider.Instance.Retrieve<String>("##Test_Param", "2", 2);

            result = CacheProvider.Instance.Retrieve<String>("##Test_Param", "1", 1);
            result = CacheProvider.Instance.Retrieve<String>("##Test_Param", "2", 2);

            CacheProvider.Instance.MakeCacheDirty("##Test_Param", "2");

            _testStr = _testStr + "-";

            result = CacheProvider.Instance.Retrieve<String>("##Test_Param", "1", 1);
            result = CacheProvider.Instance.Retrieve<String>("##Test_Param", "2", 2);

            CacheProvider.Instance.MakeCacheDirty("##Test_Param");

            result = CacheProvider.Instance.Retrieve<String>("##Test_Param", "1", 1);
            result = CacheProvider.Instance.Retrieve<String>("##Test_Param", "2", 2);

            /////////////////////////////////////////////////////////////

            CacheProvider.Instance.RegisterRepository(typeof(TestClass), () =>
            {
                return new TestClass { ID = 20 };
            });

            TestClass t1;

            t1 = CacheProvider.Instance.Retrieve<TestClass>(typeof(TestClass));
            t1 = CacheProvider.Instance.Retrieve<TestClass>(typeof(TestClass));

            CacheProvider.Instance.MakeCacheDirty(typeof(TestClass));

            t1 = CacheProvider.Instance.Retrieve<TestClass>(typeof(TestClass));
            t1 = CacheProvider.Instance.Retrieve<TestClass>(typeof(TestClass));
        }

        private void TestMakingCacheDirtySimple()
        {
            string firstName = "Mo";
            string lastName = "Chavoshi";

            CacheProvider.Instance.RegisterRepository("FirstName", () =>
            {
                return firstName;
            });

            CacheProvider.Instance.RegisterRepository("LastName", () =>
            {
                return lastName;
            });

            var name = $"{CacheProvider.Instance.Retrieve<String>("FirstName")} {CacheProvider.Instance.Retrieve<String>("LastName")}";

            firstName = "Mo2";
            lastName = "Chavoshi2";

            CacheProvider.Instance.MakeCacheDirty("FirstName");

            name = $"{CacheProvider.Instance.Retrieve<String>("FirstName")} {CacheProvider.Instance.Retrieve<String>("LastName")}";

            firstName = "Mo3";
            lastName = "Chavoshi3";

            CacheProvider.Instance.MakeCacheDirty("FirstName", "LastName");

            name = $"{CacheProvider.Instance.Retrieve<String>("FirstName")} {CacheProvider.Instance.Retrieve<String>("LastName")}";
        }

        private void TestMakingCacheDirtyComplex1()
        {
            string firstName = "Mo";
            string lastName = "Chavoshi";

            CacheProvider.Instance.RegisterRepository("FirstName", () =>
            {
                return firstName;
            }, "LastName");

            CacheProvider.Instance.RegisterRepository("LastName", () =>
            {
                return lastName;
            });

            var name = $"{CacheProvider.Instance.Retrieve<String>("FirstName")} {CacheProvider.Instance.Retrieve<String>("LastName")}";

            firstName = "Mo2";
            lastName = "Chavoshi2";

            CacheProvider.Instance.MakeCacheDirty("FirstName");

            name = $"{CacheProvider.Instance.Retrieve<String>("FirstName")} {CacheProvider.Instance.Retrieve<String>("LastName")}";

            firstName = "Mo3";
            lastName = "Chavoshi3";

            CacheProvider.Instance.MakeCacheDirty("LastName");

            name = $"{CacheProvider.Instance.Retrieve<String>("FirstName")} {CacheProvider.Instance.Retrieve<String>("LastName")}";
        }

        private void TestMakingCacheDirtyComplex2()
        {
            string firstName = "Mo";
            string lastName = "Chavoshi";

            CacheProvider.Instance.RegisterRepository("FirstName", (parameters) =>
            {
                int id = (int)parameters[0];
                return $"{id}: {firstName}";
            }, "LastName");

            CacheProvider.Instance.RegisterRepository("LastName", (parameters) =>
            {
                int id = (int)parameters[0];
                return $"{id}: {lastName}";
            }, "FirsName");

            string name;

            name = $"{CacheProvider.Instance.Retrieve<String>("FirstName", 1, 1)} {CacheProvider.Instance.Retrieve<String>("LastName", 1, 1)}";
            name = $"{CacheProvider.Instance.Retrieve<String>("FirstName", 2, 2)} {CacheProvider.Instance.Retrieve<String>("LastName", 2, 2)}";

            firstName = "Mo2";
            lastName = "Chavoshi2";

            CacheProvider.Instance.MakeCacheDirty("FirstName");

            name = $"{CacheProvider.Instance.Retrieve<String>("FirstName", 1, 1)} {CacheProvider.Instance.Retrieve<String>("LastName", 1, 1)}";
            name = $"{CacheProvider.Instance.Retrieve<String>("FirstName", 2, 2)} {CacheProvider.Instance.Retrieve<String>("LastName", 2, 2)}";

            firstName = "Mo3";
            lastName = "Chavoshi3";

            CacheProvider.Instance.MakeCacheDirty("LastName");

            name = $"{CacheProvider.Instance.Retrieve<String>("FirstName", 1, 1)} {CacheProvider.Instance.Retrieve<String>("LastName", 1, 1)}";
            name = $"{CacheProvider.Instance.Retrieve<String>("FirstName", 2, 2)} {CacheProvider.Instance.Retrieve<String>("LastName", 2, 2)}";
        }

        private void TestFactory()
        {
            InitializeIRequestServices();
            Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            ApplicationConfigurator.Configure(config);

            var person_service = TestServiceFactory.Instance.Resolve<IPersonService>();
            var city_service = TestServiceFactory.Instance.Resolve<ICityService>();

            var res2 = TestRequestServiceFactory.Instance.Resolve<ITest2RequestService>();

            res2.ProcessRequest(new Test2DataRequest());

            try
            {
                var res = person_service.Get(1);
                var x = person_service.Devide(10, 2);
            }
            catch (Exception ex)
            {

            }
        }

        void InitializeIRequestServices()
        {
            TestRequestServiceFactory.Instance.Configure();
        }

        private void Testlogger()
        {
            TraceLogManager.Instance.WriteInfoAsync("Startup Logging", "Test Message", "THIS IS A TEST DATA FROM STARTUP");
            TraceLogManager.Instance.WriteErrorAsync("Startup Logging", "Test Error");
        }

        protected override void InitializeAuthService(IServiceCollection services)
        {
            base.InitializeAuthService(services);

            var handler = new SocketsHttpHandler
            {
                MaxConnectionsPerServer = 100,
                //PooledConnectionLifetime = TimeSpan.FromMinutes(15),  //This is infinite by default
                PooledConnectionIdleTimeout = TimeSpan.FromMinutes(15),
            };

            services
                .AddHttpClient("SSO", httpClient =>
                {
                    httpClient.BaseAddress = new Uri(AppSettingsManager.Instance.Get("AuthEndpointPath"));
                })
                .ConfigurePrimaryHttpMessageHandler(() => handler);
        }

        protected override void InitializeAuthApplication(IApplicationBuilder app, IWebHostEnvironment env)
        {
            base.InitializeAuthApplication(app, env);

            AuthService.Instance.InitializeWithName(
                ServiceProvider.GetService<IHttpContextAccessor>(),
                "SSO",
                AppSettingsManager.Instance.Get("ClientID"),
                AppSettingsManager.Instance.Get("ClientSecret"));
        }

        private void InitializeAutofac()
        {
            List<Assembly> allAssemblies = new List<Assembly>();
            string path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

            foreach (string dll in Directory.GetFiles(path, "WebApplication1.dll"))
            {
                SampleRequestServiceFactory.Instance.RegisterServices(dll);
            }
        }

        private void BuildAutofac()
        {
            try
            {
                SampleRequestServiceFactory.Instance.Build();
            }
            catch (Exception)
            {
                throw;
            }
        }
    }

    [Serializable]
    public class TestClass
    {
        public int ID { get; set; }
    }
}
