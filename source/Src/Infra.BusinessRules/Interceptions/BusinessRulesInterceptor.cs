using Castle.DynamicProxy;
using DotFramework.Infra.ExceptionHandling;
using System;
using Unity.Interception.PolicyInjection.Pipeline;

namespace DotFramework.Infra.BusinessRules
{
    public class BusinessRulesInterceptor : ExceptionHandlerInterceptor
    {
        public override bool HandleException(ref Exception ex, IInvocation invocation)
        {
            return BusinessRulesExceptionHandler.Instance.HandleException(ref ex, invocation.TargetType.FullName, invocation.Method.Name);
        }
    }
}
