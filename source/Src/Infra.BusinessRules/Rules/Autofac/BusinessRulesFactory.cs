using DotFramework.Core.DependencyInjection;
using System;
using System.IO;
using System.Reflection;
using Autofac;

namespace DotFramework.Infra.BusinessRules.Autofac
{
    public class BusinessRulesFactory<TBusinessRulesFactory> : FactoryBase<TBusinessRulesFactory, RulesBase, BusinessRulesInterceptor>, IBusinessRulesFactory
        where TBusinessRulesFactory : class
    {
        protected override InterceptionMethod InterceptionMethod => InterceptionMethod.Class;

        public void Configure(string dllPath)
        {
            try
            {
                RegisterServices(dllPath);
                Build();
            }
            catch (Exception ex)
            {
                if (BusinessRulesFactoryExceptionHandler.Instance.HandleException(ref ex))
                {
                    throw ex;
                }
            }
        }

        private void RegisterServices(string dllPath)
        {
            Assembly assembly = Assembly.LoadFrom(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, dllPath));
            Register(assembly);
        }

        public TRules GetBusinessRules<TRules>() where TRules : RulesBase, new()
        {
            return Resolve<TRules>();
        }

        protected override bool HandleException(ref Exception ex)
        {
            return BusinessRulesFactoryExceptionHandler.Instance.HandleException(ref ex);
        }
    }
}
