﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotFramework.Infra.BusinessRules
{
    public  interface IBusinessRulesFactory
    {
        TRules GetBusinessRules<TRules>() where TRules : RulesBase, new();
    }
}
