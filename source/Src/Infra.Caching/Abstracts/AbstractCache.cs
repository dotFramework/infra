﻿using DotFramework.Core.Configuration;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotFramework.Infra.Caching.Abstracts
{
    public abstract class AbstractCache : ICache
    {
        private readonly ConcurrentDictionary<String, CacheRepository> _dataRepository = new ConcurrentDictionary<String, CacheRepository>();
        //private readonly ComplexKeyProvider _complexKeyProvider = new ComplexKeyProvider();

        private readonly bool _cacheEnabled;

        public AbstractCache()
        {
            _cacheEnabled = AppSettingsManager.Instance.Get<Boolean>("CacheEnabled", true);
        }

        public void RegisterRepository(object key, Func<Object> func, object[] dependentRepositories)
        {
            RegisterRepository(key, func, TimeSpan.Zero, dependentRepositories);
        }

        public void RegisterRepository(object key, Func<Object> func, TimeSpan expiry, object[] dependentRepositories)
        {
            RegisterRepository(key, _ => { return func(); }, expiry, dependentRepositories);
        }

        public void RegisterRepository(object key, Func<Object[], Object> func, object[] dependentRepositories)
        {
            RegisterRepository(key, func, TimeSpan.Zero, dependentRepositories);
        }

        public void RegisterRepository(object key, Func<Object[], Object> func, TimeSpan expiry, object[] dependentRepositories)
        {
            //if (TryGetValue(key.ToString(), out CacheItem _))
            //{
            //    throw new NotSupportedException("This repository cannot be registered as there is an item registered with the same key");
            //}

            if (!_dataRepository.ContainsKey(key.ToString()))
            {
                _dataRepository.TryAdd(key.ToString(), new CacheRepository(key, func, expiry, dependentRepositories));
            }
            else
            {
                throw new NotSupportedException("There is already a repository registered with this key");
            }
        }

        public void UnregisterRepository(object key)
        {
            if (_dataRepository.ContainsKey(key.ToString()))
            {
                _dataRepository.TryRemove(key.ToString(), out _);
            }
        }

        public void Register<T>(object key, T value) where T : class
        {
            Register(key, value, TimeSpan.Zero);
        }

        public void Register<T>(object key, T value, TimeSpan expiry) where T : class
        {
            if (_dataRepository.ContainsKey(key.ToString()))
            {
                throw new NotSupportedException("This item cannot be registered as there is a cache repository available for it");
            }

            RegisterComplexKey("_", key);
            InternalRegister(key.ToString(), value, expiry);
        }

        private void InternalRegister<T>(string key, T value, TimeSpan expiry) where T : class
        {
            TryRemoveValue(key);
            SetValue(key, new CacheItem<T>(value), expiry);
        }

        public T Retrieve<T>(object key) where T : class
        {
            return Retrieve<T>(key, null);
        }

        public T Retrieve<T>(object key, object identifier, params object[] parameters) where T : class
        {
            string dataRepositoryKey = key.ToString();
            string cacheKey = key.ToString();

            if (identifier != null)
            {
                cacheKey = GenerateComplexCacheKey(dataRepositoryKey, identifier);
            }

            if (_cacheEnabled)
            {
                if (!TryGetValue(cacheKey, out CacheItem<T> cacheItem) || cacheItem.IsDirty)
                {
                    if (_dataRepository.ContainsKey(dataRepositoryKey))
                    {
                        var repo = _dataRepository[dataRepositoryKey];

                        T value = repo.Function(parameters) as T;
                        InternalRegister(cacheKey, value, repo.Expiry);

                        if (identifier != null)
                        {
                            RegisterComplexKey(dataRepositoryKey, identifier);
                        }

                        return value;
                    }
                    else
                    {
                        if (cacheItem != null && cacheItem.IsDirty)
                        {
                            throw new KeyNotFoundException($"Cache is not registered.");
                        }
                    }
                }

                return cacheItem?.Value;
            }
            else
            {
                if (_dataRepository.ContainsKey(dataRepositoryKey))
                {
                    var repo = _dataRepository[dataRepositoryKey];
                    T value = repo.Function(parameters) as T;

                    return value;
                }
                else
                {
                    throw new KeyNotFoundException($"Cache is not registered.");
                }
            }
        }

        public void Remove(object key)
        {
            if (_dataRepository.ContainsKey(key.ToString()))
            {
                throw new NotSupportedException("This item cannot be removed as there is a cache repository available for it");
            }

            TryRemoveValue(key.ToString());
        }

        public void MakeCacheDirty(object[] keys)
        {
            Parallel.ForEach(keys, key =>
            {
                InternalMakeCacheDirty(key.ToString(), null);
            });
        }

        public void MakeCacheDirty(object key, object identifier)
        {
            InternalMakeCacheDirty(key.ToString(), identifier.ToString());
        }

        private void InternalMakeCacheDirty(string key, string identifier)
        {
            string dataRepositoryKey = key;
            string cacheKey = key;

            if (identifier != null)
            {
                cacheKey = GenerateComplexCacheKey(dataRepositoryKey, identifier);
                TryRemoveValue(cacheKey);
            }
            else
            {
                var complexKeys = GetComplexKeys(dataRepositoryKey);

                if (complexKeys?.Any() == true)
                {
                    Parallel.ForEach(complexKeys, k =>
                    {
                        TryRemoveValue(GenerateComplexCacheKey(dataRepositoryKey, k));
                    });

                    UnregisterComplexKey(dataRepositoryKey);
                }
                else
                {
                    TryRemoveValue(cacheKey);
                }
            }

            //if (!_dataRepository.ContainsKey(key))
            //{
            //    if (TryGetValue(key, out CacheItem cacheItem))
            //    {
            //        cacheItem.IsDirty = true;
            //    }
            //}
            //else
            //{
            //    if (_complexKeyProvider.ContainsKey(key))
            //    {
            //        Parallel.ForEach(_complexKeyProvider.Get(key), complexKey =>
            //        {
            //            if (TryGetValue(complexKey, out CacheItem cacheItem))
            //            {
            //                cacheItem.IsDirty = true;
            //            }
            //        });
            //    }
            //    else
            //    {
            //        if (TryGetValue(key, out CacheItem cacheItem))
            //        {
            //            cacheItem.IsDirty = true;
            //        }
            //    }

            //    //var repository = _dataRepository[key];

            //    //if (repository?.DependentRepositories != null)
            //    //{
            //    //    Parallel.ForEach(repository.DependentRepositories, dependentRepository =>
            //    //    {
            //    //        MakeCacheDirty(dependentRepository);
            //    //    });
            //    //}
            //}
        }

        protected abstract void SetValue<T>(string key, CacheItem<T> value, TimeSpan expiry) where T : class;

        protected abstract bool TryGetValue(string key, out CacheItem value);

        protected abstract bool TryGetValue<T>(string key, out CacheItem<T> value) where T : class;

        protected abstract void TryRemoveValue(string key);

        protected virtual void SetKeySet(string key, CacheItem<HashSet<string>> keySet)
        {
            SetValue(key, keySet, TimeSpan.Zero);
        }

        protected virtual CacheItem<HashSet<string>> GetKeySet(string key)
        {
            TryGetValue(key, out CacheItem<HashSet<string>> keySet);
            return keySet;
        }

        protected virtual void RemoveKeySet(string key)
        {
            TryRemoveValue(key);
        }

        public void Flush()
        {
            Parallel.ForEach(_dataRepository, (repo) =>
            {
                InternalMakeCacheDirty(repo.Key, null);
            });

            InternalMakeCacheDirty("_", null);
        }

        private string GenerateComplexCacheKey(object dataRepositoryKey, object identifier)
        {
            return $"{dataRepositoryKey}%%{identifier}";
        }

        private void RegisterComplexKey(object dataRepositoryKey, object identifier)
        {
            var key = $"%%{dataRepositoryKey}";

            var keysSet = GetKeySet(key);

            bool changed = false;

            if (keysSet == null)
            {
                var keys = new HashSet<String> { identifier.ToString() };
                keysSet = new CacheItem<HashSet<String>>(keys);

                changed = true;
            }
            else
            {
                if (!keysSet.Value.Contains(identifier))
                {
                    keysSet.Value.Add(identifier.ToString());

                    changed = true;
                }
            }

            if (changed)
            {
                SetKeySet(key, keysSet);
            }
        }

        private void UnregisterComplexKey(object dataRepositoryKey, object identifier)
        {
            var key = $"%%{dataRepositoryKey}";

            var keysSet = GetKeySet(key);

            if (keysSet != null && keysSet.Value.Contains(identifier))
            {
                keysSet.Value.Remove(identifier.ToString());
                SetKeySet(key, keysSet);
            }
        }

        private void UnregisterComplexKey(object dataRepositoryKey)
        {
            var key = $"%%{dataRepositoryKey}";
            TryRemoveValue(key);
        }

        private HashSet<String> GetComplexKeys(object dataRepositoryKey)
        {
            var key = $"%%{dataRepositoryKey}";
            var keysSet = GetKeySet(key);

            return keysSet?.Value;
        }
    }

    internal sealed class ComplexKey
    {
        public ComplexKey(object key, object identifier)
        {
            Key = key;
            Identifier = identifier;
        }

        public object Key { get; set; }
        public object Identifier { get; set; }

        public string Serialize()
        {
            return $"{Key}%%{Identifier}";
        }
    }

    internal sealed class ComplexKeyProvider
    {
        private readonly ConcurrentDictionary<Object, ConcurrentDictionary<Object, ComplexKey>> _dictionary = new ConcurrentDictionary<Object, ConcurrentDictionary<Object, ComplexKey>>();

        public string Get(object key, object identifier)
        {
            if (_dictionary.ContainsKey(key))
            {
                if (_dictionary[key].ContainsKey(identifier))
                {
                    return _dictionary[key][identifier].Serialize();
                }
            }
            else
            {
                _dictionary.TryAdd(key, new ConcurrentDictionary<Object, ComplexKey>());
            }

            var complexKey = new ComplexKey(key, identifier);
            _dictionary[key].TryAdd(identifier, complexKey);

            return complexKey.Serialize();
        }

        public IList<String> Get(object key)
        {
            if (_dictionary.ContainsKey(key))
            {
                return _dictionary[key].Values.Select(v => v.Serialize()).ToListOrDefault();
            }
            else
            {
                return null;
            }
        }

        public bool ContainsKey(object key, object identifier)
        {
            return _dictionary.ContainsKey(key) && _dictionary[key].ContainsKey(identifier);
        }

        public bool ContainsKey(object key)
        {
            return _dictionary.ContainsKey(key);
        }

        public void Remove(object key)
        {
            if (_dictionary.ContainsKey(key))
            {
                _dictionary.TryRemove(key, out _);
            }
        }
    }
}
