﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotFramework.Infra.Caching
{
    public interface ICache
    {
        void RegisterRepository(object key, Func<Object> func, object[] dependentRepositories);
        void RegisterRepository(object key, Func<Object> func, TimeSpan expiry, object[] dependentRepositories);
        void RegisterRepository(object key, Func<Object[], Object> func, object[] dependentRepositories);
        void RegisterRepository(object key, Func<Object[], Object> func, TimeSpan expiry, object[] dependentRepositories);
        void UnregisterRepository(object key);
        void Register<T>(object key, T value) where T : class;
        void Register<T>(object key, T value, TimeSpan expiry) where T : class;
        T Retrieve<T>(object key) where T : class;
        T Retrieve<T>(object key, object identifier, params object[] parameters) where T : class;
        void Remove(object key);
        void MakeCacheDirty(object[] keys);
        void MakeCacheDirty(object key, object identifier);
        void Flush();
    }
}
