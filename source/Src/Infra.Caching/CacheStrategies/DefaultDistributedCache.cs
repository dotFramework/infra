﻿using DotFramework.Infra.Caching.Abstracts;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Caching.Memory;
using System;

namespace DotFramework.Infra.Caching
{
    public class DefaultDistributedCache : AbstractCache
    {
        private readonly IDistributedCache _cache;

        public DefaultDistributedCache(IDistributedCache cache)
        {
            _cache = cache;
        }

        protected override void SetValue<T>(string key, CacheItem<T> value, TimeSpan expiry)
        {
            var cacheEntryOptions = new DistributedCacheEntryOptions();

            if (expiry != TimeSpan.Zero)
            {
                cacheEntryOptions.SetSlidingExpiration(expiry);
            }

            _cache.Set(key, value, cacheEntryOptions);
        }

        protected override bool TryGetValue(string key, out CacheItem value)
        {
            return _cache.TryGetValue(key, out value);
        }

        protected override bool TryGetValue<T>(string key, out CacheItem<T> value)
        {
            return _cache.TryGetValue(key, out value);
        }

        protected override void TryRemoveValue(string key)
        {
            _cache.Remove(key);
        }
    }
}
