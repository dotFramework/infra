﻿using DotFramework.Infra.Caching.Abstracts;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Caching.Memory;
using System;

namespace DotFramework.Infra.Caching
{
    public class DefaultMemoryCache : AbstractCache
    {
        private readonly IMemoryCache _cache;

        public DefaultMemoryCache(IMemoryCache cache)
        {
            _cache = cache;
        }

        protected override void SetValue<T>(string key, CacheItem<T> value, TimeSpan expiry)
        {
            var cacheEntryOptions = new MemoryCacheEntryOptions();

            if (expiry != TimeSpan.Zero)
            {
                cacheEntryOptions.SetSlidingExpiration(expiry);
                value.ExpiresAt = DateTime.UtcNow.Add(expiry);
            }

            _cache.Set(key, value, cacheEntryOptions);
        }

        protected override bool TryGetValue(string key, out CacheItem value)
        {
            if (_cache.TryGetValue(key, out value))
            {
                if (value?.ExpiresAt < DateTime.UtcNow)
                {
                    value.IsDirty = true;
                }

                return true;
            }
            else
            {
                return false;
            }
        }

        protected override bool TryGetValue<T>(string key, out CacheItem<T> value)
        {
            if (_cache.TryGetValue(key, out value))
            {
                if (value?.ExpiresAt < DateTime.UtcNow)
                {
                    value.IsDirty = true;
                }

                return true;
            }
            else
            {
                return false;
            }
        }

        protected override void TryRemoveValue(string key)
        {
            if (_cache.TryGetValue(key, out CacheItem _))
            {
                _cache.Remove(key);
            }
        }
    }
}
