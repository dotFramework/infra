﻿using DotFramework.Infra.Caching.Abstracts;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;

namespace DotFramework.Infra.Caching
{
    public class DictionaryCache : AbstractCache
    {
        private readonly ConcurrentDictionary<string, object> _cache;

        public DictionaryCache()
        {
            _cache = new ConcurrentDictionary<string, object>();
        }

        protected override void SetValue<T>(string key, CacheItem<T> value, TimeSpan expiry)
        {
            _cache.TryAdd(key, value);
        }

        protected override bool TryGetValue(string key, out CacheItem value)
        {
            if (_cache.TryGetValue(key, out object obj))
            {
                value = (CacheItem)obj;
                return true;
            }
            else
            {
                value = null;
                return false;
            }
        }

        protected override bool TryGetValue<T>(string key, out CacheItem<T> value)
        {
            if (_cache.TryGetValue(key, out object obj))
            {
                value = (CacheItem<T>)obj;
                return true;
            }
            else
            {
                value = null;
                return false;
            }
        }

        protected override void TryRemoveValue(string key)
        {
            _cache.TryRemove(key, out _);
        }
    }
}
