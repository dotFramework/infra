﻿using DotFramework.Core.Configuration;
using DotFramework.Infra.Caching.Abstracts;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Timers;

namespace DotFramework.Infra.Caching
{
    public class ExtendedMemoryCache : AbstractCache, IDisposable
    {
        private readonly IMemoryCache _cache;
        private readonly IDistributedCache _distributedCache;
        private readonly string _agentIdentifier;
        private readonly TimeSpan _agentsStatusInterval;
        private readonly ConcurrentDictionary<String, DateTime> _agentsKeys;
        private ConcurrentDictionary<String, DateTime> _availableKeys;

        private readonly Timer _chackStatusTimer;

        public ExtendedMemoryCache(IMemoryCache cache, IDistributedCache distributedCache)
        {
            _cache = cache;
            _distributedCache = distributedCache;

            _agentIdentifier = AppSettingsManager.Instance.Get("CacheAgentIdentifier", Guid.NewGuid().ToString());

            _agentsKeys = new ConcurrentDictionary<String, DateTime>();
            _agentsStatusInterval = AppSettingsManager.Instance.Get<TimeSpan>("AgentsStatusInterval", TimeSpan.FromSeconds(60));

            _availableKeys = new ConcurrentDictionary<String, DateTime>();

            _chackStatusTimer = CreateCheckStatusTimer(_agentsStatusInterval);
        }

        private Timer CreateCheckStatusTimer(TimeSpan agentsStatusInterval)
        {
            Timer timer = new Timer(agentsStatusInterval.TotalMilliseconds);

            timer.Elapsed += ChackStatusTimer_Elapsed;
            timer.Start();

            return timer;
        }

        private void ChackStatusTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            CheckStatus();
        }

        protected override void SetValue<T>(string key, CacheItem<T> value, TimeSpan expiry)
        {
            var cacheEntryOptions = new MemoryCacheEntryOptions();

            if (expiry != TimeSpan.Zero)
            {
                cacheEntryOptions.SetSlidingExpiration(expiry);
                value.ExpiresAt = DateTime.UtcNow.Add(expiry);
            }

            _cache.Set(key, value, cacheEntryOptions);

            SetAgent(key);
        }

        private void SetAgent(string key)
        {
            if (CacheIsNotDistributable(key))
            {
                return;
            }

            _agentsKeys.TryAdd(key, DateTime.Now);

            string agentsCacheKey = GetAgentsCacheKey(key);
            _distributedCache.TryGetValue(key, out string agentsStr);

            bool changed = false;

            if (String.IsNullOrEmpty(agentsStr))
            {
                agentsStr = _agentIdentifier;
                changed = true;
            }
            else
            {
                if (!agentsStr.Split(';').Contains(_agentIdentifier))
                {
                    agentsStr += $";{_agentIdentifier}";
                    changed = true;
                }
            }

            if (changed)
            {
                SetValueDistributed(agentsCacheKey, agentsStr, TimeSpan.Zero);
            }

            _availableKeys.TryAdd(key, DateTime.Now);
        }

        private bool CheckCacheAgent(string key)
        {
            if (CacheIsNotDistributable(key))
            {
                return true;
            }

            return _availableKeys.ContainsKey(key);
        }

        private bool CheckStatus(string key)
        {
            string agentsCacheKey = GetAgentsCacheKey(key);

            _distributedCache.TryGetValue(agentsCacheKey, out string agentsStr);
            return agentsStr?.Split(';')?.Contains(_agentIdentifier) == true;
        }

        private void CheckStatus()
        {
            List<String> keys = new List<String>();

            Parallel.ForEach(_agentsKeys, agentKey =>
            {
                var key = agentKey.Key;

                if (CheckStatus(key))
                {
                    keys.Add(agentKey.Key);
                }
            });

            if (keys.Any())
            {
                _availableKeys = new ConcurrentDictionary<String, DateTime>(keys.Select(k => new KeyValuePair<String, DateTime>(k, DateTime.Now)));
            }
            else
            {
                _availableKeys = new ConcurrentDictionary<String, DateTime>();
            }
        }

        private string GetAgentsCacheKey(string key)
        {
            return $"{key}_Agents";
        }

        protected override bool TryGetValue(string key, out CacheItem value)
        {
            if (!CheckCacheAgent(key))
            {
                InternalRemove(key);

                value = null;
                return false;
            }

            if (_cache.TryGetValue(key, out value))
            {
                if (value?.ExpiresAt < DateTime.UtcNow)
                {
                    value.IsDirty = true;
                }

                return true;
            }
            else
            {
                return false;
            }
        }

        protected override bool TryGetValue<T>(string key, out CacheItem<T> value)
        {
            if (!CheckCacheAgent(key))
            {
                InternalRemove(key);

                value = null;
                return false;
            }

            if (_cache.TryGetValue(key, out value))
            {
                if (value?.ExpiresAt < DateTime.UtcNow)
                {
                    value.IsDirty = true;
                }

                return true;
            }
            else
            {
                return false;
            }
        }

        protected override void TryRemoveValue(string key)
        {
            if (!CacheIsNotDistributable(key))
            {
                TryRemoveValueDistributed(GetAgentsCacheKey(key));
                _agentsKeys.TryRemove(key, out _);
            }

            InternalRemove(key);
        }

        private void InternalRemove(string key)
        {
            if (_cache.TryGetValue(key, out CacheItem _))
            {
                _cache.Remove(key);
            }
        }

        private void SetValueDistributed(string key, object value, TimeSpan expiry)
        {
            var cacheEntryOptions = new DistributedCacheEntryOptions();

            if (expiry != TimeSpan.Zero)
            {
                cacheEntryOptions.SetSlidingExpiration(expiry);
            }

            _distributedCache.Set(key, value, cacheEntryOptions);
        }

        private bool TryGetValueDistributed(string key, out CacheItem value)
        {
            return _distributedCache.TryGetValue(key, out value);
        }

        private bool TryGetValueDistributed<T>(string key, out CacheItem<T> value) where T : class
        {
            return _distributedCache.TryGetValue(key, out value);
        }

        private void TryRemoveValueDistributed(string key)
        {
            if (_distributedCache.TryGetValue(key, out CacheItem _))
            {
                _distributedCache.Remove(key);
            }
        }

        protected override void SetKeySet(string key, CacheItem<HashSet<string>> keySet)
        {
            if (CacheIsNotDistributable(key))
            {
                base.SetKeySet(key, keySet);
            }
            else
            {
                var strKeySet = String.Join(';', keySet.Value);
                SetValueDistributed(key, strKeySet, TimeSpan.Zero);
            }
        }

        protected override CacheItem<HashSet<string>> GetKeySet(string key)
        {
            if (CacheIsNotDistributable(key))
            {
                return base.GetKeySet(key);
            }

            _distributedCache.TryGetValue(key, out string strKeySet);
            return new CacheItem<HashSet<string>>(strKeySet?.Split(';')?.ToHashSet() ?? new HashSet<String>());
        }

        protected override void RemoveKeySet(string key)
        {
            if (CacheIsNotDistributable(key))
            {
                base.RemoveKeySet(key);
            }
            else
            {
                TryRemoveValueDistributed(key);
            }
        }

        private bool CacheIsNotDistributable(string key)
        {
            return key.StartsWith("##") || key.StartsWith("%%##");
        }

        public void Dispose()
        {
            _chackStatusTimer.Dispose();
        }
    }
}
