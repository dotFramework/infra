﻿using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace Microsoft.Extensions.Caching.Distributed
{
    internal static class DistributedCacheExtensions
    {
        public static bool TryGetValue<TItem>(this IDistributedCache cache, string key, out TItem value)
        {
            string strKey = key.ToString();
            var result = cache.Get(strKey);

            if (result == null)
            {
                value = default;
                return false;
            }
            else
            {
                try
                {
                    value = (TItem)ByteArrayToObject(result);
                    return true;
                }
                catch (Exception)
                {
                    value = default;
                    return false;
                }
            }
        }

        public static void Set(this IDistributedCache cache, string key, object value, DistributedCacheEntryOptions options)
        {
            string strKey = key.ToString();
            byte[] byteValue = ObjectToByteArray(value);

            cache.Set(strKey, byteValue, options);
        }

        public static void Remove(this IDistributedCache cache, string key)
        {
            string strKey = key.ToString();
            cache.Remove(strKey);
        }

        private static byte[] ObjectToByteArray(object obj)
        {
            BinaryFormatter bf = new BinaryFormatter();
            using (var ms = new MemoryStream())
            {
                bf.Serialize(ms, obj);
                return ms.ToArray();
            }
        }

        private static object ByteArrayToObject(byte[] arrBytes)
        {
            using (var memStream = new MemoryStream())
            {
                var binForm = new BinaryFormatter();
                memStream.Write(arrBytes, 0, arrBytes.Length);
                memStream.Seek(0, SeekOrigin.Begin);
                var obj = binForm.Deserialize(memStream);
                return obj;
            }
        }
    }
}
