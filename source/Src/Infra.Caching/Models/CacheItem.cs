﻿using System;

namespace DotFramework.Infra.Caching
{
    [Serializable]
    public class CacheItem
    {
        public bool IsDirty { get; set; }
        internal DateTime? ExpiresAt { get; set; }
    }

    [Serializable]
    public class CacheItem<T> : CacheItem where T: class
    {
        public CacheItem(T value)
        {
            Value = value;
        }

        public T Value { get; set; }
    }
}
