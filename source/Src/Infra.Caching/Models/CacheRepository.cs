﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotFramework.Infra.Caching
{
    internal class CacheRepository
    {
        public CacheRepository(object key, Func<Object[], Object> func, object[] dependentRepositories) : this(key, func, TimeSpan.Zero, false, dependentRepositories)
        {

        }

        public CacheRepository(object key, Func<Object[], Object> func, bool disableDistribution, object[] dependentRepositories) : this(key, func, TimeSpan.Zero, disableDistribution, dependentRepositories)
        {

        }

        public CacheRepository(object key, Func<Object[], Object> func, TimeSpan expiry, object[] dependentRepositories) : this(key, func, expiry, false, dependentRepositories)
        {

        }

        public CacheRepository(object key, Func<Object[], Object> func, TimeSpan expiry, bool disableDistribution, object[] dependentRepositories)
        {
            Key = key;
            Function = func;
            Expiry = expiry;
            DisableDistribution = disableDistribution;
            DependentRepositories = dependentRepositories;
        }

        public object Key { get; set; }
        public Func<Object[], Object> Function { get; set; }
        public TimeSpan Expiry { get; set; }
        public bool DisableDistribution { get; set; }
        public object[] DependentRepositories { get; set; }
    }
}
