﻿using DotFramework.Core;
using DotFramework.Infra.Caching.Abstracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotFramework.Infra.Caching
{
    public class CacheProvider : SingletonProvider<CacheProvider>, ICache
    {
        private ICache _cache;

        private CacheProvider()
        {

        }

        public void Initialize(ICache cache)
        {
            _cache = cache ?? throw new ArgumentNullException(nameof(cache));
        }

        public void RegisterRepository(object key, Func<Object> func, params object[] dependentRepositories)
        {
            EnsureDeoendentRepositoryIsNotUsed(dependentRepositories);
            _cache.RegisterRepository(key, func, dependentRepositories);
        }

        public void RegisterRepository(object key, Func<Object> func, TimeSpan expiry, params object[] dependentRepositories)
        {
            EnsureDeoendentRepositoryIsNotUsed(dependentRepositories);
            _cache.RegisterRepository(key, func, expiry, dependentRepositories);
        }

        public void RegisterRepository(object key, Func<Object[], Object> func, params object[] dependentRepositories)
        {
            EnsureDeoendentRepositoryIsNotUsed(dependentRepositories);
            _cache.RegisterRepository(key, func, dependentRepositories);
        }

        public void RegisterRepository(object key, Func<Object[], Object> func, TimeSpan expiry, params object[] dependentRepositories)
        {
            EnsureDeoendentRepositoryIsNotUsed(dependentRepositories);
            _cache.RegisterRepository(key, func, expiry, dependentRepositories);
        }

        private void EnsureDeoendentRepositoryIsNotUsed(object[] dependentRepositories)
        {
            if (dependentRepositories != null && dependentRepositories.Any())
            {
                throw new NotSupportedException("Deoendent Repository is not supported yet");
            }
        }

        public void UnregisterRepository(object key)
        {
            _cache.UnregisterRepository(key);
        }

        public void Register<T>(object key, T value) where T : class
        {
            _cache.Register(key, value);
        }

        public void Register<T>(object key, T value, TimeSpan expiry) where T : class
        {
            _cache.Register(key, value, expiry);
        }

        public T Retrieve<T>(object key) where T : class
        {
            return _cache.Retrieve<T>(key);
        }

        public T Retrieve<T>(object key, object identifier, params object[] parameters) where T : class
        {
            return _cache.Retrieve<T>(key, identifier, parameters);
        }

        public void Remove(object key)
        {
            _cache.Remove(key);
        }

        public void MakeCacheDirty(params object[] keys)
        {
            _cache.MakeCacheDirty(keys);
        }

        public void MakeCacheDirty(object key, object identifier)
        {
            _cache.MakeCacheDirty(key, identifier);
        }

        public void Flush()
        {
            _cache.Flush();
        }
    }
}
