using Autofac;
using DotFramework.Core;
using DotFramework.Core.DependencyInjection;
using DotFramework.Infra.Configuration;
using System;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;

namespace DotFramework.Infra.Caching.Autofac
{
    public class SimpleCacheBase<TCache> : FactoryBase<TCache, Object>
        where TCache : class
    {
        protected override InterceptionMethod InterceptionMethod => InterceptionMethod.Interface;
        protected ModelConfigSection ModelSection { get; set; }

        public void Configure(string sectionName)
        {

            try
            {
                ModelSection = (ModelConfigSection)ConfigurationManager.GetSection(sectionName);

                RegisterServices();
                Build();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool IsAllowedCache<T>()
        {
            return this.Container.IsRegistered<T>();
        }

        public T GetCache<T>()
        {
            return Resolve<T>();
        }

        private void RegisterServices()
        {
            if (ModelSection != null)
            {
                foreach (ModelElement model in ModelSection.Models.Cast<ModelElement>().Where(d => d.AllowCache))
                {
                    object obj = CreateAssemblyProxy(model.CollectionType, ModelSection.DllPath);

                    if (obj != null)
                    {
                        Register(obj, obj.GetType());
                    }
                }
            }
        }

        private object CreateAssemblyProxy(string serviceType, string dllPath)
        {
            Assembly assembly = Assembly.LoadFrom(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, dllPath));
            object service = assembly.CreateInstance(serviceType);

            return service;
        }

        private Type CreateType(string typeName, string dllPath)
        {
            Assembly assembly = Assembly.LoadFrom(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, dllPath));
            return assembly.GetType(typeName);
        }

        protected override bool HandleException(ref Exception ex)
        {
            return true;
        }
    }
}
