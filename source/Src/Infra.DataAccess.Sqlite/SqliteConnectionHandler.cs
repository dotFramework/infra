#if NETCOREAPP3_1
using Microsoft.Data.Sqlite;
#else
using System.Data.SQLite;
#endif
using System;
using System.Data.Common;
using System.Threading;

namespace DotFramework.Infra.DataAccess.Sqlite
{
    public class SqliteConnectionHandler : ConnectionHandler
    {
        public SqliteConnectionHandler() : base()
        {

        }

        public SqliteConnectionHandler(string connectionString) : base(connectionString)
        {
        }

        protected override DbConnection GetConnection(string connectionString)
        {
#if NETCOREAPP3_1
            DbConnection connection = new SqliteConnection(ConnectionString);
#else
            DbConnection connection = new SQLiteConnection(ConnectionString);
#endif

            const int maxRetryAttempt = 10;
            int retryAttempt = 1;

            while (true)
            {
                try
                {
                    connection.Open();
                    break;
                }
#if NETCOREAPP3_1
                catch (SqliteException ex)
#else
                catch (SQLiteException ex)
#endif
                {
                    if (ex.ErrorCode == (int)SQLiteErrorCode.Busy)
                    {
                        if (retryAttempt == maxRetryAttempt)
                        {
                            throw;
                        }

                        retryAttempt++;
                        Thread.Sleep(200);
                    }
                    else
                    {
                        throw;
                    }
                }
                catch (Exception)
                {
                    throw;
                }
            }

            return connection;
        }
    }
}
