using DotFramework.Infra.DataAccessFactory;
using System.Data;
#if NETCOREAPP3_1
using Microsoft.Data.Sqlite;
#else
using System.Data.SQLite;
#endif
using System.Data.Common;

namespace DotFramework.Infra.DataAccess.Sqlite
{
    public abstract class SqliteGeneralDataAccessBase : GeneralDataAccessBase
    {
        protected override IConnectionHandler CreateConnectionHandler()
        {
            return new SqliteConnectionHandler();
        }

        protected override DbParameter CreateParameter(string parameterName, object value)
        {
#if NETCOREAPP3_1
            return new SqliteParameter { ParameterName = parameterName, Value = value };
#else
            return new SQLiteParameter { ParameterName = parameterName, Value = value };
#endif
        }

        protected override DbParameter CreateParameter(string parameterName, DbType dbType, object value)
        {
#if NETCOREAPP3_1
            return new SqliteParameter { ParameterName = parameterName, DbType = dbType, Value = value };
#else
            return new SQLiteParameter { ParameterName = parameterName, DbType = dbType, Value = value };
#endif
        }
    }
}
