using DotFramework.Core;
using DotFramework.Core.DependencyInjection;
using DotFramework.Infra.Configuration;
using System;
using System.Configuration;
using System.IO;
using System.Reflection;

namespace DotFramework.Infra.DataAccessFactory.Autofac
{
    public class DataAccessFactoryBase<TDataAccessFacory> : FactoryBase<TDataAccessFacory, IDataAccessBase, DataAccessInterceptor> 
        where TDataAccessFacory : class
    {
        protected override InterceptionMethod InterceptionMethod => InterceptionMethod.Interface;
        protected DataAccessConfigSection DataAccessSection { get; set; }

        public void Configure(string sectionName)
        {
            try
            {
                DataAccessSection = (DataAccessConfigSection)ConfigurationManager.GetSection(sectionName);

                RegisterServices();
                Build();
            }
            catch (Exception ex)
            {
                if (DataAccessFactoryExceptionHandler.Instance.HandleException(ref ex))
                {
                    throw ex;
                }
            }
        }

        private void RegisterServices()
        {
            foreach (DataAccessServiceElement dataAccessElement in DataAccessSection.DataAccessServices)
            {
                ConnectionElement connectionElement = dataAccessElement.Connection != null ? dataAccessElement.Connection : DataAccessSection.Connection;

                string connectionString = String.Empty;

                if (connectionElement.IsEncrypted)
                {
                    connectionString = EncryptUtility.DecryptText(connectionElement.ConnectionString);
                }
                else
                {
                    connectionString = connectionElement.ConnectionString;
                }

                object dataAccess = CreateAssemblyProxy(dataAccessElement.ServiceType, DataAccessSection.DllPath);
                (dataAccess as IDataAccessBase).SetConnectionString(connectionString);

                if (dataAccess != null)
                {
                    Register(dataAccess, CreateType(dataAccessElement.ContractType, DataAccessSection.FactoryDllPath));
                }
            }
        }

        private object CreateAssemblyProxy(string serviceType, string dllPath)
        {
            Assembly assembly = Assembly.LoadFrom(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, dllPath));
            object service = assembly.CreateInstance(serviceType);

            return service;
        }

        private Type CreateType(string typeName, string dllPath)
        {
            Assembly assembly = Assembly.LoadFrom(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, dllPath));
            return assembly.GetType(typeName);
        }

        public IDataAccess GetDataAccess<IDataAccess>() where IDataAccess : IDataAccessBase
        {
            return Resolve<IDataAccess>();
        }

        public virtual void ChangeConnection(string connectionName, string connectionString, bool isEncrypted = false)
        {
            var configuration = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

            ((ConnectionConfigSection)configuration.GetSection("connectionConfigSection")).Connections[connectionName].ConnectionString = connectionString;
            ((ConnectionConfigSection)configuration.GetSection("connectionConfigSection")).Connections[connectionName].IsEncrypted = isEncrypted;

            configuration.Save(ConfigurationSaveMode.Modified);
            ConfigurationManager.RefreshSection("connectionConfigSection");
        }

        protected override bool HandleException(ref Exception ex)
        {
            return DataAccessFactoryExceptionHandler.Instance.HandleException(ref ex);
        }
    }
}
