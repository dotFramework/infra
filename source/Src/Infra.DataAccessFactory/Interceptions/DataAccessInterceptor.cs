using Castle.DynamicProxy;
using DotFramework.Infra.ExceptionHandling;
using System;
using Unity.Interception.PolicyInjection.Pipeline;

namespace DotFramework.Infra.DataAccessFactory
{
    public class DataAccessInterceptor : ExceptionHandlerInterceptor
    {
        public override bool HandleException(ref Exception ex, IInvocation invocation)
        {
            return DataAccessExceptionHandler.Instance.HandleException(ref ex, invocation.TargetType.FullName, invocation.Method.Name);
        }
    }
}
