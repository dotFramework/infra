using DotFramework.Core.Configuration;
using DotFramework.Core.Serialization;
using System;
using System.Collections.Generic;
using System.Runtime.ExceptionServices;
using Unity.Interception.InterceptionBehaviors;
using Unity.Interception.PolicyInjection.Pipeline;

namespace DotFramework.Infra.ExceptionHandling
{
    public class ExceptionHandlerInterceptionBehavior : IInterceptionBehavior
    {
        private static readonly bool _logArguments;

        static ExceptionHandlerInterceptionBehavior()
        {
            _logArguments = AppSettingsManager.Instance.Get<Boolean>("LogArguments", false);
        }

        public virtual IMethodReturn Invoke(IMethodInvocation input, GetNextInterceptionBehaviorDelegate getNext)
        {
            IMethodReturn result = null;

            try
            {
                result = getNext()(input, getNext);

                if (result.Exception != null)
                {
                    ExceptionDispatchInfo.Capture(result.Exception).Throw();
                }

                return result;
            }
            catch (Exception ex)
            {
                CaptureArguments(input, ex);

                if (HandleException(ref ex, input))
                {
                    throw ex;
                }

                return null;
            }
        }

        private void CaptureArguments(IMethodInvocation input, Exception ex)
        {
            if (_logArguments && input?.Arguments?.Count != 0)
            {
                var serilizedException = JsonSerializerHelper.SimpleSerialize2(input.Arguments);

                if (ex.Data.Contains("Arguments"))
                {
                    ex.Data["Arguments"] = serilizedException;
                }
                else
                {
                    ex.Data.Add("Arguments", serilizedException);
                }
            }
        }

        public virtual IEnumerable<Type> GetRequiredInterfaces()
        {
            return Type.EmptyTypes;
        }

        public virtual bool WillExecute
        {
            get
            {
                return true;
            }
        }

        public virtual bool HandleException(ref Exception ex, IMethodInvocation input)
        {
            return false;
        }
    }
}
