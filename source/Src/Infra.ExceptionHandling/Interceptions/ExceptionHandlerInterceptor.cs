using Castle.DynamicProxy;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DotFramework.Infra.ExceptionHandling
{
    public class ExceptionHandlerInterceptor : IInterceptor
    {
        public virtual void Intercept(IInvocation invocation)
        {
            if (invocation.Method.ReturnType == typeof(Task))
            {
                invocation.Proceed();
                invocation.ReturnValue = InterceptAsync(invocation);
            }
            else if (invocation.Method.ReturnType.IsGenericType && invocation.Method.ReturnType.GetGenericTypeDefinition() == typeof(Task<>))
            {
                invocation.Proceed();
                invocation.ReturnValue = InterceptAsync((dynamic)invocation.ReturnValue, invocation);
            }
            else
            {
                InterceptSync(invocation);
            }
        }

        private void InterceptSync(IInvocation invocation)
        {
            try
            {
                invocation.Proceed();
            }
            catch (Exception ex)
            {
                if (HandleException(ref ex, invocation))
                {
                    throw ex;
                }
            }
        }

        private async Task InterceptAsync(IInvocation invocation)
        {
            try
            {
                await (Task)invocation.ReturnValue;
            }
            catch (Exception ex)
            {
                if (HandleException(ref ex, invocation))
                {
                    throw ex;
                }
            }
        }

        private async Task<ReturnType> InterceptAsync<ReturnType>(Task<ReturnType> originalTask, IInvocation invocation)
        {
            try
            {
                return await (Task<ReturnType>)invocation.ReturnValue;
            }
            catch (Exception ex)
            {
                if (HandleException(ref ex, invocation))
                {
                    throw ex;
                }

                return default;
            }
        }

        public virtual IEnumerable<Type> GetRequiredInterfaces()
        {
            return Type.EmptyTypes;
        }

        public virtual bool WillExecute
        {
            get
            {
                return true;
            }
        }

        public virtual bool HandleException(ref Exception ex, IInvocation invocation)
        {
            return false;
        }
    }
}
