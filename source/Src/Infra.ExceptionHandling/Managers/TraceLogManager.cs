using DotFramework.Core;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace DotFramework.Infra.ExceptionHandling
{
    public class TraceLogManager : SingletonProvider<TraceLogManager>
    {
        #region Constructor

        private TraceLogManager()
        {
            IConfigurationSource config = ConfigurationSourceFactory.Create();

            LogWriterFactory logWriterFactory = new LogWriterFactory(config);
            LogWriter logWriter = logWriterFactory.Create();
            Logger.SetLogWriter(logWriter);

            ExceptionPolicyFactory factory = new ExceptionPolicyFactory(config);
            ExceptionManager exManager = factory.CreateManager();
            ExceptionPolicy.SetExceptionManager(exManager);
        }

        #endregion

        #region Variables

        private string _ApplicationCode;
        private ISessionInfoProvider _SessionInfoProvider;
        private bool _Initialized = false;

        #endregion

        #region Initialize

        public void Initialize(string applicationCode)
        {
            Initialize(applicationCode, null);
        }

        public void Initialize(string applicationCode, ISessionInfoProvider sessionInfoProvider)
        {
            _ApplicationCode = applicationCode;
            _SessionInfoProvider = sessionInfoProvider;

            _Initialized = true;
        }

        #endregion

        #region Logger

        public void WriteInfo(string title, string message, string additionalData = null, [CallerMemberName] string callerMemberName = null)
        {
            WriteInfo(title, message, GetCallerMethod(callerMemberName), additionalData);
        }

        public void WriteInfo(string title, string message, MethodBase callerMethod, string additionalData = null)
        {
            Write(title, message, TraceEventType.Information, callerMethod, additionalData);
        }

        public Task WriteInfoAsync(string title, string message, string additionalData = null, [CallerMemberName] string callerMemberName = null)
        {
            return WriteInfoAsync(title, message, GetCallerMethod(callerMemberName), additionalData);
        }

        public Task WriteInfoAsync(string title, string message, MethodBase callerMethod, string additionalData = null)
        {
            return WriteAsync(title, message, TraceEventType.Information, callerMethod, additionalData);
        }

        public void WriteWarning(string title, string message, string additionalData = null, [CallerMemberName] string callerMemberName = null)
        {
            WriteWarning(title, message, GetCallerMethod(callerMemberName), additionalData);
        }

        public void WriteWarning(string title, string message, MethodBase callerMethod, string additionalData = null)
        {
            Write(title, message, TraceEventType.Warning, callerMethod, additionalData);
        }

        public Task WriteWarningAsync(string title, string message, string additionalData = null, [CallerMemberName] string callerMemberName = null)
        {
            return WriteWarningAsync(title, message, GetCallerMethod(callerMemberName), additionalData);
        }

        public Task WriteWarningAsync(string title, string message, MethodBase callerMethod, string additionalData = null)
        {
            return WriteAsync(title, message, TraceEventType.Warning, callerMethod, additionalData);
        }

        public void WriteError(string title, string message, string additionalData = null, [CallerMemberName] string callerMemberName = null)
        {
            WriteError(title, message, GetCallerMethod(callerMemberName), additionalData);
        }

        public void WriteError(string title, string message, MethodBase callerMethod, string additionalData = null)
        {
            Write(title, message, TraceEventType.Error, callerMethod, additionalData);
        }

        public Task WriteErrorAsync(string title, string message, string additionalData = null, [CallerMemberName] string callerMemberName = null)
        {
            return WriteErrorAsync(title, message, GetCallerMethod(callerMemberName), additionalData);
        }

        public Task WriteErrorAsync(string title, string message, MethodBase callerMethod, string additionalData = null)
        {
            return WriteAsync(title, message, TraceEventType.Error, callerMethod, additionalData);
        }

        public void WriteVerbose(string title, string message, string additionalData = null, [CallerMemberName] string callerMemberName = null)
        {
            WriteVerbose(title, message, GetCallerMethod(callerMemberName), additionalData);
        }

        public void WriteVerbose(string title, string message, MethodBase callerMethod, string additionalData = null)
        {
            Write(title, message, TraceEventType.Verbose, callerMethod, additionalData);
        }

        public Task WriteVerboseAsync(string title, string message, string additionalData = null, [CallerMemberName] string callerMemberName = null)
        {
            return WriteVerboseAsync(title, message, GetCallerMethod(callerMemberName), additionalData);
        }

        public Task WriteVerboseAsync(string title, string message, MethodBase callerMethod, string additionalData = null)
        {
            return WriteAsync(title, message, TraceEventType.Verbose, callerMethod, additionalData);
        }

        public void WriteCritical(string title, string message, string additionalData = null, [CallerMemberName] string callerMemberName = null)
        {
            WriteCritical(title, message, GetCallerMethod(callerMemberName), additionalData);
        }

        public void WriteCritical(string title, string message, MethodBase callerMethod, string additionalData = null)
        {
            Write(title, message, TraceEventType.Critical, callerMethod, additionalData);
        }

        public Task WriteCriticalAsync(string title, string message, string additionalData = null, [CallerMemberName] string callerMemberName = null)
        {
            return WriteCriticalAsync(title, message, GetCallerMethod(callerMemberName), additionalData);
        }

        public Task WriteCriticalAsync(string title, string message, MethodBase callerMethod, string additionalData = null)
        {
            return WriteAsync(title, message, TraceEventType.Critical, callerMethod, additionalData);
        }

        private void Write(string title, string message, TraceEventType severity, MethodBase callerMethod, string additionalData)
        {
            if (!_Initialized)
            {
                return;
            }

            var now = DateTime.Now;

            try
            {
                Logger.Write(CreateLogEntry(title, message, severity, callerMethod, additionalData, now));
            }
            catch (Exception)
            {

            }
        }

        private Task WriteAsync(string title, string message, TraceEventType severity, MethodBase callerMethod, string additionalData)
        {
            if (!_Initialized)
            {
                return Task.CompletedTask;
            }

            var now = DateTime.Now;

            return Task.Run(() =>
            {
                try
                {
                    Logger.Write(CreateLogEntry(title, message, severity, callerMethod, additionalData, now));
                }
                catch (Exception)
                {

                }
            });
        }

        private LogEntry CreateLogEntry(string title, string message, TraceEventType severity, MethodBase callerMethod, string additionalData, DateTime actualTime)
        {
            LogEntry logEntry = new LogEntry();
            logEntry.Title = title;
            logEntry.Message = message;
            logEntry.Severity = severity;

            logEntry.ExtendedProperties.Add("Trace ID", Guid.NewGuid());
            logEntry.ExtendedProperties.Add("Application Code", _ApplicationCode);

            if (callerMethod != null)
            {
                logEntry.ExtendedProperties.Add("Class Name", callerMethod.DeclaringType.FullName);
                logEntry.ExtendedProperties.Add("Method Name", callerMethod.Name);
            }

            if (_SessionInfoProvider != null)
            {
                if (!String.IsNullOrWhiteSpace(_SessionInfoProvider?.GetUserName()))
                {
                    logEntry.ExtendedProperties.Add("UserName", _SessionInfoProvider?.GetUserName());
                }

                if (!String.IsNullOrWhiteSpace(_SessionInfoProvider?.GetConnectionID()))
                {
                    logEntry.ExtendedProperties.Add("ConnectionID", _SessionInfoProvider?.GetConnectionID());
                }

                if (!String.IsNullOrWhiteSpace(_SessionInfoProvider?.GetSessionID()))
                {
                    logEntry.ExtendedProperties.Add("SessionID", _SessionInfoProvider?.GetSessionID());
                }

                if (!String.IsNullOrWhiteSpace(_SessionInfoProvider?.GetContextID()))
                {
                    logEntry.ExtendedProperties.Add("ContextID", _SessionInfoProvider?.GetContextID());
                }
            }

            if (!String.IsNullOrWhiteSpace(additionalData))
            {
                logEntry.ExtendedProperties.Add("AdditionalData", additionalData);
            }

            logEntry.ExtendedProperties.Add("ActualTime", actualTime);

            return logEntry;
        }

        #endregion

        #region Helpers

        private MethodBase GetCallerMethod(string callerMemberName)
        {
            if (!String.IsNullOrEmpty(callerMemberName))
            {
                return ReflectionHelper.GetCallerMethod(callerMemberName);
            }

            return null;
        }

        #endregion

        #region Exception Handling

        public bool HandleException(Exception ex, string policyName)
        {
            return HandleException(ex, policyName, String.Empty, String.Empty);
        }

        public bool HandleException(Exception ex, string policyName, string className, string methodName)
        {
            ExceptionBase exception;

            if (ex is ExceptionBase)
            {
                exception = (ExceptionBase)ex;

                exception.ApplicationCode = _ApplicationCode;
                exception.ClassName = className;
                exception.MethodName = methodName;
            }
            else
            {
                exception = new InitialException(ex.Message, ex, _ApplicationCode, className, methodName);
            }

            return ExceptionPolicy.HandleException(exception, policyName);
        }

        #endregion
    }
}
