using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration.Design;
using Microsoft.Practices.EnterpriseLibrary.Logging.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Logging.Formatters;
using System;
using System.Configuration;
using System.Diagnostics;

namespace DotFramework.Infra.Logging.Configuration
{
    public abstract class AbstractLoggerServiceTraceListenerData : TraceListenerData
    {
        private const string _FormatterNameProperty = "formatter";

        /// <summary>
        /// Initializes a <see cref="AbstractLoggerServiceTraceListenerData"/>.
        /// </summary>
        public AbstractLoggerServiceTraceListenerData(Type traceListenerType)
            : base(traceListenerType)
        {
            
        }

        /// <summary>
        /// Initializes a named instance of <see cref="AbstractLoggerServiceTraceListenerData"/> with 
        /// name, endpoint address, and formatter name.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="writeLogEndpointAddress">The endpoint address for writing the log.</param>
        /// <param name="formatterName">The formatter name.</param>        
        public AbstractLoggerServiceTraceListenerData(string name,
                                          Type traceListenerType,
                                          string formatterName)
            : this(
                name,
                traceListenerType,
                formatterName,
                TraceOptions.None,
                SourceLevels.All)
        {
        }

        /// <summary>
        /// Initializes a named instance of <see cref="AbstractLoggerServiceTraceListenerData"/> with 
        /// name, endpoint address, and formatter name.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="writeLogEndpointAddress">The endpoint address for writing the log.</param>
        /// <param name="formatterName">The formatter name.</param>
        /// <param name="traceOutputOptions">The trace options.</param>
        /// <param name="filter">The filter to be applied</param>
        public AbstractLoggerServiceTraceListenerData(string name,
                                          Type traceListenerType,
                                          string formatterName,
                                          TraceOptions traceOutputOptions,
                                          SourceLevels filter)
            : base(name, traceListenerType, traceOutputOptions, filter)
        {
            Formatter = formatterName;
        }

        /// <summary>
        /// Gets and sets the formatter name.
        /// </summary>
        [ConfigurationProperty(_FormatterNameProperty, IsRequired = false)]
        [ResourceDescription(typeof(DesignResources), "LoggerServiceTraceListenerDataFormatterDescription")]
        [ResourceDisplayName(typeof(DesignResources), "LoggerServiceTraceListenerDataFormatterDisplayName")]
        [Reference(typeof(NameTypeConfigurationElementCollection<FormatterData, CustomFormatterData>), typeof(FormatterData))]
        public string Formatter
        {
            get { return (string)base[_FormatterNameProperty]; }
            set { base[_FormatterNameProperty] = value; }
        }

        /// <summary>
        /// Builds the <see cref="TraceListener" /> object represented by this configuration object.
        /// </summary>
        /// <param name="settings">The configuration settings for logging.</param>
        /// <returns>
        /// A trace listener.
        /// </returns>
        protected override TraceListener CoreBuildTraceListener(LoggingSettings settings)
        {
            var formatter = BuildFormatterSafe(settings, Formatter);
            return InitializeTraceListener(formatter);
        }

        /// <summary>
        /// Initialize the <see cref="TraceListener" /> by formatter
        /// </summary>
        /// <param name="formatter"><see cref="TraceListener" /></param>
        /// <returns><see cref="TraceListener"/></returns>
        protected abstract TraceListener InitializeTraceListener(ILogFormatter formatter);
    }
}
