using Microsoft.Practices.EnterpriseLibrary.Common.Configuration.Design;
using Microsoft.Practices.EnterpriseLibrary.Logging.Formatters;
using System.Configuration;
using System.Diagnostics;

namespace DotFramework.Infra.Logging.Configuration
{
    /// <summary>
    /// Configuration object for a <see cref="LoggerServiceTraceListener"/>.
    /// </summary>
    [ResourceDescription(typeof(DesignResources), "LoggerServiceTraceListenerDataDescription")]
    [ResourceDisplayName(typeof(DesignResources), "LoggerServiceTraceListenerDataDisplayName")]
    public class LoggerServiceTraceListenerData : AbstractLoggerServiceTraceListenerData
    {
        private const string _WriteLogEndpointAddressProperty = "writeLogEndpointAddress";

        /// <summary>
        /// Initializes a <see cref="LoggerServiceTraceListenerData"/>.
        /// </summary>
        public LoggerServiceTraceListenerData()
            : base(typeof(LoggerServiceTraceListener))
        {
            
        }

        /// <summary>
        /// Initializes a named instance of <see cref="LoggerServiceTraceListenerData"/> with 
        /// name, endpoint address, and formatter name.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="writeLogEndpointAddress">The endpoint address for writing the log.</param>
        /// <param name="formatterName">The formatter name.</param>        
        public LoggerServiceTraceListenerData(string name,
                                          string writeLogEndpointAddress,
                                          string formatterName)
            : this(
                name,
                writeLogEndpointAddress,
                formatterName,
                TraceOptions.None,
                SourceLevels.All)
        {
        }

        /// <summary>
        /// Initializes a named instance of <see cref="LoggerServiceTraceListenerData"/> with 
        /// name, endpoint address, and formatter name.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="writeLogEndpointAddress">The endpoint address for writing the log.</param>
        /// <param name="formatterName">The formatter name.</param>
        /// <param name="traceOutputOptions">The trace options.</param>
        /// <param name="filter">The filter to be applied</param>
        public LoggerServiceTraceListenerData(string name,
                                          string writeLogEndpointAddress,
                                          string formatterName,
                                          TraceOptions traceOutputOptions,
                                          SourceLevels filter)
            : base(name, typeof(LoggerServiceTraceListener), formatterName, traceOutputOptions, filter)
        {
            WriteLogEndpointAddress = writeLogEndpointAddress;
        }

        /// <summary>
        /// Gets and sets the endpoint address for writing the log.
        /// </summary>
        [ConfigurationProperty(_WriteLogEndpointAddressProperty, IsRequired = true, DefaultValue = "WriteLog")]
        [ResourceDescription(typeof(DesignResources), "LoggerServiceTraceListenerWriteLogEndpointAddressDescription")]
        [ResourceDisplayName(typeof(DesignResources), "LoggerServiceTraceListenerWriteLogEndpointAddressDisplayName")]
        public string WriteLogEndpointAddress
        {
            get { return (string)base[_WriteLogEndpointAddressProperty]; }
            set { base[_WriteLogEndpointAddressProperty] = value; }
        }

        protected override TraceListener InitializeTraceListener(ILogFormatter formatter)
        {
            LoggerServiceManager.Instance.Initialize(WriteLogEndpointAddress);
            return new LoggerServiceTraceListener(formatter);
        }
    }
}
