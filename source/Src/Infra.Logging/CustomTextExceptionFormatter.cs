﻿using DotFramework.Core.Serialization;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System;
using System.Collections;
using System.IO;
using System.Linq;
using System.Reflection;

namespace DotFramework.Infra.Logging
{
    public class CustomTextExceptionFormatter : TextExceptionFormatter
    {
        private readonly string[] _dataProperties = new string[] { "Trace ID", "Original Trace ID", "Application Code", "Class Name", "Method Name", "UserName", "ConnectionID", "SessionID", "ContextID", "Host", "Url", "Referer", "Tenant" };

        public CustomTextExceptionFormatter(TextWriter writer, Exception exception) : base(writer, exception)
        {
        }

        public CustomTextExceptionFormatter(TextWriter writer, Exception exception, Guid handlingInstanceId) : base(writer, exception, handlingInstanceId)
        {
        }

        protected override void WritePropertyInfo(PropertyInfo propertyInfo, object value)
        {
            if (propertyInfo.Name == "Data")
            {
                WriteData(propertyInfo, value);
            }
            else
            {
                if (value?.ToString().IsNullOrWhiteSpace() == false)
                {
                    base.WritePropertyInfo(propertyInfo, value);
                }
            }
        }

        private void WriteData(PropertyInfo propertyInfo, object value)
        {
            if (value is IDictionary dic)
            {
                if (dic?.Count != 0)
                {
                    foreach (var key in dic.Keys)
                    {
                        if (!_dataProperties.Contains(key))
                        {
                            var item = dic[key];

                            if (item is string && !item.ToString().IsNullOrWhiteSpace())
                            {
                                WriteItem(key, item);
                            }
                            else if (!(item is null))
                            {
                                WriteItem(key, JsonSerializerHelper.SimpleSerialize(item));
                            }
                        }
                    }
                }
            }
            else if (value is string)
            {
                WritePropertyInfo(propertyInfo, value);
            }
            else
            {
                WritePropertyInfo(propertyInfo, JsonSerializerHelper.SimpleSerialize(value));
            }
        }

        private void WriteItem(object key, object item)
        {
            Indent();
            Writer.Write(key);
            Writer.Write(" : ");
            Writer.WriteLine(item);
        }
    }
}
