using DotFramework.Infra.Logging.Configuration;
using DotFramework.Infra.Model;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using Microsoft.Practices.EnterpriseLibrary.Logging.Formatters;
using Microsoft.Practices.EnterpriseLibrary.Logging.TraceListeners;
using System;
using System.Diagnostics;
using System.Linq;

namespace DotFramework.Infra.Logging
{
    /// <summary>
    /// A <see cref="System.Diagnostics.TraceListener"/> that writes to a database, formatting the output with an <see cref="ILogFormatter"/>.
    /// </summary>
    [ConfigurationElementType(typeof(LoggerServiceTraceListenerData))]
    public class LoggerServiceTraceListener : AbstractLoggerServiceTraceListener
    {
        /// <summary>
        /// Initializes a new instance of <see cref="LoggerServiceTraceListener"/>.
        /// </summary>
        /// <param name="formatter">The formatter.</param>        
        public LoggerServiceTraceListener(
            ILogFormatter formatter)
            : base(formatter)
        {

        }

        protected override void WriteLogEntryModel(LogEntryModel logEntrymodel)
        {
            LoggerServiceManager.Instance.WriteLog(logEntrymodel);
        }
    }
}
