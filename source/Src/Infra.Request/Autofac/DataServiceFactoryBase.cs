﻿using Castle.DynamicProxy;
using DotFramework.Core.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotFramework.Infra.Request.Autofac
{
    public abstract class DataServiceFactoryBase<TDataServiceFacory, TInterceptor> : FactoryBase<TDataServiceFacory, IDataService, TInterceptor>
        where TDataServiceFacory : class
        where TInterceptor : IInterceptor
    {
        protected override InterceptionMethod InterceptionMethod => InterceptionMethod.Interface;

        protected override bool HandleException(ref Exception ex)
        {
            return DataServiceExceptionHandler.Instance.HandleException(ref ex);
        }

        protected abstract void RegisterAllServices();

        public virtual void Configure()
        {
            RegisterAllServices();

            Build();
        }
    }
}
