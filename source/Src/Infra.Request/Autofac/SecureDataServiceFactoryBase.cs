﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DotFramework.Core.DependencyInjection;
using DotFramework.Core;
using Castle.DynamicProxy;

namespace DotFramework.Infra.Request.Autofac
{
    public class SecureDataServiceFactoryBase<TDataServiceFactory> : SecureDataServiceFactoryBase<TDataServiceFactory, DataServiceInterceptor>
        where TDataServiceFactory : class
    {
        protected override void RegisterAllServices()
        {
        }
    }

    public class SecureDataServiceFactoryBase<TDataServiceFactory, TInterceptor> : DataServiceFactoryBase<TDataServiceFactory, DataServiceInterceptor>
        where TDataServiceFactory : class
        where TInterceptor : IInterceptor
    {
        protected override void RegisterAllServices()
        {
        }
    }
}
