﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DotFramework.Core.DependencyInjection;
using DotFramework.Core;
using Castle.DynamicProxy;

namespace DotFramework.Infra.Request.Autofac
{
    public class SecureRequestServiceFactoryBase<TRequestServiceFactory> : SecureRequestServiceFactoryBase<TRequestServiceFactory, RequestServiceInterceptor>
        where TRequestServiceFactory : class
    {
        protected override void RegisterAllServices()
        {
        }
    }

    public class SecureRequestServiceFactoryBase<TRequestServiceFactory, TInterceptor> : RequestServiceFactoryBase<TRequestServiceFactory, TInterceptor>
        where TRequestServiceFactory : class
        where TInterceptor : IInterceptor
    {
        protected override void RegisterAllServices()
        {
        }
    }
}
