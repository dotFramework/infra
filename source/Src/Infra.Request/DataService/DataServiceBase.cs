using System.Threading.Tasks;

namespace DotFramework.Infra.Request
{
    public abstract class DataServiceBase : IDataService
    {
    }

    public abstract class AsyncDataServiceBase : DataServiceBase, IAsyncDataService
    {
    }

    public abstract class DataServiceBase<TResponse> : DataServiceBase, IDataService<TResponse>
        where TResponse : ResponseBase
    {
        public abstract TResponse ProcessRequest();
    }

    public abstract class AsyncDataServiceBase<TResponse> : AsyncDataServiceBase, IAsyncDataService<TResponse>
        where TResponse : ResponseBase
    {
        public abstract Task<TResponse> ProcessRequest();
    }

    public abstract class DataServiceBase<TRequest, TResponse>: DataServiceBase, IDataService<TRequest, TResponse>
        where TRequest : RequestBase
        where TResponse : ResponseBase
    {
        public abstract TResponse ProcessRequest(TRequest request);
    }

    public abstract class AsyncDataServiceBase<TRequest, TResponse> : AsyncDataServiceBase, IAsyncDataService<TRequest, TResponse>
        where TRequest : RequestBase
        where TResponse : ResponseBase
    {
        public abstract Task<TResponse> ProcessRequest(TRequest request);
    }
}
