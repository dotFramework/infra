using System.Threading.Tasks;

namespace DotFramework.Infra.Request
{
    public interface IDataService
    {
        
    }

    public interface IAsyncDataService : IDataService
    {

    }

    public interface IDataService<TResponse> : IDataService
        where TResponse : ResponseBase
    {
        TResponse ProcessRequest();
    }

    public interface IAsyncDataService<TResponse> : IAsyncDataService
        where TResponse : ResponseBase
    {
        Task<TResponse> ProcessRequest();
    }

    public interface IDataService<TRequest, TResponse> : IDataService
        where TRequest : RequestBase
        where TResponse : ResponseBase
    {
        TResponse ProcessRequest(TRequest request);
    }

    public interface IAsyncDataService<TRequest, TResponse> : IAsyncDataService
        where TRequest : RequestBase
        where TResponse : ResponseBase
    {
        Task<TResponse> ProcessRequest(TRequest request);
    }
}
