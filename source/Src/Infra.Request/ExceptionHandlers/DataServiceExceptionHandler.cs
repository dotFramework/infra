using System;
using System.Data.SqlClient;
using DotFramework.Core;
using DotFramework.Infra.ExceptionHandling;

namespace DotFramework.Infra.Request
{
    public class DataServiceExceptionHandler : ExceptionHandlerBase<DataServiceExceptionHandler>
    {
        private DataServiceExceptionHandler()
        {

        }

        public override bool HandleException(ref Exception ex, string className, string methodName)
        {
            bool reThrow = false;

            if (ex is DataServiceCustomException)
            {
                reThrow = TraceLogManager.Instance.HandleException(ex, ExceptionHandlingPolicyConstants.DataServiceCustomPolicy, className, methodName);
                ex = new DataServiceCustomException(ex.Message, ex);
            }
            else if (ex is ForbiddenAccessException)
            {
                reThrow = true;
            }
            //else if (ex is IUnauthorizedHttpException)
            //{
            //    reThrow = true;
            //}
            else if ((ex is SqlException))
            {
                SqlException dbExp = ex as SqlException;

                if (dbExp.Number >= 50000)
                {
                    reThrow = TraceLogManager.Instance.HandleException(ex, ExceptionHandlingPolicyConstants.DataServiceCustomPolicy, className, methodName);
                    ex = new DataServiceCustomException(ex.Message);
                }
                else
                {
                    reThrow = TraceLogManager.Instance.HandleException(ex, ExceptionHandlingPolicyConstants.DataServicePolicy, className, methodName);
                }
            }
            else if (ex is IHttpException && ex is ICustomException)
            {
                reThrow = TraceLogManager.Instance.HandleException(ex, ExceptionHandlingPolicyConstants.DataServiceCustomPolicy, className, methodName);
                ex = new DataServiceCustomException(ex.Message, ex);
            }
            else if (ex is IHttpException && !(ex is IUnauthorizedHttpException))
            {
                reThrow = TraceLogManager.Instance.HandleException(ex, ExceptionHandlingPolicyConstants.DataServicePolicy, className, methodName);
            }
            else if (ex is ExceptionBase)
            {
                reThrow = TraceLogManager.Instance.HandleException(ex, ExceptionHandlingPolicyConstants.PassThroughPolicy, className, methodName);
                ex = new PassThroughException(ex.Message, ex);
            }
            else
            {
                reThrow = TraceLogManager.Instance.HandleException(ex, ExceptionHandlingPolicyConstants.DataServicePolicy, className, methodName);
            }

            if (reThrow)
            {
                throw ex;
            }

            return reThrow;
        }
    }
}
