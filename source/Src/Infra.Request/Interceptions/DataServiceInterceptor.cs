using Castle.DynamicProxy;
using DotFramework.Infra.ExceptionHandling;
using System;
using Unity.Interception.PolicyInjection.Pipeline;

namespace DotFramework.Infra.Request
{
    public class DataServiceInterceptor : ExceptionHandlerInterceptor
    {
        public override bool HandleException(ref Exception ex, IInvocation invocation)
        {
            return DataServiceExceptionHandler.Instance.HandleException(ref ex, invocation.TargetType.FullName, invocation.Method.Name);
        }
    }
}
