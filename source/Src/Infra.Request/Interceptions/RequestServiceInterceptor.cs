using Castle.DynamicProxy;
using DotFramework.Infra.ExceptionHandling;
using System;
using Unity.Interception.PolicyInjection.Pipeline;

namespace DotFramework.Infra.Request
{
    public class RequestServiceInterceptor : ExceptionHandlerInterceptor
    {
        public override bool HandleException(ref Exception ex, IInvocation invocation)
        {
            return RequestServiceExceptionHandler.Instance.HandleException(ref ex, invocation.TargetType.FullName, invocation.Method.Name);
        }
    }
}
