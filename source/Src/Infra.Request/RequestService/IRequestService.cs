using System.Threading.Tasks;

namespace DotFramework.Infra.Request
{
    public interface IRequestService
    {
        
    }

    public interface IAsyncRequestService : IRequestService
    {

    }

    public interface IRequestService<TResponse> : IRequestService
        where TResponse : ResponseBase
    {
        TResponse ProcessRequest();
    }

    public interface IAsyncRequestService<TResponse> : IAsyncRequestService
        where TResponse : ResponseBase
    {
        Task<TResponse> ProcessRequest();
    }

    public interface IRequestService<TRequest, TResponse> : IRequestService
        where TRequest : RequestBase
        where TResponse : ResponseBase
    {
        TResponse ProcessRequest(TRequest request);
    }

    public interface IAsyncRequestService<TRequest, TResponse> : IAsyncRequestService
        where TRequest : RequestBase
        where TResponse : ResponseBase
    {
        Task<TResponse> ProcessRequest(TRequest request);
    }
}
