using System.Threading.Tasks;

namespace DotFramework.Infra.Request
{
    public abstract class RequestServiceBase : IRequestService
    {
    }

    public abstract class AsyncRequestServiceBase : RequestServiceBase, IAsyncRequestService
    {
    }

    public abstract class RequestServiceBase<TResponse> : RequestServiceBase, IRequestService<TResponse>
        where TResponse : ResponseBase
    {
        public abstract TResponse ProcessRequest();
    }

    public abstract class AsyncRequestServiceBase<TResponse> : AsyncRequestServiceBase, IAsyncRequestService<TResponse>
        where TResponse : ResponseBase
    {
        public abstract Task<TResponse> ProcessRequest();
    }

    public abstract class RequestServiceBase<TRequest, TResponse>: RequestServiceBase, IRequestService<TRequest, TResponse>
        where TRequest : RequestBase
        where TResponse : ResponseBase
    {
        public abstract TResponse ProcessRequest(TRequest request);
    }

    public abstract class AsyncRequestServiceBase<TRequest, TResponse> : AsyncRequestServiceBase, IAsyncRequestService<TRequest, TResponse>
        where TRequest : RequestBase
        where TResponse : ResponseBase
    {
        public abstract Task<TResponse> ProcessRequest(TRequest request);
    }
}
