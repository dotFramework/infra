﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotFramework.Infra.Security
{
    public interface IApplicationProperties
    {
        string AppIdentifier { get; set; }
        string CallbackURI { get; set; }
    }
}
