﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotFramework.Infra.Security
{
    public interface IInitiator
    {
        string Initiator { get; set; }
    }
}
