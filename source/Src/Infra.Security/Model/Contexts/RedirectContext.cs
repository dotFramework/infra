namespace DotFramework.Infra.Security.Model
{
    public class RedirectContext
    {
        public RedirectContext(string redirectUrl)
        {
            RedirectUrl = redirectUrl;
        }

        public string RedirectUrl { get; private set; }
    }
}