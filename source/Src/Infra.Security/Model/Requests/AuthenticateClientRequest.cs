﻿namespace DotFramework.Infra.Security.Model
{
    public class AuthenticateClientRequest
    {
        public string ClientID { get; set; }

        public string ClientSecret { get; set; }

        public string DeviceIdentifier { get; set; }
    }
}
