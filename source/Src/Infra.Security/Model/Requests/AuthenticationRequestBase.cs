﻿using System;
using System.Collections.Generic;
using System.Security.Claims;

namespace DotFramework.Infra.Security.Model
{
    public class AuthenticationRequestBase
    {
        public string Initiator { get; set; }
        public string AppIdentifier { get; set; }

        public Dictionary<String, String> AdditionalParameters { get; set; }

        public IEnumerable<Claim> AdditionalClaims { get; set; }
    }
}
