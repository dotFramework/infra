using System.ComponentModel.DataAnnotations;

namespace DotFramework.Infra.Security.Model
{
    public class CreateClientRequest
    {
        [Required]
        public string ClientIdentifier { get; set; }

        [Required]
        public string ClientName { get; set; }

        public bool DeviceRestricted { get; set; }

        public string PreviousClientIdentifier { get; set; }
    }
}
