﻿using System.ComponentModel.DataAnnotations;

namespace DotFramework.Infra.Security.Model
{
    public class ForceLogoutRequest
    {
        [Required]
        public string GroupToken { get; set; }
    }
}
