namespace DotFramework.Infra.Security.Model
{
    public class GetClientRequest
    {
        public string ClientIdentifier { get; set; }
        public string ClientUID { get; set; }
    }
}
