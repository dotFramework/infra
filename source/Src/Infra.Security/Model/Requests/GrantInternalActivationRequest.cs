﻿using System.ComponentModel.DataAnnotations;

namespace DotFramework.Infra.Security.Model
{
    public class GrantInternalActivationRequest : AuthenticationRequestBase
    {
        [Required]
        public string UserName { get; set; }
    }
}
