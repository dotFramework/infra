using System.ComponentModel.DataAnnotations;

namespace DotFramework.Infra.Security.Model
{
    public class ReauthenticateRequest : AuthenticationRequestBase
    {
        public string RefreshToken { get; set; }
        public string AppSecretProof { get; set; }
    }
}
