﻿using System.ComponentModel.DataAnnotations;

namespace DotFramework.Infra.Security.Model
{
    public class UpdateUserInfoRequest
    {
        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }

        [EmailAddress]
        public string Email { get; set; }
        public string PhoneNumber { get; set; }

        public string AccessToken { get; set; }
    }
}
