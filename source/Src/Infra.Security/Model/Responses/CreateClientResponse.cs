namespace DotFramework.Infra.Security.Model
{
    public class CreateClientResponse
    {
        public string ClientIdentifier { get; set; }
        public string ClientID { get; set; }
        public string ClientSecret { get; set; }
    }
}
