namespace DotFramework.Infra.Security.Model
{
    public class GetClientResponse
    {
        public string ClientUID { get; set; }
        public string ClientIdentifier { get; set; }
        public string ClientName { get; set; }
    }
}
