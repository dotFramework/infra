﻿namespace DotFramework.Infra.Security.Model
{
    public class LogoutResponse
    {
        public string RedirectUrl { get; set; }
    }
}
