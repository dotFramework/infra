using DotFramework.Core;
using DotFramework.Core.Configuration;
using DotFramework.Core.Web;
using DotFramework.Infra.Security.Model;
using DotFramework.Infra.Web.API;
using DotFramework.Web.API;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
#if NETCOREAPP3_1
using Microsoft.AspNetCore.Http;
#else
using System.Web;
#endif

namespace DotFramework.Infra.Security
{
    public partial class AuthService : SingletonProvider<AuthService>
    {
        #region Variables

        string _ClientID;
        string _ClientSecret;
        bool _IsClientSecretEncrypted;

        public static object _PadLock_ClientBasicAuthorizationToken = new object();
        private AuthorizationToken _ClientBasicAuthorizationToken;
        private AuthorizationToken ClientBasicAuthorizationToken
        {
            get
            {
                //lock (_PadLock_ClientBasicAuthorizationToken)
                //{
                return _ClientBasicAuthorizationToken;
                //}
            }
        }

        public static object _PadLock_ClientAuthorizationToken = new object();
        private AuthorizationToken _ClientAuthorizationToken;
        private AuthorizationToken ClientAuthorizationToken
        {
            get
            {
                //lock (_PadLock_ClientAuthorizationToken)
                //{
                return _ClientAuthorizationToken;
                //}
            }
        }

        private AbstractHttpUtility _HttpUtility;
        private AbstractHttpUtility HttpUtility
        {
            get
            {
                return _HttpUtility;
            }
        }

        #endregion

        #region HttpContext

        protected HttpContext Context
        {
            get
            {
#if NETCOREAPP3_1
                return _HttpContextAccessor.HttpContext;
#else
                return HttpContext.Current;
#endif
            }
        }

#endregion

        public void Initialize(string authEndpointPath, string clientID, string clientSecret, bool isClientSecretEncrypted = true)
        {
            PreInitialize(clientID, clientSecret, isClientSecretEncrypted);

            _HttpUtility = new HttpClientUtility(
                authEndpointPath,
                AppSettingsManager.Instance.Get<Int32>("HttpClient:MaxConnectionsPerServer", 0));

            PostInitialize();
        }

#if NETCOREAPP3_1

        private IHttpContextAccessor _HttpContextAccessor;

        public void InitializeWithName(IHttpContextAccessor httpContextAccessor, string httpClientName, string clientID, string clientSecret, bool isClientSecretEncrypted = true)
        {
            _HttpContextAccessor = httpContextAccessor ?? throw new ArgumentNullException(nameof(httpContextAccessor));

            PreInitialize(clientID, clientSecret, isClientSecretEncrypted);

            _HttpUtility = new EnhancedHttpClientUtility(httpClientName);

            PostInitialize();
        }

#endif

        private void PreInitialize(string clientID, string clientSecret, bool isClientSecretEncrypted)
        {
            _ClientID = clientID;
            _ClientSecret = clientSecret;
            _IsClientSecretEncrypted = isClientSecretEncrypted;
        }

        private void PostInitialize()
        {
            ResetClientBasicToken().GetAwaiter().GetResult();
            ResetClientToken().GetAwaiter().GetResult();
        }

        public async Task<AuthenticateClientResponse> AuthenticateClient(AuthenticateClientRequest request)
        {
            try
            {
                var token = await CreateBasicAuthorization(request);

                var content = new List<KeyValuePair<String, String>>
                {
                    new KeyValuePair<String, String>("grant_type", "client_credentials")
                };

                string initiator;

                if (String.IsNullOrWhiteSpace(request?.DeviceIdentifier))
                {
                    initiator = GetInitiator();
                }
                else
                {
                    initiator = request.DeviceIdentifier;
                }

                if (!String.IsNullOrWhiteSpace(initiator))
                {
                    content.Add(new KeyValuePair<String, String>("device_identifier", initiator));
                }

                return await HttpUtility.PostAsync<AuthenticateClientResponse, AuthErrorResult>("Token", content, token);
            }
            catch
            {
                throw;
            }
        }

        public async Task<TokenResponseModel> Authenticate(LoginBindingModel model)
        {
            try
            {
                var content = new List<KeyValuePair<String, String>>
                {
                    new KeyValuePair<String, String>("grant_type", "password"),
                    new KeyValuePair<String, String>("userName", model.UserName),
                    new KeyValuePair<String, String>("password", model.Password)
                };

                if (!String.IsNullOrWhiteSpace(model.AppIdentifier))
                {
                    content.Add(new KeyValuePair<String, String>("app_identifier", model.AppIdentifier));
                }

                return await PostTokenWithRetry<TokenResponseModel>(content);
            }
            catch
            {
                throw;
            }
        }

        public RedirectContext ExternalLogin(string client_id, string provider, string redirect_url)
        {
            try
            {
                string path = String.Format("Auth/ExternalLogin?client_id={0}&provider={1}&redirect_uri={2}", client_id,
                                                                                                              provider,
                                                                                                              redirect_url);
                return new RedirectContext(path);
            }
            catch
            {
                throw;
            }
        }

        public async Task<TokenResponseModel> ExternalAuthenticate(ObtainLocalAccessTokenBindingModel model)
        {
            try
            {
                var content = new List<KeyValuePair<String, String>>
                {
                    new KeyValuePair<String, String>("grant_type", "external"),
                    new KeyValuePair<String, String>("provider", model.Provider),
                    new KeyValuePair<String, String>("access_token", model.AccessToken)
                };

                return await PostTokenWithRetry<TokenResponseModel>(content);
            }
            catch
            {
                throw;
            }
        }

        public async Task<TokenResponseModel> RefreshToken(RefreshTokenBindingModel model)
        {
            try
            {
                var content = new List<KeyValuePair<String, String>>
                {
                    new KeyValuePair<String, String>("grant_type", "refresh_token"),
                    new KeyValuePair<String, String>("refresh_token", model.RefreshToken)
                };

                return await PostTokenWithRetry<TokenResponseModel>(content);
            }
            catch
            {
                throw;
            }
        }

        public async Task<VerifyTokenResponseModel> VerifyToken(VerifyTokenRequest request)
        {
            try
            {
                return await HttpUtility.PostAsync<VerifyTokenResponseModel>("Auth/VerifyToken", new { AppSecretProof = request.AppSecretProof }, CreateBearerToken(request.AccessToken));
            }
            catch
            {
                throw;
            }
        }

        public async Task<GenerateClientEncryptionTokenResponse> GenerateClientEncryptionToken(GenerateClientEncryptionTokenRequest request)
        {
            try
            {
                return await HttpUtility.PostAsync<GenerateClientEncryptionTokenResponse, BadRequestErrorResult>("Auth/ClientEncryptionToken", request);
            }
            catch
            {
                throw;
            }
        }

        public async Task<ActivateClientResponse> ActivateClient(ActivateClientRequest model)
        {
            try
            {
                if (String.IsNullOrWhiteSpace(model.DeviceIdentifier))
                {
                    model.DeviceIdentifier = GetInitiator();
                }

                return await HttpUtility.PostAsync<ActivateClientResponse>("Auth/ActivateClient", model);
            }
            catch
            {
                throw;
            }
        }

        public async Task<ActivateResponse> Activate(ActivateRequest model)
        {
            try
            {
                return await HttpUtility.PostAsync<ActivateResponse>("Auth/Activate", model);
            }
            catch
            {
                throw;
            }
        }

        public async Task<ActivateTokenResponseModel> GrantActivation(GrantActivationBindingModel model)
        {
            try
            {
                FormUrlEncodedContent content = new FormUrlEncodedContent(new List<KeyValuePair<String, String>>
                {
                    new KeyValuePair<String, String>("grant_type", "activation"),
                    new KeyValuePair<String, String>("userName", model.UserName),
                    new KeyValuePair<String, String>("token", model.Token),
                    new KeyValuePair<String, String>("client_id", _ClientID)
                });

                return await PostTokenWithRetry<ActivateTokenResponseModel>(content);
            }
            catch
            {
                throw;
            }
        }

        public async Task<TokenResponseModel> GrantInternalActivation(GrantInternalActivationBindingModel model)
        {
            try
            {
                var content = new List<KeyValuePair<String, String>>
                {
                    new KeyValuePair<String, String>("grant_type", "internal_activation"),
                    new KeyValuePair<String, String>("userName", model.UserName),
                };

                return await PostBearerTokenWithRetry<TokenResponseModel>(content);
            }
            catch
            {
                throw;
            }
        }

        public async Task<VerifyClientTokenResponseModel> VerifyClientToken(VerifyTokenRequest request)
        {
            try
            {
                if (String.IsNullOrWhiteSpace(request?.AccessToken))
                {
                    throw new UnauthorizedHttpException();
                }

                return await HttpUtility.PostAsync<VerifyClientTokenResponseModel>("Auth/VerifyClientToken", null, CreateBearerToken(request.AccessToken));
            }
            catch
            {
                throw;
            }
        }

        public async Task<LockUserResponseModel> LockUser(LockUserBindingModel request)
        {
            try
            {
                try
                {
                    return await HttpUtility.PostAsync<LockUserResponseModel>("Auth/LockUser", request, ClientAuthorizationToken);
                }
                catch (UnauthorizedHttpException)
                {
                    await ResetClientToken();
                    return await HttpUtility.PostAsync<LockUserResponseModel>("Auth/LockUser", request, ClientAuthorizationToken);
                }
            }
            catch
            {
                throw;
            }
        }

        public async Task<LockUserResponseModel> UnLockUser(UnLockUserBindingModel request)
        {
            try
            {
                try
                {
                    return await HttpUtility.PostAsync<LockUserResponseModel>("Auth/UnLockUser", request, ClientAuthorizationToken);
                }
                catch (UnauthorizedHttpException)
                {
                    await ResetClientToken();
                    return await HttpUtility.PostAsync<LockUserResponseModel>("Auth/UnLockUser", request, ClientAuthorizationToken);
                }
            }
            catch
            {
                throw;
            }
        }

        public async Task<Boolean> SignOut(AuthorizationBindingModel model)
        {
            try
            {
                return await HttpUtility.PostAsync<Boolean>("Auth/Logout", null, CreateBearerToken(model.AccessToken));
            }
            catch
            {
                throw;
            }
        }

        public async Task<Boolean> ForceLogout(ForceLogoutRequest request)
        {
            try
            {
                return await HttpUtility.PostAsync<Boolean>("Auth/ForceLogout", request);
            }
            catch
            {
                throw;
            }
        }

        public async Task Register(RegisterBindingModel model)
        {
            try
            {
                try
                {
                    await HttpUtility.PostAsync<Object>("Auth/Register", model, ClientAuthorizationToken);
                }
                catch (UnauthorizedHttpException)
                {
                    await ResetClientToken();
                    await HttpUtility.PostAsync<Object>("Auth/Register", model, ClientAuthorizationToken);
                }
            }
            catch
            {
                throw;
            }
        }

        public async Task<TokenResponseModel> RegisterAndLogin(RegisterBindingModel model)
        {
            try
            {
                await Register(model);

                LoginBindingModel loginModel = new LoginBindingModel { UserName = model.UserName, Password = model.Password };
                return await Authenticate(loginModel);
            }
            catch
            {
                throw;
            }
        }

        public async Task ChangePassword(ChangePasswordBindingModel model, string accessToken)
        {
            try
            {
                await HttpUtility.PostAsync<object>("Auth/ChangePassword", model, CreateBearerToken(accessToken));
            }
            catch
            {
                throw;
            }
        }

        public async Task UpdateUserInfo(UpdateUserInfoRequest request, string accessToken)
        {
            try
            {
                await HttpUtility.PostAsync<object>("Auth/UpdateUserInfo", request, CreateBearerToken(accessToken));
            }
            catch
            {
                throw;
            }
        }

        public async Task<Int64> CreateSession(CreateSessionBindingModel model)
        {
            try
            {
                try
                {
                    return await HttpUtility.PostAsync<Int64>("Auth/CreateSession", model, ClientAuthorizationToken);
                }
                catch (UnauthorizedHttpException)
                {
                    await ResetClientToken();
                    return await HttpUtility.PostAsync<Int64>("Auth/CreateSession", model, ClientAuthorizationToken);
                }
            }
            catch
            {
                throw;
            }
        }

        public async Task<AuthorizedRolesResponseModel> GetAuthorizedRoles(GetAuthorizedRolesBindingModel model)
        {
            try
            {
                return await HttpUtility.PostAsync<AuthorizedRolesResponseModel>("Auth/GetAuthorizedRoles", model, CreateBearerToken(model.AccessToken));
            }
            catch
            {
                throw;
            }
        }

        public async Task<LockInfoResponseModel> GetLockoutInfo(GetLockoutInfoBindingModel model)
        {
            try
            {
                return await HttpUtility.PostAsync<LockInfoResponseModel>("Auth/LockoutInfo", model, CreateBearerToken(model.AccessToken));
            }
            catch
            {
                throw;
            }
        }

        public async Task<Boolean> GetAuthorizationStatus(GetAuthorizedRolesBindingModel model)
        {
            try
            {
                return await HttpUtility.PostAsync<Boolean>("Auth/GetAuthorizationStatus", model, CreateBearerToken(model.AccessToken));
            }
            catch
            {
                throw;
            }
        }

        public async Task<Boolean> ForgetPassword(ForgetPasswordBindingModel model)
        {
            try
            {
                try
                {
                    return await HttpUtility.PostAsync<Boolean>("Auth/ForgetPassword", model, ClientAuthorizationToken);
                }
                catch (UnauthorizedHttpException)
                {
                    await ResetClientToken();
                    return await HttpUtility.PostAsync<Boolean>("Auth/ForgetPassword", model, ClientAuthorizationToken);
                }
            }
            catch
            {
                throw;
            }
        }

        public async Task<Boolean> ResetPassword(ResetPasswordBindingModel model)
        {
            try
            {
                try
                {
                    return await HttpUtility.PostAsync<Boolean>("Auth/ResetPassword", model, ClientAuthorizationToken);
                }
                catch (UnauthorizedHttpException)
                {
                    await ResetClientToken();
                    return await HttpUtility.PostAsync<Boolean>("Auth/ResetPassword", model, ClientAuthorizationToken);
                }
            }
            catch
            {
                throw;
            }
        }

        public async Task<Boolean> SimpleResetPassword(SimpleResetPasswordBindingModel model)
        {
            try
            {
                try
                {
                    return await HttpUtility.PostAsync<Boolean>("Auth/SimpleResetPassword", model, ClientAuthorizationToken);
                }
                catch (UnauthorizedHttpException)
                {
                    await ResetClientToken();
                    return await HttpUtility.PostAsync<Boolean>("Auth/SimpleResetPassword", model, ClientAuthorizationToken);
                }
            }
            catch
            {
                throw;
            }
        }

        public async Task<AuthorizationToken> GetClientToken(bool renewToken)
        {
            if (renewToken)
            {
                await ResetClientToken();
                return ClientAuthorizationToken;
            }
            else
            {
                if (String.IsNullOrEmpty(ClientAuthorizationToken?.Token))
                {
                    await ResetClientToken();
                }

                return ClientAuthorizationToken;
            }
        }

        public async Task<CreateClientResponse> CreateClient(CreateClientRequest request, string accessToken)
        {
            try
            {
                return await HttpUtility.PostAsync<CreateClientResponse>("Auth/Secure/CreateClient", request, CreateBearerToken(accessToken));
            }
            catch
            {
                throw;
            }
        }

        public async Task<GetClientResponse> GetClient(GetClientRequest request)
        {
            try
            {
                return await HttpUtility.PostAsync<GetClientResponse>("Auth/Secure/GetClient", request, ClientAuthorizationToken);
            }
            catch
            {
                throw;
            }
        }

        public async Task ConfirmEmail(ConfirmEmailBindingModel model)
        {
            try
            {
                await HttpUtility.PostAsync<dynamic>("Auth/ConfirmEmail", model);
            }
            catch
            {
                throw;
            }
        }

        public async Task<RedirectContext> ConfirmEmailCallback(ConfirmEmailCallbackBindingModel model)
        {
            try
            {
                return await HttpUtility.PostAsync<RedirectContext>("Auth/ConfirmEmailCallback", model);
            }
            catch
            {
                throw;
            }
        }

        #region Helpers

        private async Task<T> PostTokenWithRetry<T>(object content)
        {
            try
            {
                return await HttpUtility.PostAsync<T, AuthErrorResult>("Token", content, ClientBasicAuthorizationToken);
            }
            catch (UnauthorizedHttpException)
            {
                await ResetClientBasicToken();
                return await HttpUtility.PostAsync<T, AuthErrorResult>("Token", content, ClientBasicAuthorizationToken);
            }
            catch (HttpException<AuthErrorResult> ex)
            {
                if (ex.ErrorResult.Error == "invalid_clientId" || ex.ErrorResult.Error == "invalid_client_credential")
                {
                    await ResetClientBasicToken();
                    return await HttpUtility.PostAsync<T, AuthErrorResult>("Token", content, ClientBasicAuthorizationToken);
                }
                else
                {
                    throw;
                }
            }
        }

        private async Task<T> PostBearerTokenWithRetry<T>(object content)
        {
            try
            {
                return await HttpUtility.PostAsync<T, AuthErrorResult>("Token", content, ClientAuthorizationToken);
            }
            catch (UnauthorizedHttpException)
            {
                await ResetClientToken();
                return await HttpUtility.PostAsync<T, AuthErrorResult>("Token", content, ClientAuthorizationToken);
            }
            catch (HttpException<AuthErrorResult> ex)
            {
                if (ex.ErrorResult.Error == "invalid_clientId" || ex.ErrorResult.Error == "invalid_client_credential")
                {
                    await ResetClientToken();
                    return await HttpUtility.PostAsync<T, AuthErrorResult>("Token", content, ClientAuthorizationToken);
                }
                else
                {
                    throw;
                }
            }
        }

        private async Task ResetClientToken()
        {
            if (!String.IsNullOrEmpty(_ClientID) && !String.IsNullOrEmpty(_ClientSecret))
            {
                //lock (_PadLock_ClientAuthorizationToken)
                //{
                var authenticateClientResponse = await AuthenticateClient(new AuthenticateClientRequest
                {
                    ClientID = _ClientID,
                    ClientSecret = _ClientSecret
                });

                _ClientAuthorizationToken = CreateBearerToken(authenticateClientResponse.AccessToken);
                //}
            }
        }

        private async Task ResetClientBasicToken()
        {
            //lock (_PadLock_ClientBasicAuthorizationToken)
            //{
            string clientSecret = await EncryptClientSecret(_ClientID, _ClientSecret);
            _ClientBasicAuthorizationToken = CreateBasicToken(_ClientID, clientSecret);
            //}
        }

        private async Task<string> EncryptClientSecret(string clientID, string clientSecret)
        {
            string result = clientSecret;

            if (_IsClientSecretEncrypted)
            {
                var clientEncryptionTokenResponse = await GenerateClientEncryptionToken(new GenerateClientEncryptionTokenRequest
                {
                    ClientID = clientID
                });

                result = EncryptUtility.GetHash(result, clientEncryptionTokenResponse.Token);
            }

            return result;
        }

        private AuthorizationToken CreateBasicToken(string clientId, string clientSecret)
        {
            return CreateBasicToken(GenerateBasicAuthorizationToken(clientId, clientSecret));
        }

        private AuthorizationToken CreateBasicToken(string token)
        {
            return new AuthorizationToken(TokenTypeEnum.Basic, token);
        }

        private AuthorizationToken CreateBearerToken(string token)
        {
            return new AuthorizationToken(TokenTypeEnum.Bearer, token);
        }

        private async Task<AuthorizationToken> CreateBasicAuthorization(AuthenticateClientRequest request)
        {
            string auturizationToken;

            if (!String.IsNullOrWhiteSpace(request?.ClientID) && !String.IsNullOrWhiteSpace(request?.ClientSecret))
            {
                string clientSecret = await EncryptClientSecret(request.ClientID, request.ClientSecret);
                auturizationToken = GenerateBasicAuthorizationToken(request.ClientID, clientSecret);
            }
            else
            {
                auturizationToken = Context?.GetToken();
            }

            if (String.IsNullOrWhiteSpace(auturizationToken))
            {
                throw new UnauthorizedHttpException();
            }
            else
            {
                var token = new AuthorizationToken(TokenTypeEnum.Basic, auturizationToken);
                return token;
            }
        }

        private string GenerateBasicAuthorizationToken(string clientId, string clientSecret)
        {
            return Convert.ToBase64String(System.Text.Encoding.GetEncoding("ISO-8859-1").GetBytes(clientId + ":" + clientSecret));
        }

        private string GetInitiator()
        {
            var current = Context;

#if NET462 || NET48
            if (current != null && current.Request.Headers.AllKeys.Select(k => k.ToLower()).Contains("initiator"))
            {
                return current.Request.Headers.Get(current.Request.Headers.AllKeys.First(k => k.ToLower() == "initiator"));
            }
            else
            {
                return null;
            }
#else
            if (current != null && current.Request.Headers.Any(h => h.Key.ToLower() == "initiator"))
            {
                return current.Request.Headers.First(h => h.Key.ToLower() == "initiator").Value.FirstOrDefault();
            }
            else
            {
                return null;
            }
#endif
        }

#endregion
    }
}
