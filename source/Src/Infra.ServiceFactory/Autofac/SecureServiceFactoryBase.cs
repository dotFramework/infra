namespace DotFramework.Infra.ServiceFactory.Autofac
{
    public class SecureServiceFactoryBase<TServiceFactory> : ServiceFactoryBase<TServiceFactory, AuthorizationInterceptor> where TServiceFactory : class
    {
    }
}
