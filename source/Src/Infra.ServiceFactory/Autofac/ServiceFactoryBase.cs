using DotFramework.Core.DependencyInjection;
using DotFramework.Infra.Configuration;
using System;
using System.Configuration;
using System.IO;
using System.Reflection;
using Castle.DynamicProxy;
using DotFramework.Core;

namespace DotFramework.Infra.ServiceFactory.Autofac
{
    public abstract class ServiceFactoryBase<TServiceFactory, TInterceptor> : FactoryBase<TServiceFactory, IServiceBase, TInterceptor>
        where TServiceFactory : class
        where TInterceptor : IInterceptor
    {
        protected override InterceptionMethod InterceptionMethod => InterceptionMethod.Interface;
        protected ServiceConfigSection ServiceSection { get; set; }

        public void Configure(string sectionName)
        {
            try
            {
                ServiceSection = (ServiceConfigSection)ConfigurationManager.GetSection(sectionName);

                ConfigureBusinessRules(ServiceSection.RulesFactoryDllPath, ServiceSection.RulesFactoryType);
                RegisterServices();
                Build();
            }
            catch (Exception ex)
            {
                if (ServiceFactoryExceptionHandler.Instance.HandleException(ref ex))
                {
                    throw ex;
                }
            }
        }

        private void ConfigureBusinessRules(string rulesFactoryDllPath, string rulesFactoryType)
        {
            Assembly assembly = Assembly.LoadFrom(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, rulesFactoryDllPath));
            object temp = Activator.CreateInstance(assembly.GetType(rulesFactoryType), true);

            Type singletonProviderType = GetSingletonProviderType(temp.GetType());

            object instance = singletonProviderType.GetProperty("Instance").GetGetMethod().Invoke(null, null);
            instance.GetType().GetMethod("Configure", new Type[] { typeof(String) }).Invoke(instance, new object[] { rulesFactoryDllPath });
        }

        public virtual IService GetService<IService>() where IService : IServiceBase
        {
            return Resolve<IService>();
        }

        protected override bool HandleException(ref Exception ex)
        {
            return ServiceFactoryExceptionHandler.Instance.HandleException(ref ex);
        }

        #region Create Proxy

        private void RegisterServices()
        {
            foreach (BusinessServiceElement serviceElement in ServiceSection.BusinessServices)
            {
                ProxyType proxyType = serviceElement.ProxyType ?? ServiceSection.ProxyType;

                object service = null;

                switch (proxyType)
                {
                    case ProxyType.Assembly:
                        service = CreateAssemblyProxy(serviceElement.ServiceType, ServiceSection.DllPath);
                        break;
                    case ProxyType.WCF:
                    case ProxyType.API:
                        break;
                    default:
                        throw new NotSupportedException("Not Supported Proxy Type");
                }

                if (service != null)
                {
                    Register(service, CreateType(serviceElement.ContractType, ServiceSection.FactoryDllPath));
                }
            }
        }

        private object CreateAssemblyProxy(string serviceType, string dllPath)
        {
            Assembly assembly = Assembly.LoadFrom(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, dllPath));
            object service = assembly.CreateInstance(serviceType);

            return service;
        }

        private Type CreateType(string typeName, string dllPath)
        {
            Assembly assembly = Assembly.LoadFrom(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, dllPath));
            return assembly.GetType(typeName);
        }

        private static Type GetSingletonProviderType(Type type)
        {
            if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(SingletonProvider<>))
            {
                return type;
            }
            else
            {
                return GetSingletonProviderType(type.BaseType);
            }
        }

        #endregion
    }

    public class ServiceFactoryBase<TServiceFacory> : ServiceFactoryBase<TServiceFacory, ServiceInterceptionBehavior, CustomEndpointBehavior>
        where TServiceFacory : class
    {

    }
}
