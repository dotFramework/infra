using Castle.DynamicProxy;
using DotFramework.Infra.ExceptionHandling;
using System;

namespace DotFramework.Infra.ServiceFactory
{
    public class ServiceInterceptor : ExceptionHandlerInterceptor
    {
        public override bool HandleException(ref Exception ex, IInvocation invocation)
        {
            return ServiceExceptionHandler.Instance.HandleException(ref ex, invocation.TargetType.FullName, invocation.Method.Name);
        }
    }
}
