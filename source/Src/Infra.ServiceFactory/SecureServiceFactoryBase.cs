namespace DotFramework.Infra.ServiceFactory
{
    public class SecureServiceFactoryBase<TServiceFactory> : ServiceFactoryBase<TServiceFactory, AuthorizationInterceptionBehavior, AuthorizationEndpointBehavior> where TServiceFactory : class
    {
    }
}
