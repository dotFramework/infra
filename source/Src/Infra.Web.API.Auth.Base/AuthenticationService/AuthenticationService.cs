using DotFramework.Core;
using DotFramework.Infra.Security;
using DotFramework.Infra.Security.Model;
using DotFramework.Infra.Web;
using DotFramework.Infra.Web.API;
#if NETFRAMEWORK
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using System.Web.Http;
using System.Web.Http.Results;
#else
using Microsoft.AspNetCore.Mvc;
#endif
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using DotFramework.Infra.Web.API.Auth.Base.Providers;
using System.IO;
using System.Threading.Tasks;

namespace DotFramework.Infra.Web.API.Auth.Base
{
    public class AuthenticationService : HttpContextProvider, IAuthenticationService
    {
        #region Constructor

        public AuthenticationService()
        {
            _AuthContext = AuthContextProvider.AuthContext;
        }

        public void Initialize(ITokenProvider tokenProvider)
        {
            _TokenProvider = tokenProvider;
        }

        #endregion

        #region Variables

        private readonly IAuthContext _AuthContext;
        private ITokenProvider _TokenProvider;

        #endregion

        #region Properties

        public ITokenProvider TokenProvider => _TokenProvider;

        #endregion

        #region IAuthenticationService

        public virtual async Task<AuthenticateClientResponse> AuthenticateClient(AuthenticateClientRequest request)
        {
            try
            {
                var identity = await AuthService.Instance.AuthenticateClient(request);
                return CreateClientIdentity(identity);
            }
            catch (HttpException<AuthErrorResult> ex)
            {
                throw new ApiCustomException(ex.ErrorResult.ErrorDescription, ex);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public virtual async Task<ObtainLocalAccessTokenResponse> InternalLogin(LoginRequest request)
        {
            try
            {
                var tokenResponse = await AuthService.Instance.Authenticate(new LoginBindingModel
                {
                    UserName = request.UserName,
                    Password = request.Password,
                    AppIdentifier = request.AppIdentifier,
                });

                if (String.IsNullOrWhiteSpace(tokenResponse.AppIdentifier) || String.IsNullOrWhiteSpace(tokenResponse.CallbackURI))
                {
                    return InternalAuthenticate(tokenResponse, request.Initiator);
                }
                else
                {
                    var response = new ObtainLocalAccessTokenResponse
                    {
                        AccessToken = tokenResponse.AccessToken,
                        //GroupToken = tokenResponse.GroupToken,
                        RefreshToken = tokenResponse.RefreshToken,
                        TokenType = tokenResponse.TokenType,
                        UserName = tokenResponse.UserName,
                        Email = tokenResponse.Email,
                        //FullName = tokenResponse.FullName,
                        //FirstName = tokenResponse.FirstName,
                        //LastName = tokenResponse.LastName,
                        IssuedAt = CloneDate(tokenResponse.IssuedAt),
                        ExpiresIn = tokenResponse.ExpiresIn,
                        ExpiresAt = CloneDate(tokenResponse.ExpiresAt),
                        //ExternalUserID = tokenResponse.ExternalUserID,
                        //ProfilePicture = tokenResponse.ProfilePicture,
                        //Friends = tokenResponse.Friends,
                        //Roles = tokenResponse.Roles,
                        //FirstTimeLogin = tokenResponse.FirstTimeLogin,
                        //Initiator = request.Initiator,
                        AppIdentifier = request.AppIdentifier,
                        CallbackURI = tokenResponse.CallbackURI
                    };

                    return response;
                }
            }
            catch (HttpException<AuthErrorResult> ex)
            {
                throw new ApiCustomException(ex.ErrorResult.ErrorDescription, ex);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<ObtainLocalAccessTokenResponse> Login(LoginRequest request)
        {
            await PreLogin(request);
            var response = await InternalLogin(request);
            await PostLogin(request, response);

            return response;
        }

        public virtual Task PreLogin(LoginRequest request)
        {
            return Task.CompletedTask;
        }

        public virtual Task PostLogin(LoginRequest request, ObtainLocalAccessTokenResponse response)
        {
            return Task.CompletedTask;
        }

        private DateTime CloneDate(DateTime datetime)
        {
            return new DateTime(datetime.Year, datetime.Month, datetime.Day, datetime.Hour, datetime.Minute, datetime.Second);
        }

        public virtual async Task<ObtainLocalAccessTokenResponse> Reauthenticate(ReauthenticateRequest request)
        {
            try
            {
                var identity = (_AuthContext.User.Identity as ClaimsIdentity);
                var accessToken = identity.FindFirst(CustomClaimTypes.SSOAccessToken)?.Value;

                try
                {
                    var appsecret_proof = request.AppSecretProof ?? identity.FindFirst(CustomClaimTypes.AppSecretProof)?.Value;

                    var tokenResponse = await VerifyToken(new VerifyTokenRequest
                    {
                        AccessToken = accessToken,
                        AppSecretProof = appsecret_proof,
                    });

                    return InternalAuthenticate(tokenResponse, accessToken, request.RefreshToken, request.Initiator, appsecret_proof);
                }
                catch (UnauthorizedHttpException)
                {
                    _TokenProvider.Revoke(_AuthContext.AccessToken);
                    throw;
                }
            }
            catch (HttpException<AuthErrorResult> ex)
            {
                throw new ApiCustomException(ex.ErrorResult.ErrorDescription, ex);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public virtual async Task<LogoutResponse> Logout()
        {
            try
            {
                await Logout(_AuthContext?.AccessToken, (_AuthContext?.User?.Identity as ClaimsIdentity)?.Claims);
            }
            catch
            {

            }

            return new LogoutResponse();
        }

        public virtual async Task<LogoutResponse> Logout(string accessToken)
        {
            try
            {
                await Logout(accessToken, TokenProvider.UnProtect(accessToken));
            }
            catch
            {

            }

            return new LogoutResponse();
        }

        public virtual async Task ForceLogout(string groupToken)
        {
            try
            {
                await RevokeGroupToken(groupToken);
            }
            catch
            {

            }
        }

        private async Task Logout(string accessToken, IEnumerable<Claim> claims)
        {
            if (!String.IsNullOrWhiteSpace(accessToken))
            {
                string groupToken = claims.FirstOrDefault(c => c.Type == CustomClaimTypes.GroupToken)?.Value;
                var ssoAccessToken = claims.FirstOrDefault(c => c.Type == CustomClaimTypes.SSOAccessToken)?.Value;

                try
                {
                    await AuthService.Instance.SignOut(new AuthorizationBindingModel { AccessToken = ssoAccessToken });
                    await RevokeToken(accessToken, groupToken, false);
                }
                catch (UnauthorizedHttpException)
                {
                    await RevokeToken(accessToken, groupToken, true);
                }
                catch (HttpException<AuthErrorResult> ex)
                {
                    await RevokeToken(accessToken, groupToken, true);
                    throw new ApiCustomException(ex.ErrorResult.ErrorDescription, ex);
                }
                catch (Exception)
                {
                    await RevokeToken(accessToken, groupToken, true);
                    throw;
                }
            }
        }

        public virtual RedirectResult GetExternalLogin(string provider, string redirect_url)
        {
            try
            {
                var context = AuthService.Instance.ExternalLogin(ApplicationManager.Instance.ApplicationCode, provider, redirect_url);

#if NETFRAMEWORK
                return new RedirectResult(new Uri(context.RedirectUrl), _AuthContext.Request);
#else
                return new RedirectResult(context.RedirectUrl);
#endif
            }
            catch (HttpException<AuthErrorResult> ex)
            {
                throw new ApiCustomException(ex.ErrorResult.ErrorDescription, ex);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public virtual async Task<ObtainLocalAccessTokenResponse> ObtainLocalAccessToken(ObtainLocalAccessTokenRequest request)
        {
            try
            {
                var tokenResponse = await AuthService.Instance.ExternalAuthenticate(new ObtainLocalAccessTokenBindingModel
                {
                    AccessToken = request.AccessToken,
                    Provider = request.Provider,
                    ClientID = ApplicationManager.Instance.ApplicationCode
                });

                return InternalAuthenticate(tokenResponse, request.Initiator);
            }
            catch (HttpException<AuthErrorResult> ex)
            {
                throw new ApiCustomException(ex.ErrorResult.ErrorDescription, ex);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public virtual async Task<ObtainLocalAccessTokenResponse> RefreshToken(RefreshTokenRequest request)
        {
            try
            {
                var tokenResponse = await AuthService.Instance.RefreshToken(new RefreshTokenBindingModel
                {
                    RefreshToken = request.RefreshToken,
                });

                if (String.IsNullOrWhiteSpace(tokenResponse.AppIdentifier) || String.IsNullOrWhiteSpace(tokenResponse.CallbackURI))
                {
                    return InternalAuthenticate(tokenResponse, request.Initiator);
                }
                else
                {
                    var response = new ObtainLocalAccessTokenResponse
                    {
                        AccessToken = tokenResponse.AccessToken,
                        //GroupToken = tokenResponse.GroupToken,
                        RefreshToken = tokenResponse.RefreshToken,
                        TokenType = tokenResponse.TokenType,
                        UserName = tokenResponse.UserName,
                        Email = tokenResponse.Email,
                        //FullName = tokenResponse.FullName,
                        //FirstName = tokenResponse.FirstName,
                        //LastName = tokenResponse.LastName,
                        IssuedAt = CloneDate(tokenResponse.IssuedAt),
                        ExpiresIn = tokenResponse.ExpiresIn,
                        ExpiresAt = CloneDate(tokenResponse.ExpiresAt),
                        //ExternalUserID = tokenResponse.ExternalUserID,
                        //ProfilePicture = tokenResponse.ProfilePicture,
                        //Friends = tokenResponse.Friends,
                        //Roles = tokenResponse.Roles,
                        //FirstTimeLogin = tokenResponse.FirstTimeLogin,
                        //Initiator = request.Initiator,
                        AppIdentifier = tokenResponse.AppIdentifier,
                        CallbackURI = tokenResponse.CallbackURI
                    };

                    return response;
                }
            }
            catch (HttpException<AuthErrorResult> ex)
            {
                if (ex.ErrorResult.Error == "invalid_grant")
                {
                    throw new UnauthorizedHttpException();
                }
                else
                {
                    throw new ApiCustomException(ex.ErrorResult.ErrorDescription, ex);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public virtual async Task<AuthenticateResponseModel> Authenticate(AuthenticateRequest request)
        {
            try
            {
                VerifyTokenResponseModel verifyTokenResponse = await VerifyToken(new VerifyTokenRequest { AccessToken = request.AccessToken, AppSecretProof = request.AppSecretProof });
                AuthenticateResponseModel response = CreateIdentity(verifyTokenResponse, request.AccessToken, request.Initiator);

                return response;
            }
            catch (HttpException<AuthErrorResult> ex)
            {
                throw new ApiCustomException(ex.ErrorResult.ErrorDescription, ex);
            }
            catch (Exception)
            {
                throw;
            }
        }

        private static async Task<VerifyTokenResponseModel> VerifyToken(VerifyTokenRequest request)
        {
            var verifyTokenResponse = await AuthService.Instance.VerifyToken(request);

            if (verifyTokenResponse == null)
            {
                throw new UnauthorizedHttpException();
            }

            return verifyTokenResponse;
        }

        public virtual async Task<GenerateClientEncryptionTokenResponse> GenerateClientEncryptionToken(GenerateClientEncryptionTokenRequest request)
        {
            try
            {
                return await AuthService.Instance.GenerateClientEncryptionToken(request);
            }
            catch (HttpException<AuthErrorResult> ex)
            {
                throw new ApiCustomException(ex.ErrorResult.ErrorDescription, ex);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public virtual async Task<ActivateClientResponse> ActivateClient(ActivateClientRequest request)
        {
            try
            {
                return await AuthService.Instance.ActivateClient(request);
            }
            catch (HttpException<AuthErrorResult> ex)
            {
                throw new ApiCustomException(ex.ErrorResult.ErrorDescription, ex);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public virtual async Task<ActivateResponse> Activate(ActivateRequest request)
        {
            try
            {
                return await AuthService.Instance.Activate(request);
            }
            catch (HttpException<AuthErrorResult> ex)
            {
                throw new ApiCustomException(ex.ErrorResult.ErrorDescription, ex);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public virtual async Task<ActivateLocalAccessTokenResponse> GrantActivation(GrantActivationRequest request)
        {
            try
            {
                var response = await AuthService.Instance.GrantActivation(new GrantActivationBindingModel
                {
                    Initiator = request.Initiator,
                    Token = request.Token,
                    UserName = request.UserName
                });

                var identity = CreateIdentity<ActivateLocalAccessTokenResponse>(response, response.AccessToken, request.Initiator);

                identity.ResetPasswordToken = response.ResetPasswordToken;

                return identity;
            }
            catch (HttpException<AuthErrorResult> ex)
            {
                throw new ApiCustomException(ex.ErrorResult.ErrorDescription, ex);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public virtual async Task<ObtainLocalAccessTokenResponse> PasswordlessLogin(GrantActivationRequest request)
        {
            try
            {
                var response = await AuthService.Instance.GrantActivation(new GrantActivationBindingModel
                {
                    Initiator = request.Initiator,
                    Token = request.Token,
                    UserName = request.UserName
                });

                return InternalAuthenticate(response, request.Initiator);
            }
            catch (HttpException<AuthErrorResult> ex)
            {
                throw new ApiCustomException(ex.ErrorResult.ErrorDescription, ex);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public virtual async Task<ActivateLocalAccessTokenResponse> GrantInternalActivation(GrantInternalActivationRequest request)
        {
            try
            {
                var response = await AuthService.Instance.GrantInternalActivation(new GrantInternalActivationBindingModel
                {
                    Initiator = request.Initiator,
                    UserName = request.UserName
                });

                var identity = CreateIdentity<ActivateLocalAccessTokenResponse>(response, response.AccessToken, request.Initiator, null, request.AdditionalClaims);
                identity.RefreshToken = response.RefreshToken;

                return identity;
            }
            catch (HttpException<AuthErrorResult> ex)
            {
                throw new ApiCustomException(ex.ErrorResult.ErrorDescription, ex);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public virtual async Task<VerifyClientTokenResponseModel> VerifyClientToken(VerifyTokenRequest request)
        {
            try
            {
                var claims = _TokenProvider.UnProtect(request.AccessToken);
                request.AccessToken = claims.FirstOrDefault(c => c.Type == CustomClaimTypes.SSOAccessToken)?.Value;

                return await AuthService.Instance.VerifyClientToken(request);
            }
            catch (HttpException<AuthErrorResult> ex)
            {
                throw new ApiCustomException(ex.ErrorResult.ErrorDescription, ex);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public virtual Task<UserDataResponseModel> Authorize(AuthorizeRequest request)
        {
            try
            {
                throw new NotImplementedException();
            }
            catch (HttpException<AuthErrorResult> ex)
            {
                throw new ApiCustomException(ex.ErrorResult.ErrorDescription, ex);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public virtual UserDataResponseModel GetUserData()
        {
            try
            {
                if (_AuthContext?.UserType == UserTypes.User)
                {
                    var user = _AuthContext?.User;

                    if (user != null)
                    {
                        return new UserDataResponseModel
                        {
                            UserName = user.Identity.Name,
                            Email = user.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value,
                            FullName = user.Claims.FirstOrDefault(c => c.Type == CustomClaimTypes.DisplayName)?.Value,
                            FirstName = user.Claims.FirstOrDefault(c => c.Type == ClaimTypes.GivenName)?.Value,
                            LastName = user.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Surname)?.Value,
                            ProfilePicture = user.Claims.FirstOrDefault(c => c.Type == CustomClaimTypes.ProfilePicture)?.Value,
                            Roles = user.Claims.Where(c => c.Type == ClaimTypes.Role).Select(c => c.Value).ToListOrDefault()
                        };
                    }
                    else
                    {
                        throw new UnauthorizedHttpException();
                    }
                }
                else
                {
                    throw new UnauthorizedHttpException();
                }
            }
            catch (HttpException<AuthErrorResult> ex)
            {
                throw new ApiCustomException(ex.ErrorResult.ErrorDescription, ex);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public virtual ClientDataResponseModel GetClientData()
        {
            try
            {
                if (_AuthContext?.UserType == UserTypes.Client)
                {
                    var user = _AuthContext?.User;

                    if (user != null)
                    {
                        return new ClientDataResponseModel
                        {
                            ClientIdentifier = user.Identity.Name,
                            ClientName = user.Claims.FirstOrDefault(c => c.Type == CustomClaimTypes.ClientName)?.Value
                        };
                    }
                    else
                    {
                        throw new UnauthorizedHttpException();
                    }
                }
                else
                {
                    throw new UnauthorizedHttpException();
                }
            }
            catch (HttpException<AuthErrorResult> ex)
            {
                throw new ApiCustomException(ex.ErrorResult.ErrorDescription, ex);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public virtual async Task<AuthenticateResponseModel> ValidateToken()
        {
            try
            {
                var identity = (_AuthContext.User.Identity as ClaimsIdentity);
                var accessToken = identity.FindFirst(CustomClaimTypes.SSOAccessToken)?.Value;

                var verifyTokenResponse = await VerifyToken(new VerifyTokenRequest { AccessToken = accessToken });

                AuthenticateResponseModel response = new AuthenticateResponseModel
                {
                    AccessToken = _AuthContext.AccessToken,
                    GroupToken = verifyTokenResponse.GroupToken,
                    TokenType = identity.AuthenticationType,
                    UserName = verifyTokenResponse.UserName,
                    Email = verifyTokenResponse.Email,
                    FullName = verifyTokenResponse.FullName,
                    FirstName = verifyTokenResponse.FirstName,
                    LastName = verifyTokenResponse.LastName,
                    IssuedAt = verifyTokenResponse.IssuedAt,
                    ExpiresIn = verifyTokenResponse.ExpiresIn,
                    ExpiresAt = verifyTokenResponse.ExpiresAt,
                    ExternalUserID = verifyTokenResponse.ExternalUserID,
                    ProfilePicture = verifyTokenResponse.ProfilePicture,
                    Friends = verifyTokenResponse.Friends,
                    Roles = verifyTokenResponse.Roles,
                    FirstTimeLogin = verifyTokenResponse.FirstTimeLogin,
                    Initiator = _AuthContext.Initiator,
                    Properties = verifyTokenResponse.Properties
                };

                return response;
            }
            catch (HttpException<AuthErrorResult> ex)
            {
                throw new ApiCustomException(ex.ErrorResult.ErrorDescription, ex);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public virtual async Task InternalRegisterWithoutAuthentication(RegisterRequest request)
        {
            try
            {
                await AuthService.Instance.Register(new RegisterBindingModel
                {
                    UserName = String.IsNullOrWhiteSpace(request.UserName) ? request.Email : request.UserName,
                    Email = request.Email,
                    FirstName = request.FirstName,
                    LastName = request.LastName,
                    Password = request.Password,
                    ConfirmPassword = request.ConfirmPassword
                });
            }
            catch (HttpException<AuthErrorResult> ex)
            {
                throw new ApiCustomException(ex.ErrorResult.ErrorDescription, ex);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public virtual async Task<ObtainLocalAccessTokenResponse> InternalRegister(RegisterRequest request)
        {
            try
            {
                var tokenResponse = await AuthService.Instance.RegisterAndLogin(new RegisterBindingModel
                {
                    UserName = String.IsNullOrWhiteSpace(request.UserName) ? request.Email : request.UserName,
                    Email = request.Email,
                    FirstName = request.FirstName,
                    LastName = request.LastName,
                    Password = request.Password,
                    ConfirmPassword = request.ConfirmPassword
                });

                return InternalAuthenticate(tokenResponse, request.Initiator);
            }
            catch (HttpException<AuthErrorResult> ex)
            {
                throw new ApiCustomException(ex.ErrorResult.ErrorDescription, ex);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<ObtainLocalAccessTokenResponse> Register(RegisterRequest request)
        {
            await PreRegister(request);
            var response = await InternalRegister(request);
            await PostRegister(request, response);

            return response;
        }

        public virtual Task PreRegister(RegisterRequest request)
        {
            return Task.CompletedTask;
        }

        public virtual Task PostRegister(RegisterRequest request, ObtainLocalAccessTokenResponse response)
        {
            return Task.CompletedTask;
        }

        public virtual async Task<ChangePasswordResponse> ChangePassword(ChangePasswordRequest request)
        {
            try
            {
                var accessToken = (_AuthContext.User.Identity as ClaimsIdentity).FindFirst(CustomClaimTypes.SSOAccessToken)?.Value;

                await AuthService.Instance.ChangePassword(new ChangePasswordBindingModel
                {
                    UserName = request.UserName,
                    ConfirmPassword = request.ConfirmPassword,
                    NewPassword = request.NewPassword,
                    OldPassword = request.OldPassword
                }, accessToken);

                return new ChangePasswordResponse();
            }
            catch (HttpException<AuthErrorResult> ex)
            {
                throw new ApiCustomException(ex.ErrorResult.ErrorDescription, ex);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public virtual async Task UpdateUserInfo(UpdateUserInfoRequest request)
        {
            try
            {
                string ssoAccessToken;

                if (!String.IsNullOrWhiteSpace(request.AccessToken))
                {
                    var claims = TokenProvider.UnProtect(request.AccessToken);
                    ssoAccessToken = claims.First(c => c.Type == CustomClaimTypes.SSOAccessToken).Value;
                }
                else
                {
                    ssoAccessToken = (_AuthContext.User.Identity as ClaimsIdentity).FindFirst(CustomClaimTypes.SSOAccessToken)?.Value;
                }

                await AuthService.Instance.UpdateUserInfo(request, ssoAccessToken);
            }
            catch (HttpException<AuthErrorResult> ex)
            {
                throw new ApiCustomException(ex.ErrorResult.ErrorDescription, ex);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public virtual async Task<ForgetPasswordResponse> InternalForgetPassword(ForgetPasswordRequest request)
        {
            try
            {
                await AuthService.Instance.ForgetPassword(new ForgetPasswordBindingModel
                {
                    UserName = request.UserName,
                    RedirectUrl = request.RedirectUrl,
                    TemplateKeyValues = request.TemplateKeyValues
                });

                return new ForgetPasswordResponse();
            }
            catch (HttpException<AuthErrorResult> ex)
            {
                throw new ApiCustomException(ex.ErrorResult.ErrorDescription, ex);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<ForgetPasswordResponse> ForgetPassword(ForgetPasswordRequest request)
        {
            await PreForgetPassword(request);
            var response = await InternalForgetPassword(request);
            await PostForgetPassword(request, response);

            return response;
        }

        public virtual Task PreForgetPassword(ForgetPasswordRequest request)
        {
            return Task.CompletedTask;
        }

        public virtual Task PostForgetPassword(ForgetPasswordRequest request, ForgetPasswordResponse response)
        {
            return Task.CompletedTask;
        }

        public virtual async Task<ResetPasswordResponse> ResetPassword(ResetPasswordRequest request)
        {
            try
            {
                await AuthService.Instance.ResetPassword(new ResetPasswordBindingModel
                {
                    UserName = request.UserName,
                    Token = request.Token,
                    NewPassword = request.NewPassword,
                    ConfirmPassword = request.ConfirmPassword
                });

                return new ResetPasswordResponse();
            }
            catch (HttpException<AuthErrorResult> ex)
            {
                throw new ApiCustomException(ex.ErrorResult.ErrorDescription, ex);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public virtual async Task<CreateClientResponse> CreateClient(CreateClientRequest request)
        {
            try
            {
                var identity = (_AuthContext.User.Identity as ClaimsIdentity);
                var accessToken = identity.FindFirst(CustomClaimTypes.SSOAccessToken)?.Value;

                if (String.IsNullOrWhiteSpace(accessToken))
                {
                    throw new UnauthorizedAccessException();
                }

                return await AuthService.Instance.CreateClient(request, accessToken);
            }
            catch (HttpException<AuthErrorResult> ex)
            {
                throw new ApiCustomException(ex.ErrorResult.ErrorDescription, ex);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public virtual async Task<GetClientResponse> GetClient(GetClientRequest request)
        {
            try
            {
                return await AuthService.Instance.GetClient(request);
            }
            catch (HttpException<AuthErrorResult> ex)
            {
                throw new ApiCustomException(ex.ErrorResult.ErrorDescription, ex);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public virtual async Task ConfirmEmail(ConfirmEmailBindingModel request)
        {
            try
            {
                await AuthService.Instance.ConfirmEmail(request);
            }
            catch (HttpException<AuthErrorResult> ex)
            {
                throw new ApiCustomException(ex.ErrorResult.ErrorDescription, ex);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public virtual async Task<RedirectResult> ConfirmEmailCallback(ConfirmEmailCallbackBindingModel model)
        {
            try
            {
                var context = await AuthService.Instance.ConfirmEmailCallback(model);

#if NETFRAMEWORK
                return new RedirectResult(new Uri(context.RedirectUrl), _AuthContext.Request);
#else
                return new RedirectResult(context.RedirectUrl);
#endif
            }
            catch (HttpException<AuthErrorResult> ex)
            {
                throw new ApiCustomException(ex.ErrorResult.ErrorDescription, ex);
            }
            catch (Exception)
            {
                throw;
            }
        }


        #endregion

        #region Protected Methods

        protected ObtainLocalAccessTokenResponse InternalAuthenticate(TokenResponseModel authenticateResponse, string initiator)
        {
            AuthenticateResponseModel identity = CreateIdentity(authenticateResponse, authenticateResponse.AccessToken, initiator);

            var response = new ObtainLocalAccessTokenResponse
            {
                AccessToken = identity.AccessToken,
                GroupToken = identity.GroupToken,
                RefreshToken = authenticateResponse.RefreshToken,
                TokenType = identity.TokenType,
                UserName = identity.UserName,
                Email = identity.Email,
                FullName = identity.FullName,
                FirstName = identity.FirstName,
                LastName = identity.LastName,
                IssuedAt = identity.IssuedAt,
                ExpiresIn = identity.ExpiresIn,
                ExpiresAt = identity.ExpiresAt,
                ExternalUserID = identity.ExternalUserID,
                ProfilePicture = identity.ProfilePicture,
                Friends = identity.Friends,
                Roles = identity.Roles,
                FirstTimeLogin = identity.FirstTimeLogin,
                Initiator = initiator
            };

            return response;
        }

        protected ObtainLocalAccessTokenResponse InternalAuthenticate(VerifyTokenResponseModel authenticateResponse, string ssoAccessToken, string refreshToken, string initiator, string appsecret_proof = null)
        {
            AuthenticateResponseModel identity = CreateIdentity(authenticateResponse, ssoAccessToken, initiator, appsecret_proof);

            var response = new ObtainLocalAccessTokenResponse
            {
                AccessToken = identity.AccessToken,
                GroupToken = identity.GroupToken,
                RefreshToken = refreshToken,
                TokenType = identity.TokenType,
                UserName = identity.UserName,
                Email = identity.Email,
                FullName = identity.FullName,
                FirstName = identity.FirstName,
                LastName = identity.LastName,
                IssuedAt = identity.IssuedAt,
                ExpiresIn = identity.ExpiresIn,
                ExpiresAt = identity.ExpiresAt,
                ExternalUserID = identity.ExternalUserID,
                ProfilePicture = identity.ProfilePicture,
                Friends = identity.Friends,
                Roles = identity.Roles,
                FirstTimeLogin = identity.FirstTimeLogin,
                Initiator = initiator
            };

            return response;
        }

        protected AuthenticateResponseModel CreateIdentity(IUserIdentity identity, string ssoAccessToken, string initiator, string appsecret_proof = null)
        {
            return CreateIdentity<AuthenticateResponseModel>(identity, ssoAccessToken, initiator, appsecret_proof);
        }

        protected TUserIdentity CreateIdentity<TUserIdentity>(IUserIdentity identity, string ssoAccessToken, string initiator, string appsecret_proof = null, IEnumerable<Claim> additionalClaims = null) where TUserIdentity : IUserIdentity, IAccessToken, IInitiator, new()
        {
            var claims = new List<Claim>();

            claims.Add(new Claim(ClaimTypes.NameIdentifier, identity.UserName));
            claims.Add(new Claim(ClaimTypes.Name, identity.UserName));
            claims.Add(new Claim(CustomClaimTypes.GroupToken, identity.GroupToken));
            claims.Add(new Claim(CustomClaimTypes.UserType, UserTypes.User));
            claims.Add(new Claim(ClaimTypes.Email, identity.Email));
            claims.Add(new Claim(CustomClaimTypes.SSOAccessToken, ssoAccessToken));

            if (!String.IsNullOrEmpty(initiator))
            {
                claims.Add(new Claim(CustomClaimTypes.Initiator, initiator));
            }

            if (!String.IsNullOrEmpty(appsecret_proof))
            {
                claims.Add(new Claim(CustomClaimTypes.AppSecretProof, appsecret_proof));
            }

            if (!String.IsNullOrEmpty(identity.FullName))
            {
                claims.Add(new Claim(CustomClaimTypes.DisplayName, identity.FullName));
            }

            if (!String.IsNullOrEmpty(identity.FirstName))
            {
                claims.Add(new Claim(ClaimTypes.GivenName, identity.FirstName));
            }

            if (!String.IsNullOrEmpty(identity.LastName))
            {
                claims.Add(new Claim(ClaimTypes.Surname, identity.LastName));
            }

            if (!String.IsNullOrEmpty(identity.ProfilePicture))
            {
                claims.Add(new Claim(CustomClaimTypes.ProfilePicture, identity.ProfilePicture));
            }

            if (identity.Roles != null)
            {
                foreach (var role in identity.Roles)
                {
                    claims.Add(new Claim(ClaimTypes.Role, role));
                }
            }

            if (CustomClaims != null)
            {
                foreach (var claim in CustomClaims)
                {
                    claims.Add(claim);
                }
            }

            if (additionalClaims != null)
            {
                foreach (var claim in additionalClaims)
                {
                    claims.Add(claim);
                }
            }

            var baseTicket = _TokenProvider.GetBaseTicket(claims);

            TUserIdentity response = new TUserIdentity
            {
                AccessToken = baseTicket.ProtectedToken,
                GroupToken = identity.GroupToken,
                TokenType = baseTicket.TokenScheme,
                UserName = identity.UserName,
                Email = identity.Email,
                FullName = identity.FullName,
                FirstName = identity.FirstName,
                LastName = identity.LastName,
                IssuedAt = baseTicket.IssuedAt,
                ExpiresIn = baseTicket.ExpiresIn,
                ExpiresAt = baseTicket.ExpiresAt,
                ExternalUserID = identity.ExternalUserID,
                ProfilePicture = identity.ProfilePicture,
                Friends = identity.Friends,
                Roles = identity.Roles,
                FirstTimeLogin = identity.FirstTimeLogin,
                Initiator = initiator,
                Properties =
                    (CustomClaims ?? Enumerable.Empty<Claim>())
                        .Concat(additionalClaims ?? Enumerable.Empty<Claim>())
                        .ToDictionary(c => c.Type, c => c.Value)
            };

            return response;
        }

        protected AuthenticateClientResponse CreateClientIdentity(AuthenticateClientResponse identity)
        {
            var claims = new List<Claim>();

            claims.Add(new Claim(ClaimTypes.Name, identity.ClientIdentifier));
            claims.Add(new Claim(CustomClaimTypes.UserType, UserTypes.Client));
            claims.Add(new Claim(CustomClaimTypes.ClientName, identity.ClientName));
            claims.Add(new Claim(CustomClaimTypes.SSOAccessToken, identity.AccessToken));

            if (CustomClientClaims != null)
            {
                foreach (var claim in CustomClientClaims)
                {
                    claims.Add(claim);
                }
            }

            var baseTicket = _TokenProvider.GetBaseTicket(claims);

            AuthenticateClientResponse response = new AuthenticateClientResponse
            {
                AccessToken = baseTicket.ProtectedToken,
                TokenType = baseTicket.TokenScheme,
                ClientIdentifier = identity.ClientIdentifier,
                ClientName = identity.ClientName,
                IssuedAt = baseTicket.IssuedAt,
                ExpiresIn = baseTicket.ExpiresIn,
                ExpiresAt = baseTicket.ExpiresAt,
                Properties = CustomClaims?.ToDictionary(c => c.Type, c => c.Value)
            };

            return response;
        }

        protected virtual IEnumerable<Claim> CustomClaims
        {
            get
            {
                return null;
            }
        }

        protected virtual IEnumerable<Claim> CustomClientClaims
        {
            get
            {
                return null;
            }
        }

        #endregion

        #region Private Methods

        private async Task RevokeToken(string accessToken, string groupToken, bool force)
        {
            try
            {
                //Todo: Commented due to disabled session
                //Context?.Session?.Clear();
                _TokenProvider.Revoke(accessToken);

                if (force && !String.IsNullOrEmpty(groupToken))
                {
                    await AuthService.Instance.ForceLogout(new ForceLogoutRequest { GroupToken = groupToken });
                }
            }
            catch (Exception)
            {

            }
        }

        private async Task RevokeGroupToken(string groupToken)
        {
            try
            {
                if (!String.IsNullOrEmpty(groupToken))
                {
                    _TokenProvider.RevokeGroup(groupToken);
                    await AuthService.Instance.ForceLogout(new ForceLogoutRequest { GroupToken = groupToken });
                }
            }
            catch (Exception)
            {

            }
        }

        #endregion

    }
}