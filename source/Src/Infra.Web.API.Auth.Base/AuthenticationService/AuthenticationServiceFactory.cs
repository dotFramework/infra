using DotFramework.Core.DependencyInjection;
using System;

namespace DotFramework.Infra.Web.API.Auth.Base
{
    public class AuthenticationServiceFactory : VirtualMethodFactoryBase<AuthenticationServiceFactory, IAuthenticationService, AuthenticationServiceInterceptionBehavior>
    {
        protected override ContainerLifeTime ContainerLifeTime => ContainerLifeTime.Singleton;

        protected override bool HandleException(ref Exception ex)
        {
            return true;
        }
    }
}
