using DotFramework.Infra.Security.Model;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using DotFramework.Infra.Web.API.Auth.Base.Providers;
#if NETFRAMEWORK
using System.Web.Http.Results;
#else
using Microsoft.AspNetCore.Mvc;
#endif

namespace DotFramework.Infra.Web.API.Auth.Base
{
    public interface IAuthenticationService
    {
        ITokenProvider TokenProvider { get; }

        void Initialize(ITokenProvider tokenProvider);
        Task<AuthenticateClientResponse> AuthenticateClient(AuthenticateClientRequest request);
        Task<ObtainLocalAccessTokenResponse> Login(LoginRequest request);
        Task<ObtainLocalAccessTokenResponse> InternalLogin(LoginRequest request);
        Task<ObtainLocalAccessTokenResponse> Reauthenticate(ReauthenticateRequest request);
        Task<LogoutResponse> Logout();
        Task<LogoutResponse> Logout(string accessToken);
        Task ForceLogout(string groupToken);
        RedirectResult GetExternalLogin(string provider, string redirect_url); 
        Task<ObtainLocalAccessTokenResponse> ObtainLocalAccessToken(ObtainLocalAccessTokenRequest request);
        Task<ObtainLocalAccessTokenResponse> RefreshToken(RefreshTokenRequest request);
        Task<AuthenticateResponseModel> Authenticate(AuthenticateRequest request);
        Task<GenerateClientEncryptionTokenResponse> GenerateClientEncryptionToken(GenerateClientEncryptionTokenRequest request);
        Task<ActivateClientResponse> ActivateClient(ActivateClientRequest request);
        Task<ActivateResponse> Activate(ActivateRequest request);
        Task<ActivateLocalAccessTokenResponse> GrantActivation(GrantActivationRequest request);
        Task<ObtainLocalAccessTokenResponse> PasswordlessLogin(GrantActivationRequest request);
        Task<ActivateLocalAccessTokenResponse> GrantInternalActivation(GrantInternalActivationRequest request);
        Task<VerifyClientTokenResponseModel> VerifyClientToken(VerifyTokenRequest request);
        Task<UserDataResponseModel> Authorize(AuthorizeRequest request);
        UserDataResponseModel GetUserData();
        ClientDataResponseModel GetClientData();
        Task<AuthenticateResponseModel> ValidateToken();
        Task InternalRegisterWithoutAuthentication(RegisterRequest request);
        Task<ObtainLocalAccessTokenResponse> InternalRegister(RegisterRequest request);
        Task<ObtainLocalAccessTokenResponse> Register(RegisterRequest request);
        Task PreRegister(RegisterRequest request);
        Task PostRegister(RegisterRequest request, ObtainLocalAccessTokenResponse response);
        Task<ForgetPasswordResponse> InternalForgetPassword(ForgetPasswordRequest request);
        Task<ForgetPasswordResponse> ForgetPassword(ForgetPasswordRequest request);
        Task<ChangePasswordResponse> ChangePassword(ChangePasswordRequest request);
        Task UpdateUserInfo(UpdateUserInfoRequest request);
        Task<ResetPasswordResponse> ResetPassword(ResetPasswordRequest request);
        Task<CreateClientResponse> CreateClient(CreateClientRequest request);

        Task<GetClientResponse> GetClient(GetClientRequest request);
        Task ConfirmEmail(ConfirmEmailBindingModel model);
        Task<RedirectResult> ConfirmEmailCallback(ConfirmEmailCallbackBindingModel model);
    }
}