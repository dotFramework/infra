﻿using DotFramework.Infra.ExceptionHandling;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Unity.Interception.InterceptionBehaviors;
using Unity.Interception.PolicyInjection.Pipeline;

namespace DotFramework.Infra.Web.API.Auth.Base
{
    public class AuthenticationServiceInterceptionBehavior : ExceptionHandlerInterceptionBehavior
    {
        public override IMethodReturn Invoke(IMethodInvocation input, GetNextInterceptionBehaviorDelegate getNext)
        {
            if (input.MethodBase.Name.StartsWith("get_") || input.MethodBase.Name.StartsWith("set_"))
            {
                return base.Invoke(input, getNext);
            }
            else
            {
                TraceLogManager.Instance.WriteInfoAsync("AuthenticationService", "Method Invocation Started", input.MethodBase);

                Stopwatch sw = new Stopwatch();
                sw.Start();

                var result = base.Invoke(input, getNext);

                sw.Stop();

                TraceLogManager.Instance.WriteInfoAsync("AuthenticationService", "Method Invocation Finished", input.MethodBase, sw.ElapsedMilliseconds.ToString());

                return result;
            }
        }

        public override bool HandleException(ref Exception ex, IMethodInvocation input)
        {
            return true;
        }
    }
}
