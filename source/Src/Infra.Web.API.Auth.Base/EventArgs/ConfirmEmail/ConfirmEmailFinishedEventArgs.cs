using DotFramework.Infra.Security.Model;
using System;

namespace DotFramework.Infra.Web.API.Auth.Base
{
    public class ConfirmEmailFinishedEventArgs : EventArgs
    {
        public readonly ConfirmEmailBindingModel Request;

        public ConfirmEmailFinishedEventArgs(ConfirmEmailBindingModel request)
        {
            Request = request;
        }
    }
}