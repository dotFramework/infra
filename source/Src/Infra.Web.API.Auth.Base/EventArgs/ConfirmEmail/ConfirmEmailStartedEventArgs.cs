using DotFramework.Infra.Security.Model;
using System;

namespace DotFramework.Infra.Web.API.Auth.Base
{
    public class ConfirmEmailStartedEventArgs : EventArgs
    {
        public readonly ConfirmEmailBindingModel Request;

        public ConfirmEmailStartedEventArgs(ConfirmEmailBindingModel request)
        {
            Request = request;
        }
    }
}