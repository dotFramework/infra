using DotFramework.Infra.Security.Model;
using System;
#if NETFRAMEWORK
using System.Web.Http.Results;
#else
using Microsoft.AspNetCore.Mvc;
#endif

namespace DotFramework.Infra.Web.API.Auth.Base
{
    public class ConfirmEmailCallbackFinishedEventArgs : EventArgs
    {
        public readonly ConfirmEmailCallbackBindingModel Request;
        public readonly RedirectResult RedirectResult;

        public ConfirmEmailCallbackFinishedEventArgs(ConfirmEmailCallbackBindingModel request, RedirectResult redirectResult)
        {
            Request = request;
            RedirectResult = redirectResult;
        }
    }
}