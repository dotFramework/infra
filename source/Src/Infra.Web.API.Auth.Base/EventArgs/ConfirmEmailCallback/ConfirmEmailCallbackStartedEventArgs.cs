using DotFramework.Infra.Security.Model;
using System;

namespace DotFramework.Infra.Web.API.Auth.Base
{
    public class ConfirmEmailCallbackStartedEventArgs : EventArgs
    {
        public readonly ConfirmEmailCallbackBindingModel Request;

        public ConfirmEmailCallbackStartedEventArgs(ConfirmEmailCallbackBindingModel request)
        {
            Request = request;
        }
    }
}