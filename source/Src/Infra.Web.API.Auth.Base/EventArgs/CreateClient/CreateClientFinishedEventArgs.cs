using DotFramework.Infra.Security.Model;
using System;

namespace DotFramework.Infra.Web.API.Auth.Base
{
    public class CreateClientFinishedEventArgs : EventArgs
    {
        public readonly CreateClientRequest Request;
        public readonly CreateClientResponse Response;

        public CreateClientFinishedEventArgs(CreateClientRequest request, CreateClientResponse response)
        {
            Request = request;
            Response = response;
        }
    }
}