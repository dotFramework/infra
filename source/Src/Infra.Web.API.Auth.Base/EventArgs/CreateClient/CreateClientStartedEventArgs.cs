using DotFramework.Infra.Security.Model;
using System;

namespace DotFramework.Infra.Web.API.Auth.Base
{
    public class CreateClientStartedEventArgs : EventArgs
    {
        public readonly CreateClientRequest Request;

        public CreateClientStartedEventArgs(CreateClientRequest request)
        {
            Request = request;
        }
    }
}