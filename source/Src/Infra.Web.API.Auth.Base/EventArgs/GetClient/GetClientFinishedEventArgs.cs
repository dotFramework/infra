using DotFramework.Infra.Security.Model;
using System;

namespace DotFramework.Infra.Web.API.Auth.Base
{
    public class GetClientFinishedEventArgs : EventArgs
    {
        public readonly GetClientRequest Request;
        public readonly GetClientResponse Response;

        public GetClientFinishedEventArgs(GetClientRequest request, GetClientResponse response)
        {
            Request = request;
            Response = response;
        }
    }
}