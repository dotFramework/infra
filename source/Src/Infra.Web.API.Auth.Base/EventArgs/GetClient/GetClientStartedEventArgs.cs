using DotFramework.Infra.Security.Model;
using System;

namespace DotFramework.Infra.Web.API.Auth.Base
{
    public class GetClientStartedEventArgs : EventArgs
    {
        public readonly GetClientRequest Request;

        public GetClientStartedEventArgs(GetClientRequest request)
        {
            Request = request;
        }
    }
}