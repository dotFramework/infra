using DotFramework.Infra.Security.Model;
using System;

namespace DotFramework.Infra.Web.API.Auth.Base
{
    public class GrantInternalActivationFinishedEventArgs : EventArgs
    {
        public readonly GrantInternalActivationRequest Request;
        public readonly ActivateLocalAccessTokenResponse Response;

        public GrantInternalActivationFinishedEventArgs(GrantInternalActivationRequest request, ActivateLocalAccessTokenResponse response)
        {
            Request = request;
            Response = response;
        }
    }
}