using DotFramework.Infra.Security.Model;
using System;

namespace DotFramework.Infra.Web.API.Auth.Base
{
    public class GrantInternalActivationStartedEventArgs : EventArgs
    {
        public readonly GrantInternalActivationRequest Request;

        public GrantInternalActivationStartedEventArgs(GrantInternalActivationRequest request)
        {
            Request = request;
        }
    }
}