using DotFramework.Infra.Security.Model;
using System;

namespace DotFramework.Infra.Web.API.Auth.Base
{
    public class InternalForgetPasswordStartedEventArgs : EventArgs
    {
        public readonly ForgetPasswordRequest Request;

        public InternalForgetPasswordStartedEventArgs(ForgetPasswordRequest request)
        {
            Request = request;
        }
    }
}