using DotFramework.Infra.Security.Model;
using System;

namespace DotFramework.Infra.Web.API.Auth.Base
{
    public class InternalLoginStartedEventArgs : EventArgs
    {
        public readonly LoginRequest Request;

        public InternalLoginStartedEventArgs(LoginRequest request)
        {
            Request = request;
        }
    }
}