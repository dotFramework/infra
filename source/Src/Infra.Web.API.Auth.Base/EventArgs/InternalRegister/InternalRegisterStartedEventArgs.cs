using DotFramework.Infra.Security.Model;
using System;

namespace DotFramework.Infra.Web.API.Auth.Base
{
    public class InternalRegisterStartedEventArgs : EventArgs
    {
        public readonly RegisterRequest Request;

        public InternalRegisterStartedEventArgs(RegisterRequest request)
        {
            Request = request;
        }
    }
}