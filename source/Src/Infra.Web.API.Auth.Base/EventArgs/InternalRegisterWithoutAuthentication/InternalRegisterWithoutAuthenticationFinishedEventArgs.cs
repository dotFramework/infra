using DotFramework.Infra.Security.Model;
using System;

namespace DotFramework.Infra.Web.API.Auth.Base
{
    public class InternalRegisterWithoutAuthenticationFinishedEventArgs : EventArgs
    {
        public readonly RegisterRequest Request;
        public readonly ObtainLocalAccessTokenResponse Response;

        public InternalRegisterWithoutAuthenticationFinishedEventArgs(RegisterRequest request)
        {
            Request = request;
        }
    }
}