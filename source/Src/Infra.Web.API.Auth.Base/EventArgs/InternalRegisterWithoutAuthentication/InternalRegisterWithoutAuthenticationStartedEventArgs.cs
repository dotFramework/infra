using DotFramework.Infra.Security.Model;
using System;

namespace DotFramework.Infra.Web.API.Auth.Base
{
    public class InternalRegisterWithoutAuthenticationStartedEventArgs : EventArgs
    {
        public readonly RegisterRequest Request;

        public InternalRegisterWithoutAuthenticationStartedEventArgs(RegisterRequest request)
        {
            Request = request;
        }
    }
}