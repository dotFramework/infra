using DotFramework.Infra.Security.Model;
using System;

namespace DotFramework.Infra.Web.API.Auth.Base
{
    public class LogoutFinishedEventArgs : EventArgs
    {
        public readonly LogoutResponse Response;

        public LogoutFinishedEventArgs(LogoutResponse response)
        {
            Response = response;
        }
    }
}