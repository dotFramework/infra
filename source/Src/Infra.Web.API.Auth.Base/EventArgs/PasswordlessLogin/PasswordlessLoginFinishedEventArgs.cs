using DotFramework.Infra.Security.Model;
using System;

namespace DotFramework.Infra.Web.API.Auth.Base
{
    public class PasswordlessLoginFinishedEventArgs : EventArgs
    {
        public readonly GrantActivationRequest Request;
        public readonly ObtainLocalAccessTokenResponse Response;

        public PasswordlessLoginFinishedEventArgs(GrantActivationRequest request, ObtainLocalAccessTokenResponse response)
        {
            Request = request;
            Response = response;
        }
    }
}