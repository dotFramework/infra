using DotFramework.Infra.Security.Model;
using System;

namespace DotFramework.Infra.Web.API.Auth.Base
{
    public class PasswordlessLoginStartedEventArgs : EventArgs
    {
        public readonly GrantActivationRequest Request;

        public PasswordlessLoginStartedEventArgs(GrantActivationRequest request)
        {
            Request = request;
        }
    }
}