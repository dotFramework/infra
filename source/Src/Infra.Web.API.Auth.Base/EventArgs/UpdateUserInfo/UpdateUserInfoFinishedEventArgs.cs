using DotFramework.Infra.Security.Model;
using System;

namespace DotFramework.Infra.Web.API.Auth.Base
{
    public class UpdateUserInfoFinishedEventArgs : EventArgs
    {
        public readonly UpdateUserInfoRequest Request;

        public UpdateUserInfoFinishedEventArgs(UpdateUserInfoRequest request)
        {
            Request = request;
        }
    }
}