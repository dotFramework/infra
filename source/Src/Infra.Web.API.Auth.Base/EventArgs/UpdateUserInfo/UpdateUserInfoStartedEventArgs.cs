using DotFramework.Infra.Security.Model;
using System;

namespace DotFramework.Infra.Web.API.Auth.Base
{
    public class UpdateUserInfoStartedEventArgs : EventArgs
    {
        public readonly UpdateUserInfoRequest Request;

        public UpdateUserInfoStartedEventArgs(UpdateUserInfoRequest request)
        {
            Request = request;
        }
    }
}