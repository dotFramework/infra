﻿using DotFramework.Core;
using DotFramework.Infra.Security.Model;
using DotFramework.Infra.Web.API.Auth.Base.Providers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
#if NETFRAMEWORK
using System.Web.Http.Results;
#else
using Microsoft.AspNetCore.Mvc;
#endif

namespace DotFramework.Infra.Web.API.Auth.Base
{
    public abstract class AbstractAuthenticationProvider<TAuthenticationProvider> : SingletonProvider<TAuthenticationProvider>, IAuthenticationProvider
        where TAuthenticationProvider : class, IAuthenticationProvider
    {
        #region Constructor

        protected AbstractAuthenticationProvider()
        {
            _AuthenticationService = AuthenticationServiceFactory.Instance.Resolve<AuthenticationService>();
        }

        #endregion

        #region Private Members

        protected IAuthenticationService _AuthenticationService;

        #endregion

        #region Private Members

        public ITokenProvider TokenProvider => _AuthenticationService.TokenProvider;

        #endregion

        #region Public Methods

        public void Initialize(IAuthenticationService authenticationService)
        {
            _AuthenticationService = authenticationService;
        }

        public async Task<AuthenticateClientResponse> AuthenticateClient(AuthenticateClientRequest request)
        {
            try
            {
                OnAuthenticateClientStarted(new AuthenticateClientStartedEventArgs(request));
                var response = await _AuthenticationService.AuthenticateClient(request);
                OnAuthenticateClientFinished(new AuthenticateClientFinishedEventArgs(request, response));

                return response;
            }
            catch (Exception ex)
            {
                OnLoginFailed(new ErrorEventArgs(ex));
                throw;
            }
        }

        public async Task<ObtainLocalAccessTokenResponse> InternalLogin(LoginRequest request)
        {
            try
            {
                OnInternalLoginStarted(new InternalLoginStartedEventArgs(request));
                var response = await _AuthenticationService.InternalLogin(request);
                OnInternalLoginFinished(new InternalLoginFinishedEventArgs(request, response));

                return response;
            }
            catch (Exception ex)
            {
                OnLoginFailed(new ErrorEventArgs(ex));
                throw;
            }
        }

        public async Task<ObtainLocalAccessTokenResponse> Login(LoginRequest request)
        {
            try
            {
                OnLoginStarted(new LoginStartedEventArgs(request));
                var response = await _AuthenticationService.Login(request);
                OnLoginFinished(new LoginFinishedEventArgs(request, response));

                return response;
            }
            catch (Exception ex)
            {
                OnLoginFailed(new ErrorEventArgs(ex));
                throw;
            }
        }

        public async Task<ObtainLocalAccessTokenResponse> Reauthenticate(ReauthenticateRequest request)
        {
            try
            {
                OnReauthenticateStarted(new ReauthenticateStartedEventArgs(request));
                var response = await _AuthenticationService.Reauthenticate(request);
                OnReauthenticateFinished(new ReauthenticateFinishedEventArgs(request, response));

                return response;
            }
            catch (Exception ex)
            {
                OnReauthenticateFailed(new ErrorEventArgs(ex));
                throw;
            }
        }

        public async Task<LogoutResponse> Logout()
        {
            try
            {
                OnLogoutStarted(new LogoutStartedEventArgs());
                var response = await _AuthenticationService.Logout();
                OnLogoutFinished(new LogoutFinishedEventArgs(response));

                return response;
            }
            catch (Exception ex)
            {
                OnLoginFailed(new ErrorEventArgs(ex));
                throw;
            }
        }

        public async Task<LogoutResponse> Logout(string accessToken)
        {
            try
            {
                OnLogoutStarted(new LogoutStartedEventArgs());
                var response = await _AuthenticationService.Logout(accessToken);
                OnLogoutFinished(new LogoutFinishedEventArgs(response));

                return response;
            }
            catch (Exception ex)
            {
                OnLoginFailed(new ErrorEventArgs(ex));
                throw;
            }
        }

        public async Task ForceLogout(string groupToken)
        {
            await _AuthenticationService.ForceLogout(groupToken);
        }

        public abstract RedirectResult GetExternalLogin(string provider, string redirect_url);

        public async Task<ObtainLocalAccessTokenResponse> ObtainLocalAccessToken(ObtainLocalAccessTokenRequest request)
        {
            try
            {
                OnObtainLocalAccessTokenStarted(new ObtainLocalAccessTokenStartedEventArgs(request));
                var response = await _AuthenticationService.ObtainLocalAccessToken(request);
                OnObtainLocalAccessTokenFinished(new ObtainLocalAccessTokenFinishedEventArgs(request, response));

                return response;
            }
            catch (Exception ex)
            {
                OnObtainLocalAccessTokenFailed(new ErrorEventArgs(ex));
                throw;
            }
        }

        public async Task<ObtainLocalAccessTokenResponse> RefreshToken(RefreshTokenRequest request)
        {
            try
            {
                OnRefreshTokenStarted(new RefreshTokenStartedEventArgs(request));
                var response = await _AuthenticationService.RefreshToken(request);
                OnRefreshTokenFinished(new RefreshTokenFinishedEventArgs(request, response));

                return response;
            }
            catch (Exception ex)
            {
                OnRefreshTokenFailed(new ErrorEventArgs(ex));
                throw;
            }
        }

        public async Task<AuthenticateResponseModel> Authenticate(AuthenticateRequest request)
        {
            try
            {
                OnAuthenticateStarted(new AuthenticateStartedEventArgs(request));
                var response = await _AuthenticationService.Authenticate(request);
                OnAuthenticateFinished(new AuthenticateFinishedEventArgs(request, response));

                return response;
            }
            catch (Exception ex)
            {
                OnAuthenticateFailed(new ErrorEventArgs(ex));
                throw;
            }
        }

        public async Task<GenerateClientEncryptionTokenResponse> GenerateClientEncryptionToken(GenerateClientEncryptionTokenRequest request)
        {
            try
            {
                OnGenerateClientEncryptionTokenStarted(new GenerateClientEncryptionTokenStartedEventArgs(request));
                var response = await _AuthenticationService.GenerateClientEncryptionToken(request);
                OnGenerateClientEncryptionTokenFinished(new GenerateClientEncryptionTokenFinishedEventArgs(request, response));

                return response;
            }
            catch (Exception ex)
            {
                OnAuthenticateFailed(new ErrorEventArgs(ex));
                throw;
            }
        }

        public async Task<ActivateClientResponse> ActivateClient(ActivateClientRequest request)
        {
            try
            {
                OnActivateClientStarted(new ActivateClientStartedEventArgs(request));
                var response = await _AuthenticationService.ActivateClient(request);
                OnActivateClientFinished(new ActivateClientFinishedEventArgs(request, response));

                return response;
            }
            catch (Exception ex)
            {
                OnActivateClientFailed(new ErrorEventArgs(ex));
                throw;
            }
        }

        public async Task<ActivateResponse> Activate(ActivateRequest request)
        {
            try
            {
                OnActivateStarted(new ActivateStartedEventArgs(request));
                var response = await _AuthenticationService.Activate(request);
                OnActivateFinished(new ActivateFinishedEventArgs(request, response));

                return response;
            }
            catch (Exception ex)
            {
                OnActivateClientFailed(new ErrorEventArgs(ex));
                throw;
            }
        }

        public async Task<ActivateLocalAccessTokenResponse> GrantActivation(GrantActivationRequest request)
        {
            try
            {
                OnGrantActivationStarted(new GrantActivationStartedEventArgs(request));
                var response = await _AuthenticationService.GrantActivation(request);
                OnGrantActivationFinished(new GrantActivationFinishedEventArgs(request, response));

                return response;
            }
            catch (Exception ex)
            {
                OnActivateClientFailed(new ErrorEventArgs(ex));
                throw;
            }
        }

        public async Task<ObtainLocalAccessTokenResponse> PasswordlessLogin(GrantActivationRequest request)
        {
            try
            {
                OnPasswordlessLoginStarted(new PasswordlessLoginStartedEventArgs(request));
                var response = await _AuthenticationService.PasswordlessLogin(request);
                OnPasswordlessLoginFinished(new PasswordlessLoginFinishedEventArgs(request, response));

                return response;
            }
            catch (Exception ex)
            {
                OnActivateClientFailed(new ErrorEventArgs(ex));
                throw;
            }
        }

        public async Task<ActivateLocalAccessTokenResponse> GrantInternalActivation(GrantInternalActivationRequest request)
        {
            try
            {
                OnGrantInternalActivationStarted(new GrantInternalActivationStartedEventArgs(request));
                var response = await _AuthenticationService.GrantInternalActivation(request);
                OnGrantInternalActivationFinished(new GrantInternalActivationFinishedEventArgs(request, response));

                return response;
            }
            catch (Exception ex)
            {
                OnActivateClientFailed(new ErrorEventArgs(ex));
                throw;
            }
        }

        public async Task<VerifyClientTokenResponseModel> VerifyClientToken(VerifyTokenRequest request)
        {
            try
            {
                OnVerifyClientTokenStarted(new VerifyClientTokenStartedEventArgs(request));
                var response = await _AuthenticationService.VerifyClientToken(request);
                OnVerifyClientTokenFinished(new VerifyClientTokenFinishedEventArgs(request, response));

                return response;
            }
            catch (Exception ex)
            {
                OnVerifyClientTokenFailed(new ErrorEventArgs(ex));
                throw;
            }
        }

        public async Task<UserDataResponseModel> Authorize(AuthorizeRequest request)
        {
            try
            {
                OnAuthorizeStarted(new AuthorizeStartedEventArgs(request));
                var response = await _AuthenticationService.Authorize(request);
                OnAuthorizeFinished(new AuthorizeFinishedEventArgs(request, response));

                return response;
            }
            catch (Exception ex)
            {
                OnAuthorizeFailed(new ErrorEventArgs(ex));
                throw;
            }
        }

        public UserDataResponseModel GetUserData()
        {
            try
            {
                OnGetUserDataStarted(new GetUserDataStartedEventArgs());
                var result = _AuthenticationService.GetUserData();
                OnGetUserDataFinished(new GetUserDataFinishedEventArgs(result));

                return result;
            }
            catch (Exception ex)
            {
                OnGetUserDataFailed(new ErrorEventArgs(ex));
                throw;
            }
        }

        public ClientDataResponseModel GetClientData()
        {
            try
            {
                OnGetClientDataStarted(new GetClientDataStartedEventArgs());
                var result = _AuthenticationService.GetClientData();
                OnGetClientDataFinished(new GetClientDataFinishedEventArgs(result));

                return result;
            }
            catch (Exception ex)
            {
                OnGetClientDataFailed(new ErrorEventArgs(ex));
                throw;
            }
        }

        public async Task<AuthenticateResponseModel> ValidateToken()
        {
            try
            {
                OnValidateTokenStarted(new ValidateTokenStartedEventArgs());
                var result = await _AuthenticationService.ValidateToken();
                OnValidateTokenFinished(new ValidateTokenFinishedEventArgs(result));

                return result;
            }
            catch (Exception ex)
            {
                OnGetUserDataFailed(new ErrorEventArgs(ex));
                throw;
            }
        }

        public async Task InternalRegisterWithoutAuthentication(RegisterRequest request)
        {
            try
            {
                OnInternalRegisterWithoutAuthenticationStarted(new InternalRegisterWithoutAuthenticationStartedEventArgs(request));
                await _AuthenticationService.InternalRegisterWithoutAuthentication(request);
                OnInternalRegisterWithoutAuthenticationFinished(new InternalRegisterWithoutAuthenticationFinishedEventArgs(request));
            }
            catch (Exception ex)
            {
                OnInternalRegisterWithoutAuthenticationFailed(new ErrorEventArgs(ex));
                throw;
            }
        }

        public async Task<ObtainLocalAccessTokenResponse> InternalRegister(RegisterRequest request)
        {
            try
            {
                OnInternalRegisterStarted(new InternalRegisterStartedEventArgs(request));
                var response = await _AuthenticationService.InternalRegister(request);
                OnInternalRegisterFinished(new InternalRegisterFinishedEventArgs(request, response));

                return response;
            }
            catch (Exception ex)
            {
                OnInternalRegisterFailed(new ErrorEventArgs(ex));
                throw;
            }
        }

        public async Task<ObtainLocalAccessTokenResponse> Register(RegisterRequest request)
        {
            try
            {
                OnRegisterStarted(new RegisterStartedEventArgs(request));
                var response = await _AuthenticationService.Register(request);
                OnRegisterFinished(new RegisterFinishedEventArgs(request, response));

                return response;
            }
            catch (Exception ex)
            {
                OnRegisterFailed(new ErrorEventArgs(ex));
                throw;
            }
        }

        public async Task<ChangePasswordResponse> ChangePassword(ChangePasswordRequest request)
        {
            try
            {
                //OnChangePasswordStarted(new ChangePasswordStartedEventArgs(request));
                var response = await _AuthenticationService.ChangePassword(request);
                //OnChangePasswordFinished(new ChangePasswordFinishedEventArgs(request, response));

                return response;
            }
            catch (Exception ex)
            {
                OnChangePasswordFailed(new ErrorEventArgs(ex));
                throw;
            }
        }

        public async Task UpdateUserInfo(UpdateUserInfoRequest request)
        {
            try
            {
                OnUpdateUserInfoStarted(new UpdateUserInfoStartedEventArgs(request));
                await _AuthenticationService.UpdateUserInfo(request);
                OnUpdateUserInfoFinished(new UpdateUserInfoFinishedEventArgs(request));
            }
            catch (Exception ex)
            {
                OnChangePasswordFailed(new ErrorEventArgs(ex));
                throw;
            }
        }

        public async Task<ForgetPasswordResponse> InternalForgetPassword(ForgetPasswordRequest request)
        {
            try
            {
                OnInternalForgetPasswordStarted(new InternalForgetPasswordStartedEventArgs(request));
                var response = await _AuthenticationService.InternalForgetPassword(request);
                OnInternalForgetPasswordFinished(new InternalForgetPasswordFinishedEventArgs(request, response));

                return response;
            }
            catch (Exception ex)
            {
                OnInternalForgetPasswordFailed(new ErrorEventArgs(ex));
                throw;
            }
        }

        public async Task<ForgetPasswordResponse> ForgetPassword(ForgetPasswordRequest request)
        {
            try
            {
                OnForgetPasswordStarted(new ForgetPasswordStartedEventArgs(request));
                var response = await _AuthenticationService.ForgetPassword(request);
                OnForgetPasswordFinished(new ForgetPasswordFinishedEventArgs(request, response));

                return response;
            }
            catch (Exception ex)
            {
                OnForgetPasswordFailed(new ErrorEventArgs(ex));
                throw;
            }
        }

        public async Task<ResetPasswordResponse> ResetPassword(ResetPasswordRequest request)
        {
            try
            {
                OnResetPasswordStarted(new ResetPasswordStartedEventArgs(request));
                var response = await _AuthenticationService.ResetPassword(request);
                OnResetPasswordFinished(new ResetPasswordFinishedEventArgs(request, response));

                return response;
            }
            catch (Exception ex)
            {
                OnResetPasswordFailed(new ErrorEventArgs(ex));
                throw;
            }
        }

        public async Task<CreateClientResponse> CreateClient(CreateClientRequest request)
        {
            try
            {
                OnCreateClientStarted(new CreateClientStartedEventArgs(request));
                var response = await _AuthenticationService.CreateClient(request);
                OnCreateClientFinished(new CreateClientFinishedEventArgs(request, response));

                return response;
            }
            catch (Exception ex)
            {
                OnCreateClientFailed(new ErrorEventArgs(ex));
                throw;
            }
        }

        public async Task<GetClientResponse> GetClient(GetClientRequest request)
        {
            try
            {
                OnGetClientStarted(new GetClientStartedEventArgs(request));
                var response = await _AuthenticationService.GetClient(request);
                OnGetClientFinished(new GetClientFinishedEventArgs(request, response));

                return response;
            }
            catch (Exception ex)
            {
                OnGetClientFailed(new ErrorEventArgs(ex));
                throw;
            }
        }

        public async Task ConfirmEmail(ConfirmEmailBindingModel request)
        {
            try
            {
                OnConfirmEmailStarted(new ConfirmEmailStartedEventArgs(request));
                await _AuthenticationService.ConfirmEmail(request);
                OnConfirmEmailFinished(new ConfirmEmailFinishedEventArgs(request));
            }
            catch (Exception ex)
            {
                OnConfirmEmailFailed(new ErrorEventArgs(ex));
                throw;
            }
        }

        public async Task<RedirectResult> ConfirmEmailCallback(ConfirmEmailCallbackBindingModel request)
        {
            try
            {
                OnConfirmEmailCallbackStarted(new ConfirmEmailCallbackStartedEventArgs(request));
                var response = await _AuthenticationService.ConfirmEmailCallback(request);
                OnConfirmEmailCallbackFinished(new ConfirmEmailCallbackFinishedEventArgs(request, response));

                return response;
            }
            catch (Exception ex)
            {
                OnConfirmEmailCallbackFailed(new ErrorEventArgs(ex));
                throw;
            }
        }

        #endregion

        #region Events

        #region AuthenticateClient

        public event EventHandler<AuthenticateClientStartedEventArgs> AuthenticateClientStarted;

        protected void OnAuthenticateClientStarted(AuthenticateClientStartedEventArgs e)
        {
            AuthenticateClientStarted?.Invoke(this, e);
        }

        public event EventHandler<AuthenticateClientFinishedEventArgs> AuthenticateClientFinished;

        protected void OnAuthenticateClientFinished(AuthenticateClientFinishedEventArgs e)
        {
            AuthenticateClientFinished?.Invoke(this, e);
        }

        public event EventHandler<ErrorEventArgs> AuthenticateClientFailed;

        protected void OnAuthenticateClientFailed(ErrorEventArgs e)
        {
            AuthenticateClientFailed?.Invoke(this, e);
        }

        #endregion

        #region InternalLogin

        public event EventHandler<InternalLoginStartedEventArgs> InternalLoginStarted;

        protected void OnInternalLoginStarted(InternalLoginStartedEventArgs e)
        {
            InternalLoginStarted?.Invoke(this, e);
        }

        public event EventHandler<InternalLoginFinishedEventArgs> InternalLoginFinished;

        protected void OnInternalLoginFinished(InternalLoginFinishedEventArgs e)
        {
            InternalLoginFinished?.Invoke(this, e);
        }

        public event EventHandler<ErrorEventArgs> InternalLoginFailed;

        protected void OnInternalLoginFailed(ErrorEventArgs e)
        {
            InternalLoginFailed?.Invoke(this, e);
        }

        #endregion

        #region Login

        public event EventHandler<LoginStartedEventArgs> LoginStarted;

        protected void OnLoginStarted(LoginStartedEventArgs e)
        {
            LoginStarted?.Invoke(this, e);
        }

        public event EventHandler<LoginFinishedEventArgs> LoginFinished;

        protected void OnLoginFinished(LoginFinishedEventArgs e)
        {
            LoginFinished?.Invoke(this, e);
        }

        public event EventHandler<ErrorEventArgs> LoginFailed;

        protected void OnLoginFailed(ErrorEventArgs e)
        {
            LoginFailed?.Invoke(this, e);
        }

        #endregion

        #region Reauthenticate

        public event EventHandler<ReauthenticateStartedEventArgs> ReauthenticateStarted;

        protected void OnReauthenticateStarted(ReauthenticateStartedEventArgs e)
        {
            ReauthenticateStarted?.Invoke(this, e);
        }

        public event EventHandler<ReauthenticateFinishedEventArgs> ReauthenticateFinished;

        protected void OnReauthenticateFinished(ReauthenticateFinishedEventArgs e)
        {
            ReauthenticateFinished?.Invoke(this, e);
        }

        public event EventHandler<ErrorEventArgs> ReauthenticateFailed;

        protected void OnReauthenticateFailed(ErrorEventArgs e)
        {
            ReauthenticateFailed?.Invoke(this, e);
        }

        #endregion

        #region Logout

        public event EventHandler<LogoutStartedEventArgs> LogoutStarted;

        protected void OnLogoutStarted(LogoutStartedEventArgs e)
        {
            LogoutStarted?.Invoke(this, e);
        }

        public event EventHandler<LogoutFinishedEventArgs> LogoutFinished;

        protected void OnLogoutFinished(LogoutFinishedEventArgs e)
        {
            LogoutFinished?.Invoke(this, e);
        }

        public event EventHandler<ErrorEventArgs> LogoutFailed;

        protected void OnLogoutFailed(ErrorEventArgs e)
        {
            LogoutFailed?.Invoke(this, e);
        }

        #endregion

        #region GetExternalLogin

        public event EventHandler<GetExternalLoginStartedEventArgs> GetExternalLoginStarted;

        protected void OnGetExternalLoginStarted(GetExternalLoginStartedEventArgs e)
        {
            GetExternalLoginStarted?.Invoke(this, e);
        }

        public event EventHandler<GetExternalLoginFinishedEventArgs> GetExternalLoginFinished;

        protected void OnGetExternalLoginFinished(GetExternalLoginFinishedEventArgs e)
        {
            GetExternalLoginFinished?.Invoke(this, e);
        }

        public event EventHandler<ErrorEventArgs> GetExternalLoginFailed;

        protected void OnGetExternalLoginFailed(ErrorEventArgs e)
        {
            GetExternalLoginFailed?.Invoke(this, e);
        }

        #endregion

        #region ObtainLocalAccessToken

        public event EventHandler<ObtainLocalAccessTokenStartedEventArgs> ObtainLocalAccessTokenStarted;

        protected void OnObtainLocalAccessTokenStarted(ObtainLocalAccessTokenStartedEventArgs e)
        {
            ObtainLocalAccessTokenStarted?.Invoke(this, e);
        }

        public event EventHandler<ObtainLocalAccessTokenFinishedEventArgs> ObtainLocalAccessTokenFinished;

        protected void OnObtainLocalAccessTokenFinished(ObtainLocalAccessTokenFinishedEventArgs e)
        {
            ObtainLocalAccessTokenFinished?.Invoke(this, e);
        }

        public event EventHandler<ErrorEventArgs> ObtainLocalAccessTokenFailed;

        protected void OnObtainLocalAccessTokenFailed(ErrorEventArgs e)
        {
            ObtainLocalAccessTokenFailed?.Invoke(this, e);
        }

        #endregion

        #region RefreshToken

        public event EventHandler<RefreshTokenStartedEventArgs> RefreshTokenStarted;

        protected void OnRefreshTokenStarted(RefreshTokenStartedEventArgs e)
        {
            RefreshTokenStarted?.Invoke(this, e);
        }

        public event EventHandler<RefreshTokenFinishedEventArgs> RefreshTokenFinished;

        protected void OnRefreshTokenFinished(RefreshTokenFinishedEventArgs e)
        {
            RefreshTokenFinished?.Invoke(this, e);
        }

        public event EventHandler<ErrorEventArgs> RefreshTokenFailed;

        protected void OnRefreshTokenFailed(ErrorEventArgs e)
        {
            RefreshTokenFailed?.Invoke(this, e);
        }

        #endregion

        #region Authenticate

        public event EventHandler<AuthenticateStartedEventArgs> AuthenticateStarted;

        protected void OnAuthenticateStarted(AuthenticateStartedEventArgs e)
        {
            AuthenticateStarted?.Invoke(this, e);
        }

        public event EventHandler<AuthenticateFinishedEventArgs> AuthenticateFinished;

        protected void OnAuthenticateFinished(AuthenticateFinishedEventArgs e)
        {
            AuthenticateFinished?.Invoke(this, e);
        }

        public event EventHandler<ErrorEventArgs> AuthenticateFailed;

        protected void OnAuthenticateFailed(ErrorEventArgs e)
        {
            AuthenticateFailed?.Invoke(this, e);
        }

        #endregion

        #region GenerateClientEncryptionToken

        public event EventHandler<GenerateClientEncryptionTokenStartedEventArgs> GenerateClientEncryptionTokenStarted;

        protected void OnGenerateClientEncryptionTokenStarted(GenerateClientEncryptionTokenStartedEventArgs e)
        {
            GenerateClientEncryptionTokenStarted?.Invoke(this, e);
        }

        public event EventHandler<GenerateClientEncryptionTokenFinishedEventArgs> GenerateClientEncryptionTokenFinished;

        protected void OnGenerateClientEncryptionTokenFinished(GenerateClientEncryptionTokenFinishedEventArgs e)
        {
            GenerateClientEncryptionTokenFinished?.Invoke(this, e);
        }

        public event EventHandler<ErrorEventArgs> GenerateClientEncryptionTokenFailed;

        protected void OnGenerateClientEncryptionTokenFailed(ErrorEventArgs e)
        {
            GenerateClientEncryptionTokenFailed?.Invoke(this, e);
        }

        #endregion

        #region ActivateClient

        public event EventHandler<ActivateClientStartedEventArgs> ActivateClientStarted;

        protected void OnActivateClientStarted(ActivateClientStartedEventArgs e)
        {
            ActivateClientStarted?.Invoke(this, e);
        }

        public event EventHandler<ActivateClientFinishedEventArgs> ActivateClientFinished;

        protected void OnActivateClientFinished(ActivateClientFinishedEventArgs e)
        {
            ActivateClientFinished?.Invoke(this, e);
        }

        public event EventHandler<ErrorEventArgs> ActivateClientFailed;

        protected void OnActivateClientFailed(ErrorEventArgs e)
        {
            ActivateClientFailed?.Invoke(this, e);
        }

        #endregion

        #region Activate

        public event EventHandler<ActivateStartedEventArgs> ActivateStarted;

        protected void OnActivateStarted(ActivateStartedEventArgs e)
        {
            ActivateStarted?.Invoke(this, e);
        }

        public event EventHandler<ActivateFinishedEventArgs> ActivateFinished;

        protected void OnActivateFinished(ActivateFinishedEventArgs e)
        {
            ActivateFinished?.Invoke(this, e);
        }

        public event EventHandler<ErrorEventArgs> ActivateFailed;

        protected void OnActivateFailed(ErrorEventArgs e)
        {
            ActivateFailed?.Invoke(this, e);
        }

        #endregion

        #region GrantActivation

        public event EventHandler<GrantActivationStartedEventArgs> GrantActivationStarted;

        protected void OnGrantActivationStarted(GrantActivationStartedEventArgs e)
        {
            GrantActivationStarted?.Invoke(this, e);
        }

        public event EventHandler<GrantActivationFinishedEventArgs> GrantActivationFinished;

        protected void OnGrantActivationFinished(GrantActivationFinishedEventArgs e)
        {
            GrantActivationFinished?.Invoke(this, e);
        }

        public event EventHandler<ErrorEventArgs> GrantActivationFailed;

        protected void OnGrantActivationFailed(ErrorEventArgs e)
        {
            GrantActivationFailed?.Invoke(this, e);
        }

        #endregion

        #region PasswordlessLogin

        public event EventHandler<PasswordlessLoginStartedEventArgs> PasswordlessLoginStarted;

        protected void OnPasswordlessLoginStarted(PasswordlessLoginStartedEventArgs e)
        {
            PasswordlessLoginStarted?.Invoke(this, e);
        }

        public event EventHandler<PasswordlessLoginFinishedEventArgs> PasswordlessLoginFinished;

        protected void OnPasswordlessLoginFinished(PasswordlessLoginFinishedEventArgs e)
        {
            PasswordlessLoginFinished?.Invoke(this, e);
        }

        public event EventHandler<ErrorEventArgs> PasswordlessLoginFailed;

        protected void OnPasswordlessLoginFailed(ErrorEventArgs e)
        {
            PasswordlessLoginFailed?.Invoke(this, e);
        }

        #endregion

        #region GrantInternalActivation

        public event EventHandler<GrantInternalActivationStartedEventArgs> GrantInternalActivationStarted;

        protected void OnGrantInternalActivationStarted(GrantInternalActivationStartedEventArgs e)
        {
            GrantInternalActivationStarted?.Invoke(this, e);
        }

        public event EventHandler<GrantInternalActivationFinishedEventArgs> GrantInternalActivationFinished;

        protected void OnGrantInternalActivationFinished(GrantInternalActivationFinishedEventArgs e)
        {
            GrantInternalActivationFinished?.Invoke(this, e);
        }

        public event EventHandler<ErrorEventArgs> GrantInternalActivationFailed;

        protected void OnGrantInternalActivationFailed(ErrorEventArgs e)
        {
            GrantInternalActivationFailed?.Invoke(this, e);
        }

        #endregion

        #region VerifyClientToken

        public event EventHandler<VerifyClientTokenStartedEventArgs> VerifyClientTokenStarted;

        protected void OnVerifyClientTokenStarted(VerifyClientTokenStartedEventArgs e)
        {
            VerifyClientTokenStarted?.Invoke(this, e);
        }

        public event EventHandler<VerifyClientTokenFinishedEventArgs> VerifyClientTokenFinished;

        protected void OnVerifyClientTokenFinished(VerifyClientTokenFinishedEventArgs e)
        {
            VerifyClientTokenFinished?.Invoke(this, e);
        }

        public event EventHandler<ErrorEventArgs> VerifyClientTokenFailed;

        protected void OnVerifyClientTokenFailed(ErrorEventArgs e)
        {
            VerifyClientTokenFailed?.Invoke(this, e);
        }

        #endregion

        #region Authorize

        public event EventHandler<AuthorizeStartedEventArgs> AuthorizeStarted;

        protected void OnAuthorizeStarted(AuthorizeStartedEventArgs e)
        {
            AuthorizeStarted?.Invoke(this, e);
        }

        public event EventHandler<AuthorizeFinishedEventArgs> AuthorizeFinished;

        protected void OnAuthorizeFinished(AuthorizeFinishedEventArgs e)
        {
            AuthorizeFinished?.Invoke(this, e);
        }

        public event EventHandler<ErrorEventArgs> AuthorizeFailed;

        protected void OnAuthorizeFailed(ErrorEventArgs e)
        {
            AuthorizeFailed?.Invoke(this, e);
        }

        #endregion

        #region GetUserData

        public event EventHandler<GetUserDataStartedEventArgs> GetUserDataStarted;

        protected void OnGetUserDataStarted(GetUserDataStartedEventArgs e)
        {
            GetUserDataStarted?.Invoke(this, e);
        }

        public event EventHandler<GetUserDataFinishedEventArgs> GetUserDataFinished;

        protected void OnGetUserDataFinished(GetUserDataFinishedEventArgs e)
        {
            GetUserDataFinished?.Invoke(this, e);
        }

        public event EventHandler<ErrorEventArgs> GetUserDataFailed;

        protected void OnGetUserDataFailed(ErrorEventArgs e)
        {
            GetUserDataFailed?.Invoke(this, e);
        }

        #endregion

        #region GetClientData

        public event EventHandler<GetClientDataStartedEventArgs> GetClientDataStarted;

        protected void OnGetClientDataStarted(GetClientDataStartedEventArgs e)
        {
            GetClientDataStarted?.Invoke(this, e);
        }

        public event EventHandler<GetClientDataFinishedEventArgs> GetClientDataFinished;

        protected void OnGetClientDataFinished(GetClientDataFinishedEventArgs e)
        {
            GetClientDataFinished?.Invoke(this, e);
        }

        public event EventHandler<ErrorEventArgs> GetClientDataFailed;

        protected void OnGetClientDataFailed(ErrorEventArgs e)
        {
            GetClientDataFailed?.Invoke(this, e);
        }

        #endregion

        #region ValidateToken

        public event EventHandler<ValidateTokenStartedEventArgs> ValidateTokenStarted;

        protected void OnValidateTokenStarted(ValidateTokenStartedEventArgs e)
        {
            ValidateTokenStarted?.Invoke(this, e);
        }

        public event EventHandler<ValidateTokenFinishedEventArgs> ValidateTokenFinished;

        protected void OnValidateTokenFinished(ValidateTokenFinishedEventArgs e)
        {
            ValidateTokenFinished?.Invoke(this, e);
        }

        public event EventHandler<ErrorEventArgs> ValidateTokenFailed;

        protected void OnValidateTokenFailed(ErrorEventArgs e)
        {
            ValidateTokenFailed?.Invoke(this, e);
        }

        #endregion

        #region InternalRegisterWithoutAuthentication

        public event EventHandler<InternalRegisterWithoutAuthenticationStartedEventArgs> InternalRegisterWithoutAuthenticationStarted;

        protected void OnInternalRegisterWithoutAuthenticationStarted(InternalRegisterWithoutAuthenticationStartedEventArgs e)
        {
            InternalRegisterWithoutAuthenticationStarted?.Invoke(this, e);
        }

        public event EventHandler<InternalRegisterWithoutAuthenticationFinishedEventArgs> InternalRegisterWithoutAuthenticationFinished;

        protected void OnInternalRegisterWithoutAuthenticationFinished(InternalRegisterWithoutAuthenticationFinishedEventArgs e)
        {
            InternalRegisterWithoutAuthenticationFinished?.Invoke(this, e);
        }

        public event EventHandler<ErrorEventArgs> InternalRegisterWithoutAuthenticationFailed;

        protected void OnInternalRegisterWithoutAuthenticationFailed(ErrorEventArgs e)
        {
            InternalRegisterWithoutAuthenticationFailed?.Invoke(this, e);
        }

        #endregion

        #region InternalRegister

        public event EventHandler<InternalRegisterStartedEventArgs> InternalRegisterStarted;

        protected void OnInternalRegisterStarted(InternalRegisterStartedEventArgs e)
        {
            InternalRegisterStarted?.Invoke(this, e);
        }

        public event EventHandler<InternalRegisterFinishedEventArgs> InternalRegisterFinished;

        protected void OnInternalRegisterFinished(InternalRegisterFinishedEventArgs e)
        {
            InternalRegisterFinished?.Invoke(this, e);
        }

        public event EventHandler<ErrorEventArgs> InternalRegisterFailed;

        protected void OnInternalRegisterFailed(ErrorEventArgs e)
        {
            InternalRegisterFailed?.Invoke(this, e);
        }

        #endregion

        #region Register

        public event EventHandler<RegisterStartedEventArgs> RegisterStarted;

        protected void OnRegisterStarted(RegisterStartedEventArgs e)
        {
            RegisterStarted?.Invoke(this, e);
        }

        public event EventHandler<RegisterFinishedEventArgs> RegisterFinished;

        protected void OnRegisterFinished(RegisterFinishedEventArgs e)
        {
            RegisterFinished?.Invoke(this, e);
        }

        public event EventHandler<ErrorEventArgs> RegisterFailed;

        protected void OnRegisterFailed(ErrorEventArgs e)
        {
            RegisterFailed?.Invoke(this, e);
        }

        #endregion

        #region ChangePassword

        public event EventHandler<ChangePasswordStartedEventArgs> ChangePasswordStarted;

        protected void OnChangePasswordStarted(ChangePasswordStartedEventArgs e)
        {
            ChangePasswordStarted?.Invoke(this, e);
        }

        public event EventHandler<ChangePasswordFinishedEventArgs> ChangePasswordFinished;

        protected void OnChangePasswordFinished(ChangePasswordFinishedEventArgs e)
        {
            ChangePasswordFinished?.Invoke(this, e);
        }

        public event EventHandler<ErrorEventArgs> ChangePasswordFailed;

        protected void OnChangePasswordFailed(ErrorEventArgs e)
        {
            ChangePasswordFailed?.Invoke(this, e);
        }

        #endregion

        #region UpdateUserInfo

        public event EventHandler<UpdateUserInfoStartedEventArgs> UpdateUserInfoStarted;

        protected void OnUpdateUserInfoStarted(UpdateUserInfoStartedEventArgs e)
        {
            UpdateUserInfoStarted?.Invoke(this, e);
        }

        public event EventHandler<UpdateUserInfoFinishedEventArgs> UpdateUserInfoFinished;

        protected void OnUpdateUserInfoFinished(UpdateUserInfoFinishedEventArgs e)
        {
            UpdateUserInfoFinished?.Invoke(this, e);
        }

        public event EventHandler<ErrorEventArgs> UpdateUserInfoFailed;

        protected void OnUpdateUserInfoFailed(ErrorEventArgs e)
        {
            UpdateUserInfoFailed?.Invoke(this, e);
        }

        #endregion

        #region InternalForgetPassword

        public event EventHandler<InternalForgetPasswordStartedEventArgs> InternalForgetPasswordStarted;

        protected void OnInternalForgetPasswordStarted(InternalForgetPasswordStartedEventArgs e)
        {
            InternalForgetPasswordStarted?.Invoke(this, e);
        }

        public event EventHandler<InternalForgetPasswordFinishedEventArgs> InternalForgetPasswordFinished;

        protected void OnInternalForgetPasswordFinished(InternalForgetPasswordFinishedEventArgs e)
        {
            InternalForgetPasswordFinished?.Invoke(this, e);
        }

        public event EventHandler<ErrorEventArgs> InternalForgetPasswordFailed;

        protected void OnInternalForgetPasswordFailed(ErrorEventArgs e)
        {
            InternalForgetPasswordFailed?.Invoke(this, e);
        }

        #endregion

        #region ForgetPassword

        public event EventHandler<ForgetPasswordStartedEventArgs> ForgetPasswordStarted;

        protected void OnForgetPasswordStarted(ForgetPasswordStartedEventArgs e)
        {
            ForgetPasswordStarted?.Invoke(this, e);
        }

        public event EventHandler<ForgetPasswordFinishedEventArgs> ForgetPasswordFinished;

        protected void OnForgetPasswordFinished(ForgetPasswordFinishedEventArgs e)
        {
            ForgetPasswordFinished?.Invoke(this, e);
        }

        public event EventHandler<ErrorEventArgs> ForgetPasswordFailed;

        protected void OnForgetPasswordFailed(ErrorEventArgs e)
        {
            ForgetPasswordFailed?.Invoke(this, e);
        }

        #endregion

        #region ResetPassword

        public event EventHandler<ResetPasswordStartedEventArgs> ResetPasswordStarted;

        protected void OnResetPasswordStarted(ResetPasswordStartedEventArgs e)
        {
            ResetPasswordStarted?.Invoke(this, e);
        }

        public event EventHandler<ResetPasswordFinishedEventArgs> ResetPasswordFinished;

        protected void OnResetPasswordFinished(ResetPasswordFinishedEventArgs e)
        {
            ResetPasswordFinished?.Invoke(this, e);
        }

        public event EventHandler<ErrorEventArgs> ResetPasswordFailed;

        protected void OnResetPasswordFailed(ErrorEventArgs e)
        {
            ResetPasswordFailed?.Invoke(this, e);
        }

        #endregion

        #region CreateClient

        public event EventHandler<CreateClientStartedEventArgs> CreateClientStarted;

        protected void OnCreateClientStarted(CreateClientStartedEventArgs e)
        {
            CreateClientStarted?.Invoke(this, e);
        }

        public event EventHandler<CreateClientFinishedEventArgs> CreateClientFinished;

        protected void OnCreateClientFinished(CreateClientFinishedEventArgs e)
        {
            CreateClientFinished?.Invoke(this, e);
        }

        public event EventHandler<ErrorEventArgs> CreateClientFailed;

        protected void OnCreateClientFailed(ErrorEventArgs e)
        {
            CreateClientFailed?.Invoke(this, e);
        }

        #endregion

        #region GetClient

        public event EventHandler<GetClientStartedEventArgs> GetClientStarted;

        protected void OnGetClientStarted(GetClientStartedEventArgs e)
        {
            GetClientStarted?.Invoke(this, e);
        }

        public event EventHandler<GetClientFinishedEventArgs> GetClientFinished;

        protected void OnGetClientFinished(GetClientFinishedEventArgs e)
        {
            GetClientFinished?.Invoke(this, e);
        }

        public event EventHandler<ErrorEventArgs> GetClientFailed;

        protected void OnGetClientFailed(ErrorEventArgs e)
        {
            GetClientFailed?.Invoke(this, e);
        }

        #endregion

        #region ConfirmEmail

        public event EventHandler<ConfirmEmailStartedEventArgs> ConfirmEmailStarted;

        protected void OnConfirmEmailStarted(ConfirmEmailStartedEventArgs e)
        {
            ConfirmEmailStarted?.Invoke(this, e);
        }

        public event EventHandler<ConfirmEmailFinishedEventArgs> ConfirmEmailFinished;

        protected void OnConfirmEmailFinished(ConfirmEmailFinishedEventArgs e)
        {
            ConfirmEmailFinished?.Invoke(this, e);
        }

        public event EventHandler<ErrorEventArgs> ConfirmEmailFailed;

        protected void OnConfirmEmailFailed(ErrorEventArgs e)
        {
            ConfirmEmailFailed?.Invoke(this, e);
        }

        #endregion

        #region ConfirmEmailCallback

        public event EventHandler<ConfirmEmailCallbackStartedEventArgs> ConfirmEmailCallbackStarted;

        protected void OnConfirmEmailCallbackStarted(ConfirmEmailCallbackStartedEventArgs e)
        {
            ConfirmEmailCallbackStarted?.Invoke(this, e);
        }

        public event EventHandler<ConfirmEmailCallbackFinishedEventArgs> ConfirmEmailCallbackFinished;

        protected void OnConfirmEmailCallbackFinished(ConfirmEmailCallbackFinishedEventArgs e)
        {
            ConfirmEmailCallbackFinished?.Invoke(this, e);
        }

        public event EventHandler<ErrorEventArgs> ConfirmEmailCallbackFailed;

        protected void OnConfirmEmailCallbackFailed(ErrorEventArgs e)
        {
            ConfirmEmailCallbackFailed?.Invoke(this, e);
        }

        #endregion

        #endregion
    }
}
