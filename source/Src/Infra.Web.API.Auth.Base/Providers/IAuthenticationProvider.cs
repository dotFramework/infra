﻿using DotFramework.Infra.Security.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
#if NETFRAMEWORK
using System.Web.Http.Results;
#else
using Microsoft.AspNetCore.Mvc;
#endif

namespace DotFramework.Infra.Web.API.Auth.Base
{
    public interface IAuthenticationProvider
    {
        void Initialize(IAuthenticationService authenticationService);

        Task<AuthenticateClientResponse> AuthenticateClient(AuthenticateClientRequest request);

        Task<ObtainLocalAccessTokenResponse> Login(LoginRequest request);

        Task<ObtainLocalAccessTokenResponse> Reauthenticate(ReauthenticateRequest request);

        Task<LogoutResponse> Logout();
        
        Task<LogoutResponse> Logout(string accessToken);
        Task ForceLogout(string groupToken);

        RedirectResult GetExternalLogin(string provider, string redirect_url);

        Task<ObtainLocalAccessTokenResponse> ObtainLocalAccessToken(ObtainLocalAccessTokenRequest request);

        Task<ObtainLocalAccessTokenResponse> RefreshToken(RefreshTokenRequest request);

        Task<AuthenticateResponseModel> Authenticate(AuthenticateRequest request);

        Task<GenerateClientEncryptionTokenResponse> GenerateClientEncryptionToken(GenerateClientEncryptionTokenRequest request);

        Task<ActivateClientResponse> ActivateClient(ActivateClientRequest request);

        Task<ActivateResponse> Activate(ActivateRequest request);

        Task<ActivateLocalAccessTokenResponse> GrantActivation(GrantActivationRequest request);

        Task<ActivateLocalAccessTokenResponse> GrantInternalActivation(GrantInternalActivationRequest request);

        Task<VerifyClientTokenResponseModel> VerifyClientToken(VerifyTokenRequest request);

        Task<UserDataResponseModel> Authorize(AuthorizeRequest request);

        UserDataResponseModel GetUserData();

        ClientDataResponseModel GetClientData();

        Task<AuthenticateResponseModel> ValidateToken();

        Task<ObtainLocalAccessTokenResponse> Register(RegisterRequest request);

        Task<ChangePasswordResponse> ChangePassword(ChangePasswordRequest request);

        Task<ForgetPasswordResponse> ForgetPassword(ForgetPasswordRequest request);

        Task<ResetPasswordResponse> ResetPassword(ResetPasswordRequest request);
    }
}
