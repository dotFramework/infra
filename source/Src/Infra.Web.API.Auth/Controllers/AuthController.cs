using DotFramework.Core;
using DotFramework.Core.Web;
using DotFramework.Infra.Security.Model;
using DotFramework.Web.API;
using System;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace DotFramework.Infra.Web.API.Auth.Controllers
{
    [RoutePrefix("Auth")]
    public class AuthController : ApiController
    {
        [HttpPost]
        [Route("AuthenticateClient")]
        public async Task<IHttpActionResult> AuthenticateClient(AuthenticateClientRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                var response = await AuthenticationProvider.Instance.AuthenticateClient(request);
                return Ok(response);
            }
            catch (Exception ex)
            {
                if (ApiExceptionHandler.Instance.HandleException(ref ex, ControllerContext, ActionContext))
                {
                    throw ex;
                }

                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        [Route("Login")]
        public async Task<IHttpActionResult> Login(LoginRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                var response = await AuthenticationProvider.Instance.Login(request);
                return Ok(response);
            }
            catch (Exception ex)
            {
                if (ApiExceptionHandler.Instance.HandleException(ref ex, ControllerContext, ActionContext))
                {
                    throw ex;
                }

                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        [Route("Reauthenticate")]
        [SSOAuthorize]
        public async Task<IHttpActionResult> Reauthenticate(ReauthenticateRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                var response = await AuthenticationProvider.Instance.Reauthenticate(request);
                return Ok(response);
            }
            catch (Exception ex)
            {
                if (ApiExceptionHandler.Instance.HandleException(ref ex, ControllerContext, ActionContext))
                {
                    throw ex;
                }

                return BadRequest(ex.Message);
            }
        }

        [HttpGet]
        [Route("ExternalLogin")]
        public IHttpActionResult GetExternalLogin(string provider, string redirect_url)
        {
            try
            {
                return AuthenticationProvider.Instance.GetExternalLogin(provider, redirect_url);
            }
            catch (Exception ex)
            {
                if (ApiExceptionHandler.Instance.HandleException(ref ex, ControllerContext, ActionContext))
                {
                    throw ex;
                }

                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        [Route("ObtainLocalAccessToken")]
        public async Task<IHttpActionResult> ObtainLocalAccessToken(ObtainLocalAccessTokenRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                var response = await AuthenticationProvider.Instance.ObtainLocalAccessToken(request);
                return Ok(response);
            }
            catch (Exception ex)
            {
                if (ApiExceptionHandler.Instance.HandleException(ref ex, ControllerContext, ActionContext))
                {
                    throw ex;
                }

                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        [Route("RefreshToken")]
        public async Task<IHttpActionResult> RefreshToken(RefreshTokenRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                ObtainLocalAccessTokenResponse response = await AuthenticationProvider.Instance.RefreshToken(request);
                return Ok(response);
            }
            catch (Exception ex)
            {
                if (ApiExceptionHandler.Instance.HandleException(ref ex, ControllerContext, ActionContext))
                {
                    throw ex;
                }

                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        [Route("Authenticate")]
        public async Task<IHttpActionResult> Authenticate(AuthenticateRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                AuthenticateResponseModel response = await AuthenticationProvider.Instance.Authenticate(request);
                return Ok(response);
            }
            catch (Exception ex)
            {
                if (ApiExceptionHandler.Instance.HandleException(ref ex, ControllerContext, ActionContext))
                {
                    throw ex;
                }

                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        [Route("ClientEncryptionToken")]
        public async Task<IHttpActionResult> GenerateClientEncryptionToken(GenerateClientEncryptionTokenRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                GenerateClientEncryptionTokenResponse response = await AuthenticationProvider.Instance.GenerateClientEncryptionToken(request);
                return Ok(response);
            }
            catch (Exception ex)
            {
                if (ApiExceptionHandler.Instance.HandleException(ref ex, ControllerContext, ActionContext))
                {
                    throw ex;
                }

                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        [Route("ActivateClient")]
        public async Task<IHttpActionResult> ActivateClient(ActivateClientRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                ActivateClientResponse response = await AuthenticationProvider.Instance.ActivateClient(request);

                return Ok(response);
            }
            catch (Exception ex)
            {
                if (ApiExceptionHandler.Instance.HandleException(ref ex, ControllerContext, ActionContext))
                {
                    throw ex;
                }

                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        [Route("Activate")]
        public async Task<IHttpActionResult> Activate(ActivateRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                ActivateResponse response = await AuthenticationProvider.Instance.Activate(request);

                return Ok(response);
            }
            catch (Exception ex)
            {
                if (ApiExceptionHandler.Instance.HandleException(ref ex, ControllerContext, ActionContext))
                {
                    throw ex;
                }

                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        [Route("GrantActivation")]
        public async Task<IHttpActionResult> GrantActivation(GrantActivationRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                ActivateLocalAccessTokenResponse response = await AuthenticationProvider.Instance.GrantActivation(request);

                return Ok(response);
            }
            catch (Exception ex)
            {
                if (ApiExceptionHandler.Instance.HandleException(ref ex, ControllerContext, ActionContext))
                {
                    throw ex;
                }

                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        [Route("PasswordlessLogin")]
        public async Task<IHttpActionResult> PasswordlessLogin(GrantActivationRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                var response = await AuthenticationProvider.Instance.PasswordlessLogin(request);

                return Ok(response);
            }
            catch (Exception ex)
            {
                if (ApiExceptionHandler.Instance.HandleException(ref ex, ControllerContext, ActionContext))
                {
                    throw ex;
                }

                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        [Route("VerifyClientToken")]
        public async Task<IHttpActionResult> VerifyClientToken()
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                VerifyClientTokenResponseModel response = await AuthenticationProvider.Instance.VerifyClientToken(new VerifyTokenRequest
                {
                    AccessToken = HttpContext.Current?.GetToken()
                });

                return Ok(response);
            }
            catch (Exception ex)
            {
                if (ApiExceptionHandler.Instance.HandleException(ref ex, ControllerContext, ActionContext))
                {
                    throw ex;
                }

                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        [Route("Authorize")]
        [SSOAuthorize]
        public IHttpActionResult Authorize(AuthorizeRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                var response = AuthenticationProvider.Instance.GetUserData();
                return Ok(response);
            }
            catch (Exception ex)
            {
                if (ApiExceptionHandler.Instance.HandleException(ref ex, ControllerContext, ActionContext))
                {
                    throw ex;
                }

                return BadRequest(ex.Message);
            }
        }

        [HttpGet]
        [Route("UserData")]
        [SSOAuthorize]
        public IHttpActionResult GetUserData()
        {
            try
            {
                var response = AuthenticationProvider.Instance.GetUserData();
                return Ok(response);
            }
            catch (Exception ex)
            {
                if (ApiExceptionHandler.Instance.HandleException(ref ex, ControllerContext, ActionContext))
                {
                    throw ex;
                }

                return BadRequest(ex.Message);
            }
        }

        [HttpGet]
        [Route("ClientData")]
        [SSOAuthorize]
        public IHttpActionResult GetClientData()
        {
            try
            {
                var response = AuthenticationProvider.Instance.GetClientData();
                return Ok(response);
            }
            catch (Exception ex)
            {
                if (ApiExceptionHandler.Instance.HandleException(ref ex, ControllerContext, ActionContext))
                {
                    throw ex;
                }

                return BadRequest(ex.Message);
            }
        }

        [HttpGet]
        [Route("ValidateToken")]
        [SSOAuthorize]
        public async Task<IHttpActionResult> ValidateToken()
        {
            try
            {
                var response = await AuthenticationProvider.Instance.ValidateToken();
                return Ok(response);
            }
            catch (Exception ex)
            {
                if (ApiExceptionHandler.Instance.HandleException(ref ex, ControllerContext, ActionContext))
                {
                    throw ex;
                }

                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        [Route("Register")]
        public async Task<IHttpActionResult> Register(RegisterRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                var response = await AuthenticationProvider.Instance.Register(request);
                return Ok(response);
            }
            catch (HttpException<ModelStateErrorResult> ex)
            {
                return GetErrorResult(ex);
            }
            catch (Exception ex)
            {
                if (ApiExceptionHandler.Instance.HandleException(ref ex, ControllerContext, ActionContext))
                {
                    throw ex;
                }

                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        [Route("ChangePassword")]
        public async Task<IHttpActionResult> ChangePassword(ChangePasswordRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                var response = await AuthenticationProvider.Instance.ChangePassword(request);
                return Ok(response);
            }
            catch (HttpException<ModelStateErrorResult> ex)
            {
                return GetErrorResult(ex);
            }
            catch (Exception ex)
            {
                if (ApiExceptionHandler.Instance.HandleException(ref ex, ControllerContext, ActionContext))
                {
                    throw ex;
                }

                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        [Route("UpdateUserInfo")]
        public async Task<IHttpActionResult> UpdateUserInfo(UpdateUserInfoRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                await AuthenticationProvider.Instance.UpdateUserInfo(request);
                return Ok();
            }
            catch (HttpException<ModelStateErrorResult> ex)
            {
                return GetErrorResult(ex);
            }
            catch (Exception ex)
            {
                if (ApiExceptionHandler.Instance.HandleException(ref ex, ControllerContext, ActionContext))
                {
                    throw ex;
                }

                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        [Route("ForgetPassword")]
        public async Task<IHttpActionResult> ForgetPassword(ForgetPasswordRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                var response = await AuthenticationProvider.Instance.ForgetPassword(request);
                return Ok(response);
            }
            catch (HttpException<ModelStateErrorResult> ex)
            {
                return GetErrorResult(ex);
            }
            catch (Exception ex)
            {
                if (ApiExceptionHandler.Instance.HandleException(ref ex, ControllerContext, ActionContext))
                {
                    throw ex;
                }

                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        [Route("ResetPassword")]
        public async Task<IHttpActionResult> ResetPassword(ResetPasswordRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                var response = await AuthenticationProvider.Instance.ResetPassword(request);
                return Ok(response);
            }
            catch (HttpException<ModelStateErrorResult> ex)
            {
                return GetErrorResult(ex);
            }
            catch (Exception ex)
            {
                if (ApiExceptionHandler.Instance.HandleException(ref ex, ControllerContext, ActionContext))
                {
                    throw ex;
                }

                return BadRequest(ex.Message);
            }
        }

        private IHttpActionResult GetErrorResult(HttpException<ModelStateErrorResult> ex)
        {
            foreach (var key in ex.ErrorResult.ModelState.Keys)
            {
                foreach (var errorMessage in ex.ErrorResult.ModelState[key])
                {
                    ModelState.AddModelError(key, errorMessage);
                }
            }

            return BadRequest(ModelState);
        }
    }
}
