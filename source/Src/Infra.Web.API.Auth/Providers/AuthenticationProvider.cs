using DotFramework.Core;
using DotFramework.Infra.Security.Model;
using DotFramework.Infra.Web.API.Auth.Base;
using System;
using System.Threading.Tasks;
#if NETFRAMEWORK
using System.Web.Http.Results;
#else
using Microsoft.AspNetCore.Mvc;
#endif

namespace DotFramework.Infra.Web.API.Auth
{
    public class AuthenticationProvider : AbstractAuthenticationProvider<AuthenticationProvider>
    {
        public override RedirectResult GetExternalLogin(string provider, string redirect_url)
        {
            try
            {
                OnGetExternalLoginStarted(new GetExternalLoginStartedEventArgs(provider, redirect_url));
                var result = _AuthenticationService.GetExternalLogin(provider, redirect_url);
                OnGetExternalLoginFinished(new GetExternalLoginFinishedEventArgs(provider, redirect_url, result));

                return result;
            }
            catch (Exception ex)
            {
                OnObtainLocalAccessTokenFailed(new ErrorEventArgs(ex));
                throw;
            }
        }
    }
}