using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DotFramework.Core;
using DotFramework.Core.Web;
using DotFramework.Infra.Security.Model;
using DotFramework.Infra.Web.API.Auth.Providers;
using DotFramework.Infra.Web.API;
using DotFramework.Infra.Web.API.Auth;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Mvc;
using DotFramework.Web.API;
using DotFramework.Infra.Web.API.Auth.Base.Providers;

namespace DotFramework.Infra.Web.API.Auth.Controllers
{
    [ApiController]
    [Route("Auth")]
    public class AuthController : ControllerBase
    {
        [HttpPost]
        [Route("AuthenticateClient")]
        public async Task<IActionResult> AuthenticateClient()
        {
            try
            {
                var response = await AuthenticationProvider.Instance.AuthenticateClient(null);
                return Ok(response);
            }
            catch (Exception ex)
            {
                return GetErrorResult(ex);
            }
        }

        [HttpPost]
        [Route("Login")]
        public async Task<IActionResult> Login(LoginRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                var response = await AuthenticationProvider.Instance.Login(request);
                return Ok(response);
            }
            catch (Exception ex)
            {
                return GetErrorResult(ex);
            }
        }

        [HttpPost]
        [Route("Reauthenticate")]
        [SSOAuthorize]
        public async Task<IActionResult> Reauthenticate(ReauthenticateRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                var response = await AuthenticationProvider.Instance.Reauthenticate(request);
                return Ok(response);
            }
            catch (Exception ex)
            {
                return GetErrorResult(ex);
            }
        }

        [HttpDelete]
        [Route("Logout")]
        [SSOAuthorize]
        public async Task<IActionResult> Logout()
        {
            try
            {
                var response = await AuthenticationProvider.Instance.Logout();
                return Ok(response);
            }
            catch (Exception ex)
            {
                return GetErrorResult(ex);
            }
        }

        [HttpGet]
        [Route("ExternalLogin")]
        public IActionResult GetExternalLogin(string provider, string redirect_url)
        {
            try
            {
                return AuthenticationProvider.Instance.GetExternalLogin(provider, redirect_url);
            }
            catch (Exception ex)
            {
                return GetErrorResult(ex);
            }
        }

        [HttpPost]
        [Route("ObtainLocalAccessToken")]
        public async Task<IActionResult> ObtainLocalAccessToken(ObtainLocalAccessTokenRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                var response = await AuthenticationProvider.Instance.ObtainLocalAccessToken(request);
                return Ok(response);
            }
            catch (Exception ex)
            {
                return GetErrorResult(ex);
            }
        }

        [HttpPost]
        [Route("RefreshToken")]
        public async Task<IActionResult> RefreshToken(RefreshTokenRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                ObtainLocalAccessTokenResponse response = await AuthenticationProvider.Instance.RefreshToken(request);
                return Ok(response);
            }
            catch (Exception ex)
            {
                return GetErrorResult(ex);
            }
        }

        [HttpPost]
        [Route("Authenticate")]
        public async Task<IActionResult> Authenticate(AuthenticateRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                AuthenticateResponseModel response = await AuthenticationProvider.Instance.Authenticate(request);
                return Ok(response);
            }
            catch (Exception ex)
            {
                return GetErrorResult(ex);
            }
        }

        [HttpPost]
        [Route("ClientEncryptionToken")]
        public async Task<IActionResult> GenerateClientEncryptionToken(GenerateClientEncryptionTokenRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                GenerateClientEncryptionTokenResponse response = await AuthenticationProvider.Instance.GenerateClientEncryptionToken(request);
                return Ok(response);
            }
            catch (Exception ex)
            {
                return GetErrorResult(ex);
            }
        }

        [HttpPost]
        [Route("ActivateClient")]
        public async Task<IActionResult> ActivateClient(ActivateClientRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                ActivateClientResponse response = await AuthenticationProvider.Instance.ActivateClient(request);

                return Ok(response);
            }
            catch (Exception ex)
            {
                return GetErrorResult(ex);
            }
        }

        [HttpPost]
        [Route("Activate")]
        public async Task<IActionResult> Activate(ActivateRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                ActivateResponse response = await AuthenticationProvider.Instance.Activate(request);

                return Ok(response);
            }
            catch (Exception ex)
            {
                return GetErrorResult(ex);
            }
        }

        [HttpPost]
        [Route("GrantActivation")]
        public async Task<IActionResult> GrantActivation(GrantActivationRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                ActivateLocalAccessTokenResponse response = await AuthenticationProvider.Instance.GrantActivation(request);

                return Ok(response);
            }
            catch (Exception ex)
            {
                return GetErrorResult(ex);
            }
        }

        [HttpPost]
        [Route("PasswordlessLogin")]
        public async Task<IActionResult> PasswordlessLogin(GrantActivationRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                var response = await AuthenticationProvider.Instance.PasswordlessLogin(request);

                return Ok(response);
            }
            catch (Exception ex)
            {
                return GetErrorResult(ex);
            }
        }

        [HttpPost]
        [Route("VerifyClientToken")]
        [SSOAuthorize]
        public async Task<IActionResult> VerifyClientToken()
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                VerifyClientTokenResponseModel response = await AuthenticationProvider.Instance.VerifyClientToken(new VerifyTokenRequest
                {
                    AccessToken = HttpContext?.GetToken()
                });

                return Ok(response);
            }
            catch (Exception ex)
            {
                return GetErrorResult(ex);
            }
        }

        [HttpPost]
        [Route("Authorize")]
        //[SSOAuthorize]
        public IActionResult Authorize(AuthorizeRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                var response = AuthenticationProvider.Instance.GetUserData();
                return Ok(response);
            }
            catch (Exception ex)
            {
                return GetErrorResult(ex);
            }
        }

        [HttpGet]
        [Route("UserData")]
        [SSOAuthorize]
        public IActionResult GetUserData()
        {
            try
            {
                var response = AuthenticationProvider.Instance.GetUserData();
                return Ok(response);
            }
            catch (Exception ex)
            {
                return GetErrorResult(ex);
            }
        }

        [HttpGet]
        [Route("ClientData")]
        [SSOAuthorize]
        public IActionResult GetClientData()
        {
            try
            {
                var response = AuthenticationProvider.Instance.GetClientData();
                return Ok(response);
            }
            catch (Exception ex)
            {
                return GetErrorResult(ex);
            }
        }

        [HttpGet]
        [Route("ValidateToken")]
        [SSOAuthorize]
        public async Task<IActionResult> ValidateToken()
        {
            try
            {
                var response = await AuthenticationProvider.Instance.ValidateToken();
                return Ok(response);
            }
            catch (Exception ex)
            {
                return GetErrorResult(ex);
            }
        }

        [HttpPost]
        [Route("Register")]
        public async Task<IActionResult> Register(RegisterRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                var response = await AuthenticationProvider.Instance.Register(request);
                return Ok(response);
            }
            catch (Exception ex)
            {
                return GetErrorResult(ex);
            }
        }

        [HttpPost]
        [Route("ChangePassword")]
        [SSOAuthorize]
        public async Task<IActionResult> ChangePassword(ChangePasswordRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                var response = await AuthenticationProvider.Instance.ChangePassword(request);
                return Ok(response);
            }
            catch (Exception ex)
            {
                return GetErrorResult(ex);
            }
        }

        [HttpPost]
        [Route("UpdateUserInfo")]
        [SSOAuthorize]
        public async Task<IActionResult> UpdateUserInfo(UpdateUserInfoRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                await AuthenticationProvider.Instance.UpdateUserInfo(request);
                return Ok();
            }
            catch (Exception ex)
            {
                return GetErrorResult(ex);
            }
        }

        [HttpPost]
        [Route("ForgetPassword")]
        public async Task<IActionResult> ForgetPassword(ForgetPasswordRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                var response = await AuthenticationProvider.Instance.ForgetPassword(request);
                return Ok(response);
            }
            catch (Exception ex)
            {
                return GetErrorResult(ex);
            }
        }

        [HttpPost]
        [Route("ResetPassword")]
        public async Task<IActionResult> ResetPassword(ResetPasswordRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                var response = await AuthenticationProvider.Instance.ResetPassword(request);
                return Ok(response);
            }
            catch (Exception ex)
            {
                return GetErrorResult(ex);
            }
        }

        [HttpPost]
        [Route("ConfirmEmail")]
        public async Task<IActionResult> ConfirmEmail(ConfirmEmailBindingModel request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                await AuthenticationProvider.Instance.ConfirmEmail(request);
                return Ok();
            }
            catch (Exception ex)
            {
                return GetErrorResult(ex);
            }
        }

        [HttpGet]
        [Route("ConfirmEmailCallback")]
        public async Task<IActionResult> ConfirmEmailCallback([FromQuery] ConfirmEmailCallbackBindingModel model)
        {
            try
            {
                return await AuthenticationProvider.Instance.ConfirmEmailCallback(model);
            }
            catch (Exception ex)
            {
                return GetErrorResult(ex);
            }
        }

#if DEBUG

        [HttpPost]
        [Route("CreateClient")]
        public async Task<IActionResult> CreateClient(CreateClientRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                var response = await AuthenticationProvider.Instance.CreateClient(request);
                return Ok(response);
            }
            catch (Exception ex)
            {
                return GetErrorResult(ex);
            }
        }

        [HttpPost]
        [Route("GetClient")]
        public async Task<IActionResult> GetClient(GetClientRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                var response = await AuthenticationProvider.Instance.GetClient(request);
                return Ok(response);
            }
            catch (Exception ex)
            {
                return GetErrorResult(ex);
            }
        }

#endif

        private IActionResult GetErrorResult(Exception ex)
        {
            if (ex is HttpException<ModelStateErrorResult> exp)
            {
                foreach (var key in exp.ErrorResult.ModelState.Keys)
                {
                    foreach (var errorMessage in exp.ErrorResult.ModelState[key])
                    {
                        ModelState.AddModelError(key, errorMessage);
                    }
                }

                return BadRequest(ModelState);
            }
            else if (ex is HttpException<BadRequestErrorResult> expB)
            {
                return BadRequest(expB.ErrorResult.Message);
            }
            else
            {
                try
                {
                    if (ApiExceptionHandler.Instance.HandleException(ref ex, HttpContext))
                    {
                        throw ex;
                    }
                }
                catch (UnauthorizedHttpException)
                {
                    return Unauthorized();
                }
                catch (Exception)
                {
                    throw;
                }

                return BadRequest(ex.Message);
            }
        }
    }
}
