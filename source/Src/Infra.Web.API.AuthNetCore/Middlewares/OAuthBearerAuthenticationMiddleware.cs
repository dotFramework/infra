﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using DotFramework.Core.Configuration;
using DotFramework.Infra.Web.API.Auth.Base;
using DotFramework.Infra.Web.API.Auth.Providers;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.Extensions.Primitives;

namespace DotFramework.Infra.Web.API.Auth.Middleware
{
    public class OAuthBearerAuthenticationMiddleware<TAuthenticationService>
        where TAuthenticationService : DotFramework.Infra.Web.API.Auth.Base.IAuthenticationService
    {
        private readonly RequestDelegate _next;
        private readonly OAuthBearerHandler _OAuthBearerHandler;

        public OAuthBearerAuthenticationMiddleware(RequestDelegate next, IProtectionProvider protectionProvider)
        {
            _next = next;
            _OAuthBearerHandler = new OAuthBearerHandler(protectionProvider);

            var tokenProvider = new TokenProvider(protectionProvider, SecureWebAPIStartup.AccessTokenExpireTimeSpan, "Authentication");
            var authenticationService = AuthenticationServiceFactory.Instance.Resolve<TAuthenticationService>();

            authenticationService.Initialize(tokenProvider);
        }

        public async Task Invoke(HttpContext context)
        {
            var endpoint = context.GetEndpoint();

            if (endpoint != null)
            {
                try
                {
                    var ticket = _OAuthBearerHandler.Authenticate(context);

                    if (ticket != null)
                    {
                        context.User = ticket.Principal;
                    }
                }
                catch (Exception)
                {
                    
                }
            }

            await _next(context);
        }
    }
}
