﻿using DotFramework.Infra.Web.API.Auth.Base.Providers;
using DotFramework.Infra.Web.API.Auth.Providers;
using DotFramework.Web.API;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Primitives;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace DotFramework.Infra.Web.API.Auth
{
    internal class OAuthBearerHandler
    {
        private readonly IProtectionProvider _protectionProvider;

        internal OAuthBearerHandler(IProtectionProvider protectionProvider)
        {
            _protectionProvider = protectionProvider;
        }

        public AuthenticationTicket Authenticate(HttpContext context)
        {
            var token = context.GetToken();

            if (!String.IsNullOrEmpty(token))
            {
                return _protectionProvider.UnProtect(token, "Authentication");
            }
            else
            {
                return null;
            }
        }
    }
}
