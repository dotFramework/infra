using DotFramework.Core;
using DotFramework.Infra.Security.Model;
using DotFramework.Infra.Web.API.Auth.Base;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace DotFramework.Infra.Web.API.Auth
{
    public class AuthenticationProvider : AbstractAuthenticationProvider<AuthenticationProvider>
    {
        public override RedirectResult GetExternalLogin(string provider, string redirect_url)
        {
            try
            {
                OnGetExternalLoginStarted(new GetExternalLoginStartedEventArgs(provider, redirect_url));
                var result = _AuthenticationService.GetExternalLogin(provider, redirect_url);
                //TODO
                //OnGetExternalLoginFinished(new GetExternalLoginFinishedEventArgs(provider, redirect_url, result));

                return result;
            }
            catch (Exception ex)
            {
                OnObtainLocalAccessTokenFailed(new ErrorEventArgs(ex));
                throw;
            }
        }
    }
}