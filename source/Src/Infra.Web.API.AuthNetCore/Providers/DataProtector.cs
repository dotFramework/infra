﻿using DotFramework.Core;
using DotFramework.Core.Configuration;
using DotFramework.Infra.Security;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.DataProtection;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Text;

namespace DotFramework.Infra.Web.API.Auth.Providers
{
    public class DataProtector : HttpContextProvider
    {
        private readonly IDataProtectionProvider _dataProtectionProvider;
        private readonly List<string> _purpose = new List<string>() { "TokenMiddleware", "Access_Token", "v2" };
        private readonly ITicketCompressor _ticketCompressor;

        private readonly bool _CompressTicket;

        public DataProtector(IDataProtectionProvider dataProtectionProvider, bool compressTicket, ITicketCompressor ticketCompressor)
        {
            if (_dataProtectionProvider == null)
            {
                _dataProtectionProvider = dataProtectionProvider.CreateProtector(_purpose);
            }

            _CompressTicket = compressTicket;
            _ticketCompressor = ticketCompressor;
        }

        public string Protect(AuthenticationTicket ticket, string tokenType)
        {
            if (_dataProtectionProvider == null)
            {
                throw new Exception("IDataProtectionProvider is not provided through initialization");
            }

            var sticket = TicketSerializer.Default.Serialize(CompressTicket(ticket, tokenType));
            var protector = _dataProtectionProvider.CreateProtector(_purpose);

            return protector.Protect(Encoding.UTF8.GetString(sticket));
        }

        public AuthenticationTicket UnProtect(string token, string tokenType)
        {
            if (_dataProtectionProvider == null)
            {
                throw new Exception("IDataProtectionProvider is not provided through initialization");
            }

            var protector = _dataProtectionProvider.CreateProtector(_purpose);

            try
            {
                string decryptedToken = protector.Unprotect(token);
                AuthenticationTicket ticket = TicketSerializer.Default.Deserialize(Encoding.UTF8.GetBytes(decryptedToken));

                if (ticket.Properties.ExpiresUtc < DateTime.UtcNow)
                {
                    RemoveIdentity(ticket, tokenType);
                    throw new UnauthorizedHttpException();
                }

                return ExtractTicket(ticket, tokenType);
            }
            catch (Exception)
            {
                throw new UnauthorizedHttpException();
            }
        }

        public void Revoke(string token, string tokenType)
        {
            if (_dataProtectionProvider == null)
            {
                throw new Exception("IDataProtectionProvider is not provided through initialization");
            }

            var protector = _dataProtectionProvider.CreateProtector(_purpose);

            try
            {
                string decryptedToken = protector.Unprotect(token);
                AuthenticationTicket ticket = TicketSerializer.Default.Deserialize(Encoding.UTF8.GetBytes(decryptedToken));

                RemoveIdentity(ticket, tokenType);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void RevokeGroup(string groupToken, string tokenType)
        {
            try
            {
                _ticketCompressor.Remove(
                    tokenType,
                    groupToken);
            }
            catch (Exception)
            {
                throw;
            }
        }

        private AuthenticationTicket CompressTicket(AuthenticationTicket ticket, string tokenType)
        {
            if (!_CompressTicket)
            {
                return ticket;
            }
            else
            {
                var parentGroupToken = Context?.User?.Claims.FirstOrDefault(c => c.Type == CustomClaimTypes.GroupToken);

                var groupTokenClaim = ticket.Principal.Claims.FirstOrDefault(c => c.Type == CustomClaimTypes.GroupToken) ?? parentGroupToken?.Clone() ?? new Claim(CustomClaimTypes.GroupToken, CreateShortGuid());
                var tokenIdentifier = CreateShortGuid();
                var tokenIdentifierClaim = new Claim(CustomClaimTypes.TokenIdentifier, tokenIdentifier);
                var nameIdentifier = ticket.Principal.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier)?.Value ?? ticket.Principal.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Name)?.Value;

                ClaimsIdentity oAuthIdentity_compressed = new ClaimsIdentity(new List<Claim> { groupTokenClaim, tokenIdentifierClaim }, ticket.AuthenticationScheme);
                ClaimsPrincipal claimsPrincipal_compressed = new ClaimsPrincipal(oAuthIdentity_compressed);
                AuthenticationTicket ticket_compressed = new AuthenticationTicket(claimsPrincipal_compressed, ticket.Properties, ticket.AuthenticationScheme);

                if (!ticket.Principal.Claims.Any(c => c.Type == CustomClaimTypes.GroupToken))
                {
                    ticket.Principal.Claims.Append(groupTokenClaim);
                }

                SaveIdentityClaims(ticket, groupTokenClaim.Value, tokenIdentifier, tokenType, nameIdentifier);

                return ticket_compressed;
            }
        }

        private AuthenticationTicket ExtractTicket(AuthenticationTicket ticket, string tokenType)
        {
            if (!_CompressTicket)
            {
                return ticket;
            }
            else
            {
                ClaimsIdentity oAuthIdentity = new ClaimsIdentity(RetrieveIdentityClaims(ticket, tokenType), ticket.AuthenticationScheme);
                ClaimsPrincipal claimsPrincipal = new ClaimsPrincipal(oAuthIdentity);

                AuthenticationTicket new_ticket = new AuthenticationTicket(claimsPrincipal, ticket.Properties, ticket.AuthenticationScheme);

                return new_ticket;
            }
        }

        private void RemoveIdentity(AuthenticationTicket ticket, string tokenType)
        {
            var groupTokenClaim = ticket.Principal.Claims.FirstOrDefault(c => c.Type == CustomClaimTypes.GroupToken);
            var tokenIdentifierClaim = ticket.Principal.Claims.FirstOrDefault(c => c.Type == CustomClaimTypes.TokenIdentifier);

            _ticketCompressor.Remove(
                tokenType,
                groupTokenClaim?.Value, 
                tokenIdentifierClaim.Value);
        }

        private void SaveIdentityClaims(AuthenticationTicket ticket, string groupIdentifier, string tokenIdentifier, string tokenType, string nameIdentifier)
        {
            var sticket = CustomTicketSerializer.Default.Serialize(ticket);

            var protector = _dataProtectionProvider.CreateProtector(_purpose);
            var protectedTicket = protector.Protect(Encoding.UTF8.GetString(sticket));

            _ticketCompressor.Save(
                protectedTicket, 
                tokenType,
                groupIdentifier, 
                tokenIdentifier,
                nameIdentifier,
                ticket.Properties.IssuedUtc.Value.DateTime, 
                ticket.Properties.ExpiresUtc.Value.DateTime);
        }

        private IEnumerable<Claim> RetrieveIdentityClaims(AuthenticationTicket ticket, string tokenType)
        {
            if (!ticket.Principal.Claims.Any(c => c.Type == CustomClaimTypes.GroupToken))
            {
                throw new NullReferenceException("Group Token Claim cannot be empty.");
            }
            else if (!ticket.Principal.Claims.Any(c => c.Type == CustomClaimTypes.TokenIdentifier))
            {
                throw new NullReferenceException("Token Identifier Claim cannot be empty.");
            }
            else
            {
                var groupTokenClaim = ticket.Principal.Claims.First(c => c.Type == CustomClaimTypes.GroupToken);
                var tokenIdentifierClaim = ticket.Principal.Claims.First(c => c.Type == CustomClaimTypes.TokenIdentifier);

                var token = _ticketCompressor.Retrieve(
                    tokenType,
                    groupTokenClaim.Value,
                    tokenIdentifierClaim.Value);

                var protector = _dataProtectionProvider.CreateProtector(_purpose);
                string decryptedToken = protector.Unprotect(token);
                AuthenticationTicket new_ticket = CustomTicketSerializer.Default.Deserialize(Encoding.UTF8.GetBytes(decryptedToken));

                return new_ticket.Principal.Claims;
            }
        }

        private string CreateShortGuid()
        {
            var ticks = new DateTime(2016, 1, 1).Ticks;
            var ans = DateTime.Now.Ticks - ticks;
            return ans.ToString("x");
        }
    }
}
