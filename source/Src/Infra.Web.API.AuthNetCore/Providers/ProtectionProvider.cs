﻿using DotFramework.Core;
using DotFramework.Core.Configuration;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.DataProtection;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace DotFramework.Infra.Web.API.Auth.Providers
{
    public interface IProtectionProvider
    {
        void Initialize(IDataProtectionProvider dataProtectionProvider, bool compressTicket, ITicketCompressor ticketCompressor);
        string Protect(AuthenticationTicket ticket, string tokenType);
        AuthenticationTicket UnProtect(string token, string tokenType);
        void Revoke(string token, string tokenType);
        void RevokeGroup(string groupToken, string tokenType);
    }

    public abstract class ProtectionProvider : IProtectionProvider
    {
        protected DataProtector _DataProtector;
        protected bool _CompressTicket;

        public void Initialize(IDataProtectionProvider dataProtectionProvider, bool compressTicket, ITicketCompressor ticketCompressor)
        {
            _CompressTicket = compressTicket;

            if (ticketCompressor == null)
            {
                ticketCompressor = new FileTicketCompressor(AppSettingsManager.Instance.Get("IdentityRepositoryPath", Path.Combine(Directory.GetCurrentDirectory(), "Identity")));
            }

            _DataProtector =
                new DataProtector(
                    dataProtectionProvider,
                    _CompressTicket,
                    ticketCompressor);
        }

        public virtual void Initialize()
        {
            
        }

        public virtual string Protect(AuthenticationTicket ticket, string tokenType)
        {
            return _DataProtector.Protect(ticket, tokenType);
        }

        public virtual AuthenticationTicket UnProtect(string token, string tokenType)
        {
            return _DataProtector.UnProtect(token, tokenType);
        }

        public virtual void Revoke(string token, string tokenType)
        {
            if (_CompressTicket)
            {
                _DataProtector.Revoke(token, tokenType);
            }
            else
            {
                throw new NotSupportedException("Uncompressed token cannot be revoked!");
            }
        }

        public virtual void RevokeGroup(string groupToken, string tokenType)
        {
            if (_CompressTicket)
            {
                _DataProtector.RevokeGroup(groupToken, tokenType);
            }
            else
            {
                throw new NotSupportedException("Uncompressed token cannot be revoked!");
            }
        }
    }

    public class AuthenticationProtectionProvider : ProtectionProvider
    {
        
    }
}
