using System;
using DotFramework.Core;
using DotFramework.Infra.ExceptionHandling;
using DotFramework.Infra.Web.API.Auth.Base;
using DotFramework.Infra.Web.API.Auth.Base.Providers;
using DotFramework.Infra.Web.API.Auth.Middleware;
using DotFramework.Infra.Web.API.Auth.Providers;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace DotFramework.Infra.Web.API.Auth
{
    public abstract class SecureWebAPIStartup : WebAPIStartup
    {
        public SecureWebAPIStartup(IConfiguration configuration) : base(configuration)
        {

        }

        public static TimeSpan AccessTokenExpireTimeSpan { get; protected set; }

        public override void InitializeServices(IServiceCollection services)
        {
            base.InitializeServices(services);
            InitializeAuthService(services);
            services.AddSingleton<IProtectionProvider, AuthenticationProtectionProvider>();
        }

        protected virtual void InitializeAuthService(IServiceCollection services)
        {
            services.AddSingleton<IAuthContext, AuthContext>();
        }

        public override void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            base.Configure(app, env);
        }

        public override void InitializeApplication(IApplicationBuilder app, IWebHostEnvironment env)
        {
            base.InitializeApplication(app, env);
        }

        public virtual void UseAuthenticationMiddleware(IApplicationBuilder app)
        {
            UseAuthenticationMiddleware<AuthenticationService>(app);
        }

        public virtual void UseAuthenticationMiddleware<TAuthenticationService>(IApplicationBuilder app)
            where TAuthenticationService: IAuthenticationService
        {
            app.UseMiddleware<OAuthBearerAuthenticationMiddleware<TAuthenticationService>>();
        }

        protected virtual void InitializeAuthApplication(IApplicationBuilder app, IWebHostEnvironment env)
        {
            AuthContextProvider.Configure(ServiceProvider.GetService<IAuthContext>());

            IProtectionProvider protectionProvider = ServiceProvider.GetService<IProtectionProvider>();
            protectionProvider.Initialize(
                ServiceProvider.GetService<IDataProtectionProvider>(),
                    true,
                    ServiceProvider.GetService<ITicketCompressor>());
        }

        protected virtual void InitializeAuthServices(IServiceCollection services)
        {
            services.AddDataProtection();
        }

        protected override void ConfigureSessionProvider(IServiceCollection services)
        {
            services.AddSingleton<ISessionInfoProvider, SecureSessionInfoProvider>();
        }
    }

    public class SecureSessionInfoProvider : SessionInfoProvider
    {
        public override string GetUserName()
        {
            return AuthContextProvider.AuthContext?.UserName;
        }
    }
}
