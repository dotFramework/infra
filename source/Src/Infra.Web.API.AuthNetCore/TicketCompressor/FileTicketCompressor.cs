﻿using Microsoft.AspNetCore.Authentication;
using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Claims;
using System.Text;

namespace DotFramework.Infra.Web.API
{
    public class FileTicketCompressor : ITicketCompressor
    {
        private readonly string _TicketRepositoryPath = Path.Combine(Directory.GetCurrentDirectory(), "Identity");

        public FileTicketCompressor(string ticketRepositoryPath)
        {
            _TicketRepositoryPath = ticketRepositoryPath;

            if (String.IsNullOrEmpty(_TicketRepositoryPath))
            {
                throw new ArgumentNullException("TicketRepositoryPath");
            }
            else
            {
                if (!Directory.Exists(_TicketRepositoryPath))
                {
                    Directory.CreateDirectory(_TicketRepositoryPath);
                }
            }
        }

        public void Save(string protectedTicket, string tokenType, string groupIdentifier, string tokenIdentifier, string nameIdentifier, DateTime issuedAt, DateTime expiresAt)
        {
            string identityFileDirPath = Path.Combine(_TicketRepositoryPath, groupIdentifier, tokenType);
            var identityFilePath = Path.Combine(identityFileDirPath, $"{tokenIdentifier}.dat");

            Directory.CreateDirectory(identityFileDirPath);
            File.WriteAllText(identityFilePath, protectedTicket);
        }

        public string Retrieve(string tokenType, string groupIdentifier, string tokenIdentifier)
        {
            var identityFilePath = Path.Combine(_TicketRepositoryPath, groupIdentifier, tokenType, $"{tokenIdentifier}.dat");
            return File.ReadAllText(identityFilePath);
        }

        public void Remove(string tokenType, string groupIdentifier)
        {
            if (!String.IsNullOrEmpty(groupIdentifier))
            {
                if (tokenType == "Authentication")
                {
                    string groupIdentityPath = Path.Combine(_TicketRepositoryPath, groupIdentifier);

                    if (Directory.Exists(groupIdentityPath))
                    {
                        Directory.Delete(groupIdentityPath, true);
                    }
                }
                else
                {
                    string groupIdentityPath = Path.Combine(_TicketRepositoryPath, groupIdentifier, tokenType);

                    if (Directory.Exists(groupIdentityPath))
                    {
                        Directory.Delete(groupIdentityPath, true);
                    }
                }
            }
        }

        public void Remove(string tokenType, string groupIdentifier, string tokenIdentifier)
        {
            Remove(tokenType, groupIdentifier);
        }
    }
}
