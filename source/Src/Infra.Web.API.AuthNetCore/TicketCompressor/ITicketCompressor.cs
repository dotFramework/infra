﻿using Microsoft.AspNetCore.Authentication;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;

namespace DotFramework.Infra.Web.API
{
    public interface ITicketCompressor
    {
        void Save(string protectedTicket, string tokenType, string groupIdentifier, string tokenIdentifier, string nameIdentifier, DateTime issuedAt, DateTime expiresAt);
        string Retrieve(string tokenType, string groupIdentifier, string tokenIdentifier);
        void Remove(string tokenType, string groupIdentifier);
        void Remove(string tokenType, string groupIdentifier, string tokenIdentifier);
    }
}
