﻿using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;

namespace DotFramework.Infra.Web.API.Controllers
{
    [ApiController]
    public class ErrorController : ControllerBase
    {
        [Route("/error")]
        [HttpGet]
        public IActionResult GetError() => Problem();

        [Route("/error")]
        [HttpPost]
        public IActionResult PostError() => Problem();

        [Route("/error")]
        [HttpPut]
        public IActionResult PutError() => Problem();

        [Route("/error")]
        [HttpDelete]
        public IActionResult DeleteError() => Problem();

        [Route("/error")]
        [HttpPatch]
        public IActionResult PatchError() => Problem();

        [Route("/error")]
        [HttpHead]
        public IActionResult HeadError() => Problem();

        [Route("/error")]
        [HttpOptions]
        public IActionResult OptionsError() => Problem();
    }
}
