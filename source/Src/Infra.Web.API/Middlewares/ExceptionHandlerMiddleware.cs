﻿using DotFramework.Core.Serialization;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Abstractions;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.AspNetCore.Routing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.ExceptionServices;
using System.Text;
using System.Threading.Tasks;

namespace DotFramework.Infra.Web.API
{
    public class ExceptionHandlerMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ProblemDetailsFactory _problemDetailsFactory;

        private static readonly ActionDescriptor EmptyActionDescriptor = new ActionDescriptor();
        private static readonly RouteData EmptyRouteData = new RouteData();

        public ExceptionHandlerMiddleware(RequestDelegate next, ProblemDetailsFactory problemDetailsFactory)
        {
            _next = next ?? throw new ArgumentNullException(nameof(next));
            _problemDetailsFactory = problemDetailsFactory ?? throw new ArgumentNullException(nameof(problemDetailsFactory));
        }

        public async Task InvokeAsync(HttpContext context)
        {
            ExceptionDispatchInfo? edi = null;

            try
            {
                await _next(context);
            }
            catch (Exception ex)
            {
                edi = ExceptionDispatchInfo.Capture(ex);
            }

            if (edi != null)
            {
                await HandleException(context, edi);
            }
        }

        private async Task HandleException(HttpContext context, ExceptionDispatchInfo edi)
        {
            if (context.Response.HasStarted)
            {
                edi.Throw(); // Re-throw the original exception if we can't handle it properly.
            }

            try
            {
                var error = edi.SourceException;

                var feature = new ExceptionHandlerFeature
                {
                    Path = context.Request.Path,
                    Error = error,
                };

                ClearResponse(context, StatusCodes.Status500InternalServerError);

                context.Features.Set<IExceptionHandlerPathFeature>(feature);
                context.Features.Set<IExceptionHandlerFeature>(feature);

                var details = _problemDetailsFactory.CreateProblemDetails(context);

                if (details != null) // Don't handle the exception if we can't or don't want to convert it to ProblemDetails
                {
                    await WriteProblemDetails(context, details);
                }
            }
            catch (Exception)
            {
                // If we fail to write a problem response, we log the exception and throw the original below.
            }

            //edi.Throw(); // Re-throw the original exception if we can't handle it properly or it's intended.
        }

        private async Task WriteProblemDetails(HttpContext context, ProblemDetails details)
        {
            var routeData = context.GetRouteData() ?? EmptyRouteData;

            var actionContext = new ActionContext(context, routeData, EmptyActionDescriptor);

            var result = new ObjectResult(details)
            {
                StatusCode = details.Status ?? context.Response.StatusCode,
                ContentTypes = new MediaTypeCollection { "application/problem+json" }
            };

            await result.ExecuteResultAsync(actionContext);

            await context.Response.CompleteAsync();
        }

        private void ClearResponse(HttpContext context, int statusCode)
        {
            var headers = new HeaderDictionary();

            foreach (var header in context.Response.Headers)
            {
                headers.Add(header);
            }

            context.Response.Clear();
            context.Response.StatusCode = statusCode;

            foreach (var header in headers)
            {
                context.Response.Headers.Add(header);
            }
        }
    }
}
