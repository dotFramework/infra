﻿using DotFramework.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace DotFramework.Infra.Web.API
{
    public abstract class HttpContextProvider
    {
        public HttpContext Context
        {
            get
            {
                return HttpContext.Current;
            }
        }
    }
}
