﻿using DotFramework.Core;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace DotFramework.Infra.Web.API
{
    public abstract class HttpContextProvider
    {
        public HttpContextProvider(IHttpContextAccessor httpContextAccessor)
        {
            _HttpContextAccessor = httpContextAccessor;
        }

        public HttpContextProvider() : this(ServiceContext.Instance.ServiceProvider.GetService<IHttpContextAccessor>())
        {
        }

        private IHttpContextAccessor _HttpContextAccessor;

        protected HttpContext Context
        {
            get
            {
                return _HttpContextAccessor.HttpContext;
            }
        }
    }
}
