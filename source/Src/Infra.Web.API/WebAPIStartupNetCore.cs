﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Hosting;
using DotFramework.Infra.ExceptionHandling;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using DotFramework.Core.DependencyInjection;
using Microsoft.Extensions.Caching.Memory;
using System.Threading;
using System.Net;
using DotFramework.Core;
using Microsoft.AspNetCore.Http.Extensions;

namespace DotFramework.Infra.Web.API
{
    public abstract class WebAPIStartup
    {
        public abstract string AplicationCode { get; }
        public IConfiguration Configuration { get; }
        public static IServiceProvider ServiceProvider { get; private set; }

        public WebAPIStartup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public virtual void ConfigureServices(IServiceCollection services)
        {
            ConfigureSessionProvider(services);
            ConfigureCache(services);

            services.AddSession((options) =>
            {
                ConfigureSession(options);
            });
        }

        protected virtual void ConfigureSessionProvider(IServiceCollection services)
        {
            services.AddSingleton<ISessionInfoProvider, SessionInfoProvider>();
        }

        protected virtual void ConfigureCache(IServiceCollection services)
        {
            AddMemoryCache(services);
            AddDistributedCache(services);
        }

        protected virtual void AddMemoryCache(IServiceCollection services)
        {
            services.AddMemoryCache((options) =>
            {
                SetupMemoryCache(options);
            });
        }

        protected virtual void AddDistributedCache(IServiceCollection services)
        {
            services.AddDistributedMemoryCache((options) =>
            {
                SetupMemoryDistributedCache(options);
            });
        }

        private void SetupMemoryCache(MemoryCacheOptions options)
        {

        }

        private void SetupMemoryDistributedCache(MemoryDistributedCacheOptions options)
        {

        }

        protected virtual void ConfigureSession(SessionOptions options)
        {
            options.IdleTimeout = TimeSpan.FromMinutes(10);
        }

        public virtual void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            ServiceProvider = app.ApplicationServices;
            ServiceContext.Instance.Initialize(app.ApplicationServices);

            //app.UseSession();
            app.UseMiddleware<ExceptionHandlerMiddleware>();
        }

        public virtual void InitializeServices(IServiceCollection services)
        {
            services.AddTransient<ProblemDetailsFactory, CustomProblemDetailsFactory>();
            services.AddHttpContextAccessor();
            services.AddHttpClient();
        }

        public virtual void InitializeApplication(IApplicationBuilder app, IWebHostEnvironment env)
        {
            InitializeTraceLogger();
        }

        public virtual void InitializeTraceLogger()
        {
            TraceLogManager.Instance.Initialize(AplicationCode, ServiceProvider.GetService<ISessionInfoProvider>());
        }
    }

    public class SessionInfoProvider : HttpContextProvider, ISessionInfoProvider
    {
        public virtual string GetSessionID()
        {
            return null;
        }

        public virtual string GetContextID()
        {
            if (Context != null && Context.Items.ContainsKey("ContextID"))
            {
                return Context.Items["ContextID"].ToString();
            }
            else
            {
                return null;
            }
        }

        public virtual string GetConnectionID()
        {
            return Context?.Connection.Id;
        }

        public virtual string GetUserName()
        {
            return null;
        }

        public virtual string GetHost()
        {
            return Context?.Request.Host.ToString();
        }

        public virtual string GetUrl()
        {
            return Context?.Request.GetDisplayUrl();
        }
        
        public virtual string GetReferer()
        {
            return GetRefererSafe(Context?.Request);
        }

        public virtual string GetTenant()
        {
            return null;
        }

        private void MakeSessionAvailable(ISession session)
        {
            try
            {
                if (!session.IsAvailable)
                {
                    lock (session)
                    {
                        session.LoadAsync().GetAwaiter().GetResult();

                        try
                        {
                            session.SetString("TempForLoading", Guid.NewGuid().ToString());
                        }
                        catch (Exception)
                        {
                            session.Remove("TempForLoading");
                        }
                    }
                }
            }
            catch (Exception)
            {

            }
        }

        private string GetRefererSafe(HttpRequest request)
        {
            if (request?.Headers?.TryGetValue("Referer", out var referer) == true)
            {
                return referer.ToString();
            }

            return null;
        }
    }
}
